# Blender

Blender add-on for importing and exporting Pod cars, ghosts, and tracks.

![Scorp car loaded with io_scene_pod](res/readme/car.png)
![AlderOEM track loaded with io_scene_pod](res/readme/track.png)
