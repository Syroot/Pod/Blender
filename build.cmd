@ECHO OFF
SET IN_DIR="%CD%\src\io_scene_pod"
SET OUT_DIR="%CD%\dist"

IF NOT EXIST %OUT_DIR% MD %OUT_DIR%

PUSHD %IN_DIR%
BLENDER --command extension build --output-dir %OUT_DIR%
POPD
