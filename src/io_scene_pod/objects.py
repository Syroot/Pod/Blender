import bmesh
import bpy
from ubipod.object import Face, FaceProperty, Object

from . import butils, colors, materials


def load(object: Object, name: str, col: bpy.types.Collection, images: list[bpy.types.Image]) -> bpy.types.Object:
    # Create bmesh.
    mesh = bpy.data.meshes.new(name)
    bm = bmesh.new()
    uv_loop_layer = bm.loops.layers.uv.new()
    # Add positions.
    for pos in object.positions:
        bm.verts.new(pos)
    bm.verts.ensure_lookup_table()
    # Connect faces.
    mat_indices = {}
    for face in object.faces:
        try:
            f = bm.faces.new((bm.verts[face.indices[i]] for i in range(face.index_count)))
        except ValueError as e:
            print(e)  # Ignore duplicate faces.
            continue
        # Set texture coordinates.
        for i in range(face.index_count):
            uv = face.effect_data.texture_uvs[i]
            f.loops[i][uv_loop_layer].uv = (uv[0] / 255.0, (255 - uv[1]) / 255.0)
        # Set material.
        face_color = colors.from_rgb888(face.effect_data.color)
        face_image = images[face.effect_data.texture_index]
        face_property = face.decode_property()
        mat = materials.acquire(face.effect, face_color, face_image, face_property, face.mirror)
        mat_index = mat_indices.get(mat)
        if mat_index is None:
            mat_index = mat_indices[mat] = len(mat_indices)
            mesh.materials.append(mat)
        f.material_index = mat_index
        # Set property.
        f.hide = not face_property.visible
    # Create mesh object and link to collection.
    bm.to_mesh(mesh)
    bm.free()
    ob = bpy.data.objects.new(name, mesh)
    col.objects.link(ob)
    return ob


def save(context: bpy.types.Context, ob: bpy.types.Object, images: list[bpy.types.Image], ignore_transform: bool = False) -> Object:
    object = Object()
    bm = butils.bmesh_from_object(ob, context.evaluated_depsgraph_get(), ignore_transform)
    # Add positions and normals.
    for v in bm.verts:
        object.positions.append(tuple(v.co))
        object.normals.append(tuple(v.normal))
    object.radius = object.calc_prism().calc_radius()
    # Connect faces.
    uv_loop_layer = bm.loops.layers.uv[0]
    for f in bm.faces:
        face = Face()
        object.faces.append(face)
        face.normal = tuple(f.normal)
        # Set texture coordinates.
        for i, l in enumerate(f.loops):
            face.indices[i] = l.vert.index
            uv = tuple(l[uv_loop_layer].uv)
            face.effect_data.texture_uvs[i] = (round(uv[0] * 255), 255 - round(uv[1] * 255))
        face.index_count = i + 1
        # Set color or texture material.
        mat = ob.data.materials[f.material_index]
        face.material_name = mat.name.upper()
        face.effect = materials.get_effect(mat)
        if face.effect.is_color():
            face.effect_data.color = colors.to_rgb888(materials.get_color(mat))
        else:
            img = materials.get_image(mat)
            try:
                face.effect_data.texture_index = images.index(img)
            except ValueError:
                face.effect_data.texture_index = len(images)
                images.append(img)
        # Set property and mirror.
        property = FaceProperty(not f.hide, materials.get_road(mat), materials.get_wall(mat), materials.get_slip(mat),
                                materials.get_backface(mat), materials.get_trans(mat), materials.get_dural(mat),
                                materials.get_lod(mat))
        face.encode_property(property)
        face.mirror = materials.get_mirror(mat)
    bm.free()
    return object
