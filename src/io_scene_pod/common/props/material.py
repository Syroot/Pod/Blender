import bpy
from ubipod.object import Effect

from ... import materials


class POD_Material(bpy.types.PropertyGroup):
    def get_effect(self) -> int:
        return materials.get_effect(bpy.context.object.active_material).value

    def set_effect(self, value: int) -> None:
        materials.set_effect(bpy.context.object.active_material, Effect(value))

    def get_color(self) -> tuple[float, float, float]:
        return materials.get_color(bpy.context.object.active_material)

    def set_color(self, value: tuple[float, float, float]) -> None:
        materials.set_color(bpy.context.object.active_material, value)

    def get_backface(self) -> bool:
        return materials.get_backface(bpy.context.object.active_material)

    def set_backface(self, value: bool) -> None:
        materials.set_backface(bpy.context.object.active_material, value)

    # Properties actually stored in the material use get/set.
    effect: bpy.props.EnumProperty(name="Effect", get=get_effect, set=set_effect, items=(
        (Effect.FLAT.name, "Color", "", Effect.FLAT.value),
        (Effect.GOURAUD.name, "Color Gouraud", "", Effect.GOURAUD.value),
        (Effect.TEXTURE.name, "Texture", "", Effect.TEXTURE.value),
        (Effect.TEXGOU.name, "Texture Gouraud", "", Effect.TEXGOU.value),
    ))
    road: bpy.props.BoolProperty(name="Is Road")
    wall: bpy.props.BoolProperty(name="Is Wall")
    slip: bpy.props.IntProperty(name="Slipperiness", subtype='FACTOR', min=0, max=255)
    color: bpy.props.FloatVectorProperty(name="Color", subtype='COLOR', min=0.0, max=1.0, get=get_color, set=set_color)
    lod: bpy.props.IntProperty(name="Texture Level", subtype='FACTOR', min=0, max=3)
    backface: bpy.props.BoolProperty(name="Show Backface", get=get_backface, set=set_backface)
    trans: bpy.props.BoolProperty(name="Transparent")
    dural: bpy.props.BoolProperty(name="Dural Effect")
    mirror: bpy.props.IntProperty(name="Mirror Group")


def register() -> None:
    bpy.types.Material.pod = bpy.props.PointerProperty(type=POD_Material)


def unregister() -> None:
    del bpy.types.Material.pod
