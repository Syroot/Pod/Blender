import bpy


class POD_Sound(bpy.types.PropertyGroup):
    type: bpy.props.EnumProperty(name="Sound Event Type", items=(
        ('NONE', "None", "", 'MUTE_IPO_OFF', 0),
        ('FIX', "Fix", "", 'OUTLINER_DATA_SPEAKER', 1),
        ('PATCH', "Patch", "", 'OUTLINER_OB_SPEAKER', 2),
    ))
    id: bpy.props.IntProperty(name="Sound Event ID", min=0, max=0x7FFF, soft_max=100)

    def draw(self, layout: bpy.types.UILayout, text: str = "") -> None:
        if layout.use_property_split:
            split = layout.split(factor=0.4)
            split.alignment = 'RIGHT'
            split.label(text=text)
            row = split.row(align=True)
            row.prop(self, "type", icon_only=True)
            sub = row.split(align=True)
            sub.active = self.type != 'NONE'
            sub.use_property_split = False
            sub.prop(self, "id", text="")
        else:
            row = layout.row(align=True)
            row.prop(self, "type", icon_only=True)
            sub = row.split(align=True)
            sub.active = self.type != 'NONE'
            sub.prop(self, "id", text=text)

    def get(self) -> int:
        if self.type == 'PATCH':
            return -0x8000 + self.id
        elif self.type == 'FIX':
            return self.id
        else:
            return -1

    def set(self, value: int) -> None:
        if value == -1:
            self.type = 'NONE'
        elif value & 0x8000:
            self.type = 'PATCH'
            self.id = value & 0x7FFF
        else:
            self.type = 'FIX'
            self.id = value
