import bpy
from ubipod.object import Effect

from ... import materials
from ..ops.update_material_name import POD_OT_UpdateMaterialName
from ..props.material import POD_Material


class POD_PT_Material(bpy.types.Panel):
    bl_context = "material"
    bl_label = "Pod"
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return context.object.active_material != None

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        layout.use_property_decorate = False
        layout.use_property_split = True
        mat = context.object.active_material
        if materials.is_compatible(mat):
            props: POD_Material = mat.pod
            row = layout.row()
            row.prop(props, "road")
            row.prop(props, "wall")
            if props.road or props.wall:
                layout.prop(props, "slip")
            layout.prop(props, "effect")
            if Effect[props.effect].is_color():
                layout.prop(props, "color")
            else:
                color_in = materials.get_surface_node(mat).inputs["Base Color"]
                layout.template_ID(color_in.links[0].from_node, "image", text="Texture", open="image.open")
                layout.prop(props, "lod")
                layout.prop(props, "dural")
            layout.prop(props, "backface")
            layout.prop(props, "trans")
            layout.prop(props, "mirror")
            layout.operator(POD_OT_UpdateMaterialName.bl_idname, text="Update Material Name")
        else:
            layout.label(text="Missing Principled BSDF node.", icon='CANCEL')
