from typing import Union

import bpy

from ... import materials


class POD_OT_UpdateMaterialName(bpy.types.Operator):
    """Sets the material name to describe Pod specific settings"""
    bl_idname = "material.pod_update_name"
    bl_label = "Update Pod Material Name"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        ob = context.object
        return ob != None and ob.active_material != None

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        mat = context.object.active_material
        mat.name = materials.get_material_key(mat)
        return {'FINISHED'}
