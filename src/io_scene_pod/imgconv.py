from typing import Sequence

import numpy
from numpy.typing import NDArray
from PIL import Image

Colors = NDArray[numpy.float32]
Pixels = tuple[float, ...]
Size = tuple[int, int]


def load_palette_555(data: bytes, count: int = 256) -> Colors:
    values = numpy.frombuffer(data, count=count, dtype=numpy.uint16)
    colors = numpy.empty((len(values), 4), dtype=numpy.float32)
    colors[:, 0] = (values >> 10 & 0b11111) / 0b11111
    colors[:, 1] = (values >> 5 & 0b11111) / 0b11111
    colors[:, 2] = (values & 0b11111) / 0b11111
    colors[:, 3] = 1
    return colors


def load_palette_565(data: bytes, count: int = 256) -> Colors:
    values = numpy.frombuffer(data, count=count, dtype=numpy.uint16)
    colors = numpy.empty((len(values), 4), dtype=numpy.float32)
    colors[:, 0] = (values >> 11 & 0b11111) / 0b11111
    colors[:, 1] = (values >> 5 & 0b111111) / 0b111111
    colors[:, 2] = (values & 0b11111) / 0b11111
    colors[:, 3] = 1
    return colors


def load_palette_888(data: bytes, count: int = 256) -> Colors:
    if count != -1:
        count *= 3
    values = numpy.frombuffer(data, count=count, dtype=numpy.uint8)
    values.shape = -1, 3
    colors = numpy.empty((len(values), 4), dtype=numpy.float32)
    colors[:, 0] = values[:, 0] / 255
    colors[:, 1] = values[:, 1] / 255
    colors[:, 2] = values[:, 2] / 255
    colors[:, 3] = 1
    return colors


def load_pixels_555(data: bytes, size: Size) -> Pixels:
    colors = load_palette_555(data, size[0] * size[1])
    colors = _flip_y(colors, size)
    return tuple(colors)


def load_pixels_565(data: bytes, size: Size) -> Pixels:
    colors = load_palette_565(data, size[0] * size[1])
    colors = _flip_y(colors, size)
    return tuple(colors)


def load_pixels_888(data: bytes, size: Size) -> Pixels:
    colors = load_palette_888(data, size[0] * size[1])
    colors = _flip_y(colors, size)
    return tuple(colors)


def load_pixels_pal(data: bytes, size: Size, palette: Colors) -> Pixels:
    indices = numpy.frombuffer(data, count=size[0] * size[1], dtype=numpy.uint8)
    colors = palette[indices]
    colors = _flip_y(colors, size)
    return tuple(colors)


def save_palette_555(colors: Colors) -> bytes:
    colors.shape = -1, 4
    data = numpy.uint16(numpy.uint8(colors[:, 0] * 255) >> 3) << 10 \
        | numpy.uint16(numpy.uint8(colors[:, 1] * 255) >> 3) << 5 \
        | numpy.uint16(numpy.uint8(colors[:, 2] * 255) >> 3)
    return data.tobytes()


def save_palette_565(colors: Colors) -> bytes:
    colors.shape = -1, 4
    data = numpy.uint16(numpy.uint8(colors[:, 0] * 255) >> 3) << 11 \
        | numpy.uint16(numpy.uint8(colors[:, 1] * 255) >> 2) << 5 \
        | numpy.uint16(numpy.uint8(colors[:, 2] * 255) >> 3)
    return data.tobytes()


def save_palette_888(colors: Colors) -> bytes:
    colors.shape = -1, 4
    data = numpy.empty((len(colors), 3), dtype=numpy.uint8)
    data[:, 0] = colors[:, 0] * 255
    data[:, 1] = colors[:, 1] * 255
    data[:, 2] = colors[:, 2] * 255
    return data.tobytes()


def save_pixels_555(pixels: Pixels, size: Size) -> bytes:
    colors = numpy.array(pixels, dtype=numpy.float32)
    colors = _flip_y(colors, size)
    return save_palette_555(colors)


def save_pixels_565(pixels: Pixels, size: Size) -> bytes:
    colors = numpy.array(pixels, dtype=numpy.float32)
    colors = _flip_y(colors, size)
    return save_palette_565(colors)


def save_pixels_888(pixels: Pixels, size: Size) -> bytes:
    colors = numpy.array(pixels, dtype=numpy.float32)
    colors = _flip_y(colors, size)
    return save_palette_888(colors)


def save_pixels_pal(pixels: Pixels, size: Size, palette: Colors) -> bytes:
    # Load palette.
    pal = Image.new('P', (256, 1))
    pal.putpalette(save_palette_888(palette), 'RGB')
    # Quantize image.
    img = _to_pil(pixels, size)
    return img.quantize(palette=pal).tobytes()


def save_pixels_pal_auto(pixels: Sequence[Pixels], size: Size) -> tuple[list[bytes], Colors]:
    # Concat images vertically to quantize together.
    w, h = size
    imgs = Image.new('RGB', (w, h * len(pixels)))
    for i, p in enumerate(pixels):
        sub = _to_pil(p, size)
        imgs.paste(sub, (0, h * i))
    # Quantize 256 color palette listing pure black first to support transparency.
    # PIL sorts by descending brightness, making black come last, so swap it to the first slot or reserve it.
    img = imgs.quantize()
    palette = bytearray(img.palette.palette)
    palette += bytes(3 * 256 - len(palette))
    if any(palette[-3:]):
        img = imgs.quantize(colors=255)
        palette = bytearray(img.palette.palette)
        palette += bytes(3 * 256 - len(palette))
    # Swap black (last) color to come first.
    palette[-3:] = palette[:3]
    palette[:3] = bytes(3)
    data = bytearray(img.tobytes())
    for i in range(len(data)):
        if data[i] == 0:
            data[i] = 255
        elif data[i] == 255:
            data[i] = 0
    # Split image data apart again.
    s = w * h
    data = [bytes(data[i * s:(i + 1) * s]) for i in range(len(pixels))]
    return data, load_palette_888(palette)


def _flip_y(colors: Colors, size: Size) -> Colors:
    colors.shape = size[1], size[0], 4
    return colors[::-1].ravel()


def _to_pil(pixels: Pixels, size: Size) -> Image.Image:
    data = save_pixels_888(pixels, size)
    return Image.frombuffer('RGB', size, data)
