import pathlib
from typing import Union

import bpy
import bpy_extras
from ubipod.binary import BinaryReader
from ubipod.ghost import Ghost, Sequence

from ... import butils


class POD_OT_GhostImport(bpy.types.Operator, bpy_extras.io_utils.ImportHelper):
    """Load a Pod GHT or SEQ ghost file"""
    bl_idname = "import_scene.ght_seq"
    bl_label = "Import Pod Ghost"
    bl_options = {'UNDO'}
    filename_ext = ".ght,.seq"
    filter_glob: bpy.props.StringProperty(default="*.ght;*.seq", options={'HIDDEN'})

    @staticmethod
    def menu(menu: bpy.types.Menu, context: bpy.types.Context) -> None:
        menu.layout.operator(POD_OT_GhostImport.bl_idname, text="Pod Ghost (.ght, .seq)")

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        # Read from file.
        with BinaryReader(open(self.properties.filepath, "rb")) as read:
            path = pathlib.Path(self.properties.filepath)
            ext = path.suffix.upper()
            if ext == ".GHT":
                ghost = read.any(Ghost)
                sequences = ghost.sequences
            elif ext == ".SEQ":
                sequences = [read.any(Sequence)]
            else:
                raise ValueError(f"Unsupported ghost file extension {ext}.")
        # Convert data.
        [self.import_sequence(x) for x in sequences]
        return {'FINISHED'}

    def import_sequence(self, sequence: Sequence) -> None:
        col = butils.create_collection("Ghosts")
        name = f"Ghost {sequence.info.player_name} {format_pod_time(sequence.info.race_time)}"
        # Create curve.
        curve = bpy.data.curves.new(name, 'CURVE')
        curve.dimensions = '3D'
        spline: bpy.types.Spline = curve.splines.new('POLY')
        spline.points.add(len(sequence.points) - 1)
        for i, point in enumerate(sequence.points):
            spline.points[i].co = point.position + (1,)
        # Create object.
        ob = bpy.data.objects.new(name, curve)
        col.objects.link(ob)


def format_pod_time(time: float) -> str:
    time = round(time, 3)
    min = int(time / 60)
    sec = int(time % 60)
    mil = int(round(time % 1 * 1000, 0))
    return f"{min:02}'{sec:02}\"{mil:03}"


def register() -> None:
    bpy.types.TOPBAR_MT_file_import.append(POD_OT_GhostImport.menu)


def unregister() -> None:
    bpy.types.TOPBAR_MT_file_import.remove(POD_OT_GhostImport.menu)
