import bpy

from .. import utils
from .scene import POD_CircuitCompetitor, POD_CircuitScene


class POD_CircuitWinman(bpy.types.PropertyGroup):
    def zone_index_update(self, context: bpy.types.Context) -> None:
        # Select zone object.
        for ob in context.selected_objects:
            ob.select_set(False)
        context.object.select_set(True)
        context.scene.objects[self.zone_index].select_set(True)

    def competitor_difficulty_update(self, context: bpy.types.Context) -> None:
        # Select first competitor of chosen difficulty, if any.
        props: POD_CircuitScene = context.scene.pod_circuit
        for i, competitor in enumerate(props.competitors):
            competitor: POD_CircuitCompetitor
            if competitor.difficulty == self.competitor_difficulty:
                self.competitor_index = i
                return
        self.competitor_index = -1

    zone_index: bpy.props.IntProperty(name="Visible Sector", update=zone_index_update)
    times_reverse: bpy.props.BoolProperty()
    phases_reverse: bpy.props.BoolProperty()
    competitor_difficulty: bpy.props.EnumProperty(name="Competitor Difficulty", default=utils.Dif.EASY.name, items=(
        (utils.Dif.EASY.name, "E", "Easy", utils.Dif.EASY.value),
        (utils.Dif.MEDIUM.name, "M", "Medium", utils.Dif.MEDIUM.value),
        (utils.Dif.HARD.name, "H", "Hard", utils.Dif.HARD.value),
    ), update=competitor_difficulty_update)
    competitor_index: bpy.props.IntProperty(name="Competitors")
    competitor_attack_index: bpy.props.IntProperty(name="Competitor Attacks")


def register() -> None:
    bpy.types.WindowManager.pod_circuit = bpy.props.PointerProperty(type=POD_CircuitWinman)


def unregister() -> None:
    del bpy.types.WindowManager.pod_circuit
