import bpy
from ubipod.circuit import MapConstraintType, MapGraph

from ...common.props.sound import POD_Sound
from .. import utils
from .events import POD_CircuitBeaconEvent


class POD_CircuitSectorZone(bpy.types.PropertyGroup):
    ob: bpy.props.PointerProperty(type=bpy.types.Object)


class POD_CircuitSector(bpy.types.PropertyGroup):
    zones: bpy.props.CollectionProperty(type=POD_CircuitSectorZone)


class POD_CircuitSpeaker(bpy.types.PropertyGroup):
    sound: bpy.props.PointerProperty(type=POD_Sound)


class POD_CircuitBeacon(bpy.types.PropertyGroup):
    reverse: bpy.props.BoolProperty(name="Reverse")
    positive_events: bpy.props.CollectionProperty(type=POD_CircuitBeaconEvent, name="Positive Events")
    positive_events_index: bpy.props.IntProperty(name="Positive Events")
    negative_events: bpy.props.CollectionProperty(type=POD_CircuitBeaconEvent, name="Negative Events")
    negative_events_index: bpy.props.IntProperty(name="Negative Events")


class POD_CircuitGate(bpy.types.PropertyGroup):
    reverse: bpy.props.BoolProperty(name="Reverse")


class POD_CircuitTrail(bpy.types.PropertyGroup):
    def beacon_poll(self, ob: bpy.types.Object) -> bool:
        return utils.is_beacon(ob, self.reverse) and utils.get_beacon_event_dir(ob, 'RACE_SECTION') != 0

    def reverse_update(self, context: bpy.types.Context) -> None:
        self.beacon = None

    reverse: bpy.props.BoolProperty(name="Reverse", update=reverse_update)
    difficulties: bpy.props.EnumProperty(name="Difficulties", options={'ENUM_FLAG'}, items=(
        (utils.Dif.EASY.name, "E", "Easy", utils.Dif.EASY.value),
        (utils.Dif.MEDIUM.name, "M", "Medium", utils.Dif.MEDIUM.value),
        (utils.Dif.HARD.name, "H", "Hard", utils.Dif.HARD.value),
    ))
    beacon: bpy.props.PointerProperty(name="Beacon", type=bpy.types.Object, poll=beacon_poll)
    beacon_direction: bpy.props.EnumProperty(name="Beacon Direction", items=(
        ('POSITIVE', "Positive", "", 'ADD', 1),
        ('NEGATIVE', "Negative", "", 'REMOVE', -1),
    ))
    level: bpy.props.IntProperty(name="Level", subtype='FACTOR', min=0, max=9)
    constraint_type: bpy.props.EnumProperty(name="Constraint", items=(
        (MapConstraintType.NONE.name, "None", "", MapConstraintType.NONE.value),
        (MapConstraintType.MIN_SPEED.name, "Min. Speed", "", MapConstraintType.MIN_SPEED.value),
        (MapConstraintType.MAX_SPEED.name, "Max. Speed", "", MapConstraintType.MAX_SPEED.value),
        (MapConstraintType.STOP_ATTACK.name, "Stop Attack", "", MapConstraintType.STOP_ATTACK.value),
        (MapConstraintType.DISENGAGE.name, "Disengage", "", MapConstraintType.DISENGAGE.value),
        (MapConstraintType.PIT.name, "Pit", "", MapConstraintType.PIT.value),
        (MapConstraintType.START.name, "Start", "", MapConstraintType.START.value),
    ))
    constraint_param: bpy.props.IntProperty(name="Constraint Parameter", min=0, max=32700)
    constraint_number: bpy.props.IntProperty(name="Constraint Number", min=0, max=255)
    graph: bpy.props.EnumProperty(name="Graph", items=(
        (MapGraph.NONE.name, "Default", "", 'RADIOBUT_OFF', MapGraph.NONE.value),
        (MapGraph.PREFER.name, "Prefer", "", 'SOLO_OFF', MapGraph.PREFER.value),
        (MapGraph.ADDITIONAL.name, "Additional", "", 'PARTICLE_PATH', MapGraph.ADDITIONAL.value),
        (MapGraph.IGNORE.name, "Ignore", "", 'X', MapGraph.IGNORE.value),
    ))
    shortcut: bpy.props.BoolProperty(name="Shortcut")


class POD_CircuitObject(bpy.types.PropertyGroup):
    def type_update(self, context: bpy.types.Context) -> None:
        ob = context.object
        if ob and ob.type == 'MESH' and self.type == 'SECTOR':
            utils.set_ob_brightness(context, ob)  # create brightness layer if it does not exist yet

    type: bpy.props.EnumProperty(name="Type", update=type_update, items=(
        ('NONE', "None", "", 0),
        ('SECTOR', "Sector", "", 'GROUP_VERTEX', 1),
        ('SPEAKER', "Speaker", "", 'OUTLINER_OB_SPEAKER', 2),
        ('PIT', "Pit", "", 'MODIFIER', 3),
        ('BEACON', "Beacon", "", 'ORIENTATION_NORMAL', 4),
        ('GATE', "Gate", "", 'PLAY', 5),
        ('TRAIL', "Trail", "", 'CURVE_PATH', 6),
    ))
    sector: bpy.props.PointerProperty(type=POD_CircuitSector)
    speaker: bpy.props.PointerProperty(type=POD_CircuitSpeaker)
    beacon: bpy.props.PointerProperty(type=POD_CircuitBeacon)
    gate: bpy.props.PointerProperty(type=POD_CircuitGate)
    trail: bpy.props.PointerProperty(type=POD_CircuitTrail)


@bpy.app.handlers.persistent
def depsgraph_update_post_handler(scene: bpy.types.Scene) -> None:
    # Remove zones no longer in scene or no longer qualifying as sector.
    for ob in scene.objects:
        zones = ob.pod_circuit.sector.zones
        for i in reversed(range(len(zones))):
            zone = zones[i]
            if not utils.is_sector(zone.ob) or zone.ob.name not in scene.objects:
                zones.remove(i)


def register() -> None:
    bpy.types.Object.pod_circuit = bpy.props.PointerProperty(type=POD_CircuitObject)
    bpy.app.handlers.depsgraph_update_post.append(depsgraph_update_post_handler)


def unregister() -> None:
    del bpy.types.Object.pod_circuit
    bpy.app.handlers.depsgraph_update_post.remove(depsgraph_update_post_handler)
