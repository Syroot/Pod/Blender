import bpy
from ubipod.circuit import CompetitorAttackContext, CompetitorAttackType, CompetitorBehavior

from .. import utils
from .events import POD_CircuitPhaseEvent


class POD_CircuitPatcher(bpy.types.PropertyGroup):
    def img_poll(self, img: bpy.types.Image) -> bool:
        return img.size[0] == 288 and img.size[1] == 167

    def img_name_poll(self, img: bpy.types.Image) -> bool:
        return img.size[0] == 68 and img.size[1] == 17

    id: bpy.props.IntProperty(name="ID", min=0, max=255, default=255)
    laps: bpy.props.IntProperty(name="Laps", min=1, max=8, default=3)
    reversable: bpy.props.BoolProperty(name="Reversable", default=True)
    img: bpy.props.PointerProperty(name="Image", type=bpy.types.Image, poll=img_poll)
    img_name: bpy.props.PointerProperty(name="Name Image", type=bpy.types.Image, poll=img_name_poll)
    img_name_reverse: bpy.props.PointerProperty(name="Reverse Name Image", type=bpy.types.Image, poll=img_name_poll)


class POD_CircuitPits(bpy.types.PropertyGroup):
    repair_seconds: bpy.props.FloatProperty(name="Repair Time (Global)", min=0, soft_max=10, step=0.1, subtype='TIME_ABSOLUTE')


class POD_CircuitTimesDirection(bpy.types.PropertyGroup):
    min_part_1: bpy.props.FloatProperty(name="Minimum Part 1", min=0.0, soft_max=60, step=0.1, subtype='TIME_ABSOLUTE')
    min_part_2: bpy.props.FloatProperty(name="Minimum Part 2", min=0.0, soft_max=60, step=0.1, subtype='TIME_ABSOLUTE')
    min_part_3: bpy.props.FloatProperty(name="Minimum Part 3", min=0.0, soft_max=60, step=0.1, subtype='TIME_ABSOLUTE')
    min_part_4: bpy.props.FloatProperty(name="Minimum Part 4", min=0.0, soft_max=60, step=0.1, subtype='TIME_ABSOLUTE')
    avg_part_1: bpy.props.FloatProperty(name="Average Part 1", min=0.0, soft_max=60, step=0.1, subtype='TIME_ABSOLUTE')
    avg_part_2: bpy.props.FloatProperty(name="Average Part 2", min=0.0, soft_max=60, step=0.1, subtype='TIME_ABSOLUTE')
    avg_part_3: bpy.props.FloatProperty(name="Average Part 3", min=0.0, soft_max=60, step=0.1, subtype='TIME_ABSOLUTE')
    avg_part_4: bpy.props.FloatProperty(name="Average Part 4", min=0.0, soft_max=60, step=0.1, subtype='TIME_ABSOLUTE')


class POD_CircuitTimes(bpy.types.PropertyGroup):
    forward: bpy.props.PointerProperty(type=POD_CircuitTimesDirection)
    reverse: bpy.props.PointerProperty(type=POD_CircuitTimesDirection)


class POD_CircuitPhasesDirection(bpy.types.PropertyGroup):
    arrival_events: bpy.props.CollectionProperty(type=POD_CircuitPhaseEvent)
    arrival_events_index: bpy.props.IntProperty(name="Arrival Events")
    load_events: bpy.props.CollectionProperty(type=POD_CircuitPhaseEvent)
    load_events_index: bpy.props.IntProperty(name="Load Events")
    start_events: bpy.props.CollectionProperty(type=POD_CircuitPhaseEvent)
    start_events_index: bpy.props.IntProperty(name="Start Events")
    finish_events: bpy.props.CollectionProperty(type=POD_CircuitPhaseEvent)
    finish_events_index: bpy.props.IntProperty(name="Finish Events")


class POD_CircuitPhases(bpy.types.PropertyGroup):
    forward: bpy.props.PointerProperty(type=POD_CircuitPhasesDirection)
    reverse: bpy.props.PointerProperty(type=POD_CircuitPhasesDirection)


class POD_CircuitCompetitorAttack(bpy.types.PropertyGroup):
    type: bpy.props.EnumProperty(name="Type", items=(
        (CompetitorAttackType.ERROR.name, "Pilot Error", "", 'RADIOBUT_OFF', CompetitorAttackType.ERROR.value),
        (CompetitorAttackType.EVENT.name, "Special Event", "", 'RADIOBUT_OFF', CompetitorAttackType.EVENT.value),
        None,
        (CompetitorAttackType.DETACH.name, "Detach", "", 'TRIA_UP', CompetitorAttackType.DETACH.value),
        (CompetitorAttackType.HIT_FRONT.name, "Hit Front", "", 'TRIA_UP', CompetitorAttackType.HIT_FRONT.value),
        None,
        (CompetitorAttackType.TAIL_LEFT.name, "Tail Left", "", 'TRIA_LEFT', CompetitorAttackType.TAIL_LEFT.value),
        (CompetitorAttackType.PASS_LEFT.name, "Pass Left", "", 'TRIA_LEFT', CompetitorAttackType.PASS_LEFT.value),
        (CompetitorAttackType.HIT_LEFT.name, "Hit Left", "", 'TRIA_LEFT', CompetitorAttackType.HIT_LEFT.value),
        None,
        (CompetitorAttackType.TAIL_RIGHT.name, "Tail Right", "", 'TRIA_RIGHT', CompetitorAttackType.TAIL_RIGHT.value),
        (CompetitorAttackType.PASS_RIGHT.name, "Pass Right", "", 'TRIA_RIGHT', CompetitorAttackType.PASS_RIGHT.value),
        (CompetitorAttackType.HIT_RIGHT.name, "Hit Right", "", 'TRIA_RIGHT', CompetitorAttackType.HIT_RIGHT.value),
        None,
        (CompetitorAttackType.ZIGZAG.name, "Zig Zag", "", 'TRIA_DOWN', CompetitorAttackType.ZIGZAG.value),
        (CompetitorAttackType.ACCEL.name, "Accelerate", "", 'TRIA_DOWN', CompetitorAttackType.ACCEL.value),
        (CompetitorAttackType.BRAKE.name, "Brake", "", 'TRIA_DOWN', CompetitorAttackType.BRAKE.value),
    ))
    context: bpy.props.EnumProperty(name="Context", options={'ENUM_FLAG'}, items=(
        (CompetitorAttackContext.FRONT.name, "F", "Front", 'TRIA_UP', CompetitorAttackContext.FRONT.value),
        (CompetitorAttackContext.LEFT.name, "L", "Left", 'TRIA_LEFT', CompetitorAttackContext.LEFT.value),
        (CompetitorAttackContext.RIGHT.name, "R", "Right", 'TRIA_RIGHT', CompetitorAttackContext.RIGHT.value),
        (CompetitorAttackContext.REAR.name, "B", "Behind", 'TRIA_DOWN', CompetitorAttackContext.REAR.value),
    ))
    percentage: bpy.props.IntProperty(name="Probability", subtype='PERCENTAGE', min=0, max=100, step=5)


class POD_CircuitCompetitor(bpy.types.PropertyGroup):
    difficulty: bpy.props.EnumProperty(name="Difficulty", default=utils.Dif.EASY.name, items=(
        (utils.Dif.EASY.name, "E", "Easy", utils.Dif.EASY.value),
        (utils.Dif.MEDIUM.name, "M", "Medium", utils.Dif.MEDIUM.value),
        (utils.Dif.HARD.name, "H", "Hard", utils.Dif.HARD.value),
    ))
    name: bpy.props.StringProperty(name="Name", maxlen=8)
    level: bpy.props.IntProperty(name="Level", subtype='FACTOR', min=0, max=9)
    behavior: bpy.props.EnumProperty(name="Behavior", default=CompetitorBehavior.PASSIVE.name, items=(
        (CompetitorBehavior.PASSIVE.name, "Passive", "", CompetitorBehavior.PASSIVE.value),
        (CompetitorBehavior.DEFENSIVE.name, "Defensive", "", CompetitorBehavior.DEFENSIVE.value),
        (CompetitorBehavior.AGGRESSIVE.name, "Aggressive", "", CompetitorBehavior.AGGRESSIVE.value),
    ))
    car_index: bpy.props.IntProperty(name="Car Index", subtype='FACTOR', default=1, min=1, max=7)
    attacks: bpy.props.CollectionProperty(name="Attacks", type=POD_CircuitCompetitorAttack)


class POD_CircuitScene(bpy.types.PropertyGroup):
    patcher: bpy.props.PointerProperty(type=POD_CircuitPatcher)
    pits: bpy.props.PointerProperty(type=POD_CircuitPits)
    times: bpy.props.PointerProperty(type=POD_CircuitTimes)
    phases: bpy.props.PointerProperty(type=POD_CircuitPhases)
    competitors: bpy.props.CollectionProperty(type=POD_CircuitCompetitor)


def register() -> None:
    bpy.types.Scene.pod_circuit = bpy.props.PointerProperty(type=POD_CircuitScene)


def unregister() -> None:
    del bpy.types.Scene.pod_circuit
