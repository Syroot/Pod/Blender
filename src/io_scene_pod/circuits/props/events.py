from typing import Sequence, Union

import bpy

from ...common.props.sound import POD_Sound
from .. import utils
from ..utils import EventName


def _build_items(source: str) -> Sequence[tuple[str, str, str, str, int] | None]:
    items = []

    def add(type: EventName, text: str, icon: str):
        items.append((type.name, text, "", icon, type.value))

    # Add condition types.
    if source == "BEACON":
        add(EventName.COND_COMPETITOR, "If Competitor", 'SYSTEM')
        add(EventName.COND_PLAYER, "If Player", 'SYSTEM')
        add(EventName.COND_LAP, "If Lap", 'SYSTEM')
    add(EventName.COND_PROB, "If Probability", 'SYSTEM')
    add(EventName.COND_BEGIN, "Then", 'SYSTEM')
    add(EventName.COND_ELSE, "Else", 'SYSTEM')
    add(EventName.COND_END, "End If", 'SYSTEM')
    add(EventName.INT_BREAK, "Break", 'SYSTEM')
    # Add animation types.
    items.append(None)
    add(EventName.ANI_FIX, "Fix Animation", 'ANIM')
    add(EventName.ANI_START, "Start Animation", 'ANIM')
    add(EventName.ANI_PAUSE, "Pause Animation", 'ANIM')
    if source == "ANIM":
        add(EventName.ANI_BLOCK, "Block Animation", 'ANIM')
        add(EventName.ANI_MOVE, "Move Animation", 'ANIM')
        add(EventName.ANI_RESTART, "Restart Animation", 'ANIM')
        add(EventName.ANI_RESET, "Reset Animation", 'ANIM')
        add(EventName.ANI_STOP, "Stop Animation", 'ANIM')
    # Add timing types.
    if source == "BEACON":
        items.append(None)
        add(EventName.TIME_P_PART, "Part Checkpoint", 'PREVIEW_RANGE')
        add(EventName.TIME_P_LAP, "Lap Checkpoint", 'PREVIEW_RANGE')
        add(EventName.TIME_N_PART, "Undo Part Checkpoint", 'PREVIEW_RANGE')
        add(EventName.TIME_N_LAP, "Undo Lap Checkpoint", 'PREVIEW_RANGE')
    # Add macro types.
    items.append(None)
    add(EventName.MACRO_ALL_OFF, "Enable All Macros", 'SETTINGS')
    add(EventName.MACRO_ALL_ON, "Disable All Macros", 'SETTINGS')
    add(EventName.MACRO_ENABLE, "Enable Macro", 'SETTINGS')
    add(EventName.MACRO_DISABLE, "Disable Macro", 'SETTINGS')
    add(EventName.MACRO_EXCHANGE, "Exchange Macro", 'SETTINGS')
    add(EventName.MACRO_INIT, "Init Macro", 'SETTINGS')
    add(EventName.MACRO_TRIGGER, "Trigger Macro", 'SETTINGS')
    add(EventName.MACRO_REPLACE, "Replace Macro", 'SETTINGS')
    # Add sound types.
    items.append(None)
    if source == "BEACON":
        add(EventName.SOUND_REVERB_UPDATE, "Update Reverb", 'PLAY_SOUND')
    elif source == "PHASE":
        add(EventName.SOUND_REVERB_RESET, "Reset Reverb", 'PLAY_SOUND')
    add(EventName.SOUND_PLAY_LOCAL, "Play Local Sound", 'PLAY_SOUND')
    add(EventName.SOUND_PLAY_SOURCE, "Play Source Sound", 'PLAY_SOUND')
    add(EventName.SOUND_PLAY_STATIC, "Play Static Sound", 'PLAY_SOUND')
    add(EventName.SOUND_STOP_LOCAL, "Stop Local Sound", 'PLAY_SOUND')
    add(EventName.SOUND_STOP_SOURCE, "Stop Source Sound", 'PLAY_SOUND')
    add(EventName.SOUND_STOP_STATIC, "Stop Static Sound", 'PLAY_SOUND')
    # Add race types.
    if source == "BEACON":
        items.append(None)
        add(EventName.RACE_CONSTRAINT, "Constraint", 'AUTO')
        add(EventName.RACE_DECIDE_PIT, "Decide Pit", 'AUTO')
        add(EventName.RACE_SECTION, "Section", 'AUTO')
        add(EventName.RACE_FINISH_ROLLING, "End Rolling Start", 'AUTO')
        add(EventName.RACE_FINISH_COURSE, "End Course", 'AUTO')
        add(EventName.RACE_SHORTCUT, "Allow Shortcuts", 'AUTO')
        add(EventName.RACE_TUNNEL_ENTER, "Enter Tunnel", 'AUTO')
        add(EventName.RACE_TUNNEL_EXIT, "Exit Tunnel", 'AUTO')
        add(EventName.RACE_DAMAGE_ON, "Enable Damage", 'AUTO')
        add(EventName.RACE_DAMAGE_OFF, "Disable Damage", 'AUTO')
        add(EventName.RACE_WRONGWAY_ON, "Enable Wrong Way", 'AUTO')
        add(EventName.RACE_WRONGWAY_OFF, "Disable Wrong Way", 'AUTO')
        add(EventName.RACE_CAMERA_CEILING, "Limit Camera Height", 'AUTO')
        add(EventName.RACE_CAMERA_FREE, "Free Camera Height", 'AUTO')
        add(EventName.INT_ENV_ON, "Show Environment", 'AUTO')
        add(EventName.INT_ENV_OFF, "Hide Environment", 'AUTO')
    return items


class POD_CircuitEventParams(bpy.types.PropertyGroup):
    def speaker_poll(self, ob: bpy.types.Object) -> bool:
        return utils.is_speaker(ob)

    camera_ceiling: bpy.props.IntProperty(name="Max. Ground Distance")
    cond_lap: bpy.props.EnumProperty(name="Laps", options={'ENUM_FLAG'}, items=(
        ('LAP1', "1", "Lap 1", 1 << 0),
        ('LAP2', "2", "Lap 2", 1 << 1),
        ('LAP3', "3", "Lap 3", 1 << 2),
        ('LAP4', "4", "Lap 4", 1 << 3),
        ('LAP5', "5", "Lap 5", 1 << 4),
        ('LAP6', "6", "Lap 6", 1 << 5),
        ('LAP7', "7", "Lap 7", 1 << 6),
        ('LAP8', "8", "Lap 8", 1 << 7),
    ))
    cond_prob: bpy.props.IntProperty(name="Probability", min=0, soft_max=100, subtype="PERCENTAGE")
    sound_effect: bpy.props.EnumProperty(name="Sound Effect", items=(
        ('EFFECT_8', "Stop Reverb (8)", "", 8),
        ('EFFECT_9', "Start Reverb (9)", "", 9),
        ('EFFECT_10', "Start Reverb (11)", "", 11),
    ))
    sound: bpy.props.PointerProperty(type=POD_Sound)
    speaker: bpy.props.PointerProperty(name="Speaker", type=bpy.types.Object, poll=speaker_poll)


class POD_CircuitAnimEvent(bpy.types.PropertyGroup):
    type: bpy.props.EnumProperty(name="Type", default=EventName.INT_BREAK.name, items=_build_items("ANIM"))
    params: bpy.props.PointerProperty(type=POD_CircuitEventParams)


class POD_CircuitBeaconEvent(bpy.types.PropertyGroup):
    type: bpy.props.EnumProperty(name="Type", default=EventName.INT_BREAK.name, items=_build_items("BEACON"))
    params: bpy.props.PointerProperty(type=POD_CircuitEventParams)


class POD_CircuitPhaseEvent(bpy.types.PropertyGroup):
    type: bpy.props.EnumProperty(name="Type", default=EventName.INT_BREAK.name, items=_build_items("PHASE"))
    params: bpy.props.PointerProperty(type=POD_CircuitEventParams)


POD_CircuitEvent = Union[POD_CircuitAnimEvent | POD_CircuitBeaconEvent | POD_CircuitPhaseEvent]
