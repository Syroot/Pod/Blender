import bpy


class POD_CircuitWorld(bpy.types.PropertyGroup):
    def sun_texture_poll(self, img: bpy.types.Image) -> bool:
        return img.size[0] == 128 and img.size[1] == 128

    fog_distance: bpy.props.IntProperty(name="Fog Distance")
    fog_intensity: bpy.props.IntProperty(name="Fog Intensity")
    fog_mist_z: bpy.props.IntProperty(name="Fog Mist Z")
    fog_mist_intensity: bpy.props.IntProperty(name="Fog Mist Intensity")
    bg_on: bpy.props.BoolProperty(name="Background Enabled")
    bg_color: bpy.props.EnumProperty(name="Background Color", items=(
        ('BLACK', "Black", "", 0),
        ('RED', "Red", "", 1),
        ('GREEN', "Green", "", 2),
        ('BLUE', "Blue", "", 4),
        ('WHITE', "White", "", 7),
    ))
    bg_texture: bpy.props.PointerProperty(name="Background Texture", type=bpy.types.Image)
    bg_texture_start: bpy.props.IntProperty(name="Background Texture Start")
    bg_texture_end: bpy.props.IntProperty(name="Background Texture End")
    sky_on: bpy.props.BoolProperty(name="Sky Enabled")
    sky_type: bpy.props.EnumProperty(name="Sky Type", items=(
        ('GOURAUD', "Gouraud", "", 2),
        ('GOURAUD_TEXTURE', "Gouraud Texture", "", 1),
        ('GOURAUD_TEXTURE_FAST', "Gouraud Texture (Fast)", "", 3),
        ('SHADOW_PROJECTION', "Shadow Projection", "", 4),
    ))
    sky_z: bpy.props.IntProperty(name="Sky Z")
    sky_zoom: bpy.props.IntProperty(name="Sky Zoom")
    sky_gouraud_intensity: bpy.props.IntProperty(name="Sky Gouraud Intensity", min=-7000, max=7000)
    sky_gouraud_start: bpy.props.IntProperty(name="Sky Gouraud Intensity")
    sky_speed: bpy.props.IntProperty(name="Sky Speed")
    sky_texture: bpy.props.PointerProperty(name="Sky Texture", type=bpy.types.Image)
    sun_on: bpy.props.BoolProperty(name="Has Sun")
    sun_texture: bpy.props.PointerProperty(name="Sun Texture", type=bpy.types.Image, poll=sun_texture_poll)
    sun_color: bpy.props.FloatVectorProperty(name="Sun Color", subtype='COLOR', min=0.0, max=1.0)


def register() -> None:
    bpy.types.World.pod_circuit = bpy.props.PointerProperty(type=POD_CircuitWorld)


def unregister() -> None:
    del bpy.types.World.pod_circuit
