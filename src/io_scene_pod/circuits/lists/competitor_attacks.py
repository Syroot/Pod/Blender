import bpy

from ..props.scene import POD_CircuitCompetitor, POD_CircuitCompetitorAttack
from ..props.winman import POD_CircuitWinman


class POD_UL_CircuitCompetitorAttacks(bpy.types.UIList):
    def draw_item(self, context: bpy.types.Context, layout: bpy.types.UILayout,
                  data: POD_CircuitCompetitor, item: POD_CircuitCompetitorAttack, icon: int,
                  active_data: POD_CircuitWinman, active_propname: str,
                  index: int, flt_flags: int) -> None:
        self.use_filter_show = False
        cols = layout.column_flow(columns=3)
        cols.prop(item, "type", text="", emboss=False)
        cols.row().prop(item, "context", text="")
        cols.prop(item, "percentage", text="")
