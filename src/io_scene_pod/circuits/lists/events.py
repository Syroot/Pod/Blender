import bpy

from ... import butils
from ..ops.event_actions import POD_OT_CircuitEventActions
from ..props.events import POD_CircuitEvent, POD_CircuitEventParams
from ..utils import EventName


class POD_UL_CircuitEvents(bpy.types.UIList):
    def draw_item(self, context: bpy.types.Context, layout: bpy.types.UILayout,
                  data: bpy.types.AnyType, item: POD_CircuitEvent, icon: int,
                  active_data: bpy.types.AnyType, active_propname: str,
                  index: int, flt_flags: int) -> None:

        # Determine text that may be indented and prefixed for conditions.
        def is_cond(event: POD_CircuitEvent) -> bool:
            return EventName.COND_COMPETITOR <= EventName[event.type] <= EventName.COND_LAP

        events_propname = active_propname.removesuffix("_index")
        events: list[POD_CircuitEvent] = getattr(data, events_propname)
        indent = 0
        for i in range(index):
            if events[i].type == EventName.COND_BEGIN.name:
                indent += 1
            elif events[i].type == EventName.COND_END.name:
                indent = max(0, indent - 1)
        if item.type in (EventName.COND_ELSE.name, EventName.COND_END.name):
            indent = max(0, indent - 1)
        text = " " * indent * 4
        if index > 0 and is_cond(item) and is_cond(events[index - 1]):
            text += "And "
        text += butils.get_enum_name(item, "type")

        # Draw item.
        self.use_filter_show = False
        split = layout.split()
        type = split.row()
        type.prop(item, "type", icon_only=True)
        type.label(text=text)
        params: POD_CircuitEventParams = item.params
        if item.type == EventName.MACRO_TRIGGER.name:
            split.label(text="Not yet implemented")
        elif item.type == EventName.MACRO_ENABLE.name:
            split.label(text="Not yet implemented")
        elif item.type == EventName.MACRO_DISABLE.name:
            split.label(text="Not yet implemented")
        elif item.type == EventName.MACRO_REPLACE.name:
            split.label(text="Not yet implemented")
        elif item.type == EventName.MACRO_EXCHANGE.name:
            split.label(text="Not yet implemented")
        elif item.type == EventName.MACRO_INIT.name:
            split.label(text="Not yet implemented")
        elif item.type == EventName.COND_LAP.name:
            row = split.row()
            row.use_property_split = False
            row.prop(params, "cond_lap")
        elif item.type == EventName.COND_PROB.name:
            split.prop(params, "cond_prob", text="")
        elif item.type == EventName.ANI_MOVE.name:
            split.label(text="Not yet implemented")
        elif item.type == EventName.ANI_START.name:
            split.label(text="Not yet implemented")
        elif item.type == EventName.ANI_FIX.name:
            split.label(text="Not yet implemented")
        elif item.type == EventName.ANI_PAUSE.name:
            split.label(text="Not yet implemented")
        elif item.type in [EventName.SOUND_PLAY_SOURCE.name, EventName.SOUND_STOP_SOURCE.name,
                           EventName.SOUND_PLAY_STATIC.name, EventName.SOUND_STOP_STATIC.name]:
            row = split.row()
            row.use_property_split = False
            params.sound.draw(row)
        elif item.type in [EventName.SOUND_PLAY_LOCAL.name, EventName.SOUND_STOP_LOCAL.name]:
            split.prop(params, "speaker", text="", icon='OUTLINER_OB_SPEAKER')
        elif item.type in [EventName.SOUND_REVERB_RESET.name, EventName.SOUND_REVERB_UPDATE.name]:
            split.prop(params, "sound_effect", text="")
        elif item.type == EventName.RACE_CAMERA_CEILING.name:
            split.prop(params, "camera_ceiling", text="")


def draw_event_list(layout: bpy.types.UILayout, data: bpy.types.AnyType, propname: str) -> None:
    active_propname = propname + "_index"
    row = layout.row()
    list_id = POD_UL_CircuitEvents.__name__ + propname
    row.template_list(POD_UL_CircuitEvents.__name__, list_id, data, propname, data, active_propname, rows=4)
    col = row.column(align=True)
    col.context_pointer_set("pod_circuit_events_data", data)
    col.context_pointer_set("pod_circuit_events_prop", data.bl_rna.properties[propname])
    col.context_pointer_set("pod_circuit_events_active_prop", data.bl_rna.properties[active_propname])
    col.operator(POD_OT_CircuitEventActions.bl_idname, icon='ADD', text="").action = 'ADD'
    col.operator(POD_OT_CircuitEventActions.bl_idname, icon='REMOVE', text="").action = 'REMOVE'
    col.separator()
    col.operator(POD_OT_CircuitEventActions.bl_idname, icon='TRIA_UP', text="").action = 'UP'
    col.operator(POD_OT_CircuitEventActions.bl_idname, icon='TRIA_DOWN', text="").action = 'DOWN'
