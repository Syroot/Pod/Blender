import bpy

from ... import butils
from ..props.scene import POD_CircuitCompetitor, POD_CircuitScene
from ..props.winman import POD_CircuitWinman


class POD_UL_CircuitCompetitors(bpy.types.UIList):
    def draw_item(self, context: bpy.types.Context, layout: bpy.types.UILayout,
                  data: POD_CircuitScene, item: POD_CircuitCompetitor, icon: int,
                  active_data: POD_CircuitWinman, active_propname: str,
                  index: int, flt_flags: int) -> None:
        cols = layout.column_flow(columns=4)
        cols.prop(item, "name", text="", emboss=False)
        cols.prop(item, "level", text="")
        cols.prop(item, "behavior", text="")
        cols.prop(item, "car_index", text="")

    def filter_items(self, context: bpy.types.Context, data: POD_CircuitCompetitor, propname: str) -> tuple[list[int], list[int]]:
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        return butils.filter_list(self, data, propname, "name", lambda c: c.difficulty != wm_props.competitor_difficulty)
