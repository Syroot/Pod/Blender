import bpy

from ... import butils
from .. import utils
from ..ops.sector_toggle_zone import POD_OT_CircuitSectorToggleZone
from ..props.winman import POD_CircuitWinman


class POD_UL_CircuitSectorZones(bpy.types.UIList):
    def draw_item(self, context: bpy.types.Context, layout: bpy.types.UILayout,
                  data: bpy.types.Scene, item: bpy.types.Object, icon: int,
                  active_data: POD_CircuitWinman, active_propname: str,
                  index: int, flt_flags: int) -> None:
        row_icon = 'HIDE_ON'
        for zone in context.object.pod_circuit.sector.zones:
            if zone.ob == item:
                row_icon = 'HIDE_OFF'
                break
        row = layout.row(align=True)
        row.context_pointer_set("pod_circuit_sector_zone_ob", item)
        row.operator(POD_OT_CircuitSectorToggleZone.bl_idname, text="", icon=row_icon, emboss=False)
        row.prop(item, "name", text="", emboss=False)

    def filter_items(self, context: bpy.types.Context, data: bpy.types.Object, propname: str):
        return butils.filter_list(self, data, propname, "name",
                                  lambda ob: ob == context.object or not utils.is_sector(ob))
