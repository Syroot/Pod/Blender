import bpy
from mathutils import Vector

from .. import butils
from . import utils
from .props.object import POD_CircuitTrail


class Node:
    def __init__(self) -> None:
        self.beacon_ob: bpy.types.Object = None
        self.beacon_dir = 0
        self.trails: list[Trail] = []
        self.in_arcs: list[Arc] = []
        self.out_arcs: list[Arc] = []
        self.distance = 0.0
        self.neighbor: Neighbor | int = None
        self.subgraphs: list[list[Node]] = [None] * 5
        self.subgraph_count = 0
        self.total_out_arc_count = 0


class Trail:
    def __init__(self, ob: bpy.types.Object) -> None:
        self.ob = ob
        self.props: POD_CircuitTrail = None
        self.parent_node: Node = None
        # Calculate transformed points.
        assert isinstance(ob.data, bpy.types.Curve)
        curve: bpy.types.Curve = ob.data
        spline: bpy.types.Spline = curve.splines[0]
        self.points: list[Vector] = [ob.matrix_world @ p.co.xyz for p in spline.points]


class Arc:
    def __init__(self) -> None:
        self.parent_node: Node = None
        self.child_node: Node = None
        self.trails: list[Trail] = []
        self.distance_trail: Trail = None
        self.distances: list[float] = None

    def set_distance_trail(self, trail: Trail) -> None:
        self.distance_trail = trail
        self.distances = [0.0] * len(trail.points)


class Ppr:
    def __init__(self, arc: Arc = None, point: int = 0) -> None:
        self.arc = arc
        self.point = point

    def get_next_pos(self) -> Vector:
        if len(self.arc.distances) - 1 > self.point:
            return self.arc.distance_trail.points[self.point + 1]
        else:
            child_node = self.arc.child_node
            if len(child_node.out_arcs) == 1:
                return child_node.out_arcs[0].distance_trail.points[0]
            else:
                return child_node.beacon_ob.location


class Cp:
    def __init__(self) -> None:
        self.beacon_ob: bpy.types.Object = None
        self.beacon_dir = 0
        self.distance = 0


class Neighbor:
    def __init__(self) -> None:
        self.initial_node: Node = None
        self.final_node: Node = None
        self.parent: Neighbor | int = None


class Graph:
    def __init__(self, context: bpy.types.Context, op: bpy.types.Operator, reverse: bool, dif: utils.Dif) -> None:
        self.context = context
        self.op = op
        self.dif = dif
        self.beacon_obs: list[bpy.types.Object] = [x for x in context.scene.objects if utils.is_beacon(x, reverse)]
        self.trail_obs: list[bpy.types.Object] = [x for x in context.scene.objects if utils.is_trail(x, reverse, dif)]
        self.nodes: list[Node] = []
        self.arcs: list[Arc] = []
        self.cps: list[Cp] = []
        self.neighbors: list[Neighbor] = []
        self.circuit_length = 0.0

        self._create_nodes()
        self._create_arcs()
        self._find_distance_trails()
        self._find_cps()
        self._calc_arc_distances(self._find_start_ppr())
        self._calc_cp_distances()
        self._calc_neighbors()

    def _create_nodes(self) -> None:
        # Create nodes for each positive or negative section beacon event.
        for beacon_ob in self.beacon_obs:
            for beacon_dir in [1, -1]:
                if utils.get_beacon_event_dir(beacon_ob, 'RACE_SECTION', beacon_dir):
                    node = Node()
                    self.nodes.append(node)
                    node.beacon_ob = beacon_ob
                    node.beacon_dir = beacon_dir
                    # Add trails set to start with the beacon.
                    for trail_ob in self.trail_obs:
                        trail_props: POD_CircuitTrail = trail_ob.pod_circuit.trail
                        trail_beacon_dir = butils.get_enum_value(trail_props, "beacon_direction")
                        if trail_props.beacon == beacon_ob and trail_beacon_dir == beacon_dir:
                            trail = Trail(trail_ob)
                            trail.props = trail_props
                            trail.parent_node = node
                            node.trails.append(trail)

    def _create_arcs(self) -> None:
        def add_trail_to_arc(node: Node, out_node: Node, trail: Trail) -> None:
            def can_add(trail: Trail) -> bool:
                return trail.props.graph == 'ADDITIONAL' or trail.props.graph != 'IGNORE'

            def can_merge(trail: Trail) -> bool:
                # Never merge additional or pit trails.
                if trail.props.graph == 'ADDITIONAL':
                    return False
                if trail.props.constraint_type == 'PIT' and trail.props.graph != 'IGNORE':
                    return False
                return True

            # Try to find an existing mergeable arc that leads to trail's out node.
            arc = None
            if can_merge(trail):
                for out_arc in node.out_arcs:
                    if out_arc.child_node == out_node and any((can_merge(t) for t in out_arc.trails)):
                        arc = out_arc
                        break
            # Create a new arc if none was found or merging is not allowed.
            if not arc and can_add(trail):
                arc = Arc()
                arc.parent_node = node
                arc.parent_node.out_arcs.append(arc)
                arc.child_node = out_node
                arc.child_node.in_arcs.append(arc)
                self.arcs.append(arc)
            if arc:
                arc.trails.append(trail)

        # Determine out nodes of trails.
        for node in self.nodes:
            for trail in node.trails:
                # Ignore trails without segments or start trails that are not additional.
                if len(trail.points) < 2 or trail.props.constraint_type == 'START' and trail.props.graph != 'ADDITIONAL':
                    continue
                # Check distance to node position (max. distance 150, does not verify height).
                end = trail.points[-1]
                dir = end - trail.points[-2]
                found = False
                max_dist = 0.0
                while not found and max_dist < 150.0:
                    max_dist += 1.0
                    for out_node in self.nodes:
                        if not out_node.trails or not out_node.trails[0].points:
                            continue
                        beacon_ob = out_node.beacon_ob
                        dist = abs(end.x - beacon_ob.location.x) + abs(end.y - beacon_ob.location.y)
                        if max_dist < dist:
                            continue
                        # Pass direction check to properly identify beacons with 2 sections.
                        _, beacon_normal = utils.get_beacon_model(self.context, beacon_ob)
                        d = dir.dot(beacon_normal)
                        if d < 0 and out_node.beacon_dir < 0 or d > 0 and out_node.beacon_dir > 0:
                            if found:
                                self.op.report({'WARNING'}, f"{trail.ob.name} ends in multiple sections.")
                            else:
                                add_trail_to_arc(node, out_node, trail)
                                found = True
                # Show warning for unconnected trails.
                if not found:
                    msg = trail.ob.name
                    if trail.props.constraint_type != 'NONE':
                        msg += f" ({butils.get_enum_name(trail.props, 'constraint_type')})"
                    msg += " does not end in a section."
                    self.op.report({'WARNING'}, msg)

    def _find_distance_trails(self) -> None:
        for arc in self.arcs:
            # Search for preferred trails.
            for trail in arc.trails:
                if trail.points:
                    if trail.props.level > (arc.distance_trail.props.level if arc.distance_trail else -1):
                        if trail.props.graph == 'PREFER':
                            arc.set_distance_trail(trail)
            # Otherwise consider unignored trails that aren't disengage.
            if not arc.distance_trail:
                for trail in arc.trails:
                    if trail.points:
                        if trail.props.level > (arc.distance_trail.props.level if arc.distance_trail else -1):
                            if trail.props.graph != 'IGNORE' and trail.props.constraint_type != 'DISENGAGE':
                                arc.set_distance_trail(trail)
            # Otherwise just use the first trail.
            if not arc.distance_trail:
                arc.set_distance_trail(arc.trails[0])

    def _find_cps(self) -> None:
        # Find positive lap or part beacons.
        for beacon_ob in self.beacon_obs:
            if event_dir := utils.get_beacon_event_dir(beacon_ob, 'TIME_P_LAP'):
                cp = Cp()
                cp.beacon_ob = beacon_ob
                cp.beacon_dir = event_dir
                self.cps.insert(0, cp)
            elif event_dir := utils.get_beacon_event_dir(beacon_ob, 'TIME_P_PART'):
                cp = Cp()
                cp.beacon_ob = beacon_ob
                cp.beacon_dir = event_dir
                self.cps.append(cp)

    def _find_start_ppr(self) -> Ppr:
        # Return last point on arc before crossing the lap beacon.
        lap_beacon_ob = self.cps[0].beacon_ob
        lap_beacon_dir = self.cps[0].beacon_dir
        lap_beacon_points, lap_beacon_normal = utils.get_beacon_model(self.context, lap_beacon_ob)
        for arc in self.arcs:
            # Find point and distance on an arc that are closest to the lap beacon.
            closest_dist = float("inf")
            for point in arc.distance_trail.points:
                dist0 = lap_beacon_points[0] - point
                dist1 = lap_beacon_points[1] - point
                max0 = max(abs(dist0.x), abs(dist0.y), abs(dist0.z))
                max1 = max(abs(dist1.x), abs(dist1.y), abs(dist1.z))
                closest_dist = min(closest_dist, max0, max1)
            if closest_dist < 100.0:
                # Find index of last point on trail that has not crossed the lap beacon.
                for i in range(len(arc.distances) - 1, -1, -1):
                    dist = (arc.distance_trail.points[i] - lap_beacon_points[0]).dot(lap_beacon_normal)
                    if lap_beacon_dir == 1 and dist < 0 or lap_beacon_dir == -1 and dist > 0:
                        break
                if i != -1:
                    start = arc.distance_trail.points[i]
                    if i == len(arc.distances) - 1:
                        # Get position on next arc. Ignoring node comparison here (which would typically be False).
                        end = Ppr(arc, i).get_next_pos()
                    else:
                        end = arc.distance_trail.points[i + 1]
                    if utils.get_beacon_pass_dir(lap_beacon_points, lap_beacon_normal, start, end):
                        return Ppr(arc, i)
        raise Exception(f"No {self.dif.name.title()} distance trail passes the lap beacon {lap_beacon_ob.name}.")

    def _calc_arc_distances(self, start_ppr: Ppr) -> None:
        # Find arc and beacon on which the lap event occurs.
        beacon_points, beacon_normal = utils.get_beacon_model(self.context, self.cps[0].beacon_ob)
        start_arc = start_ppr.arc
        start_point = start_ppr.point
        self._start_node = start_arc.parent_node
        # Calculate start arc distances from lap beacon to arc start.
        dist = abs((start_arc.distance_trail.points[start_point] - beacon_points[0]).dot(beacon_normal))
        start_arc.distances[start_point] = dist
        for i in range(start_point - 1, -1, -1):
            dist += (start_arc.distance_trail.points[i + 1] - start_arc.distance_trail.points[i]).length
            start_arc.distances[i] = dist

        # Calculate other arc distances.
        def calc_arc(arc: Arc, handled_nodes: list[Node]) -> None:
            # Calculate remaining distance from this arc to its parent node.
            node = arc.parent_node
            dist = arc.distances[0] + (node.beacon_ob.location - arc.distance_trail.points[0]).length
            # Continue with the arc resulting in largest distance on node (if it was not processed yet).
            if dist > node.distance and node not in handled_nodes:
                node.distance = dist
                handled_nodes.append(node)
                # Go through the in-arcs connecting to this node.
                for in_arc in node.in_arcs:
                    if in_arc == start_arc:
                        continue
                    arc_dist = dist
                    # Determine distance from child node to last point on in-arc.
                    arc_dist += (in_arc.distance_trail.points[-1] - node.beacon_ob.location).length
                    in_arc.distances[-1] = arc_dist
                    # Calculate remaining distances on in-arc.
                    for i in range(len(in_arc.distances) - 2, -1, -1):
                        arc_dist += (in_arc.distance_trail.points[i + 1] - in_arc.distance_trail.points[i]).length
                        in_arc.distances[i] = arc_dist
                    # Continue calculating distances for the parent node of the in-arc (if not ending in itself).
                    if in_arc.parent_node != node:
                        calc_arc(in_arc, handled_nodes)
                handled_nodes.pop()

        calc_arc(start_arc, [])

        # Calculate start arc distances from arc end to lap beacon.
        dist = start_arc.child_node.distance
        if len(start_arc.distances) - 1 > start_point:
            start_arc.distances[-1] = dist + 1 / (1 << 16)
            for i in range(len(start_arc.distances) - 2, start_point, -1):
                dist += (start_arc.distance_trail.points[i + 1] - start_arc.distance_trail.points[i]).length
                start_arc.distances[i] = dist

        # Calculate total circuit length.
        if len(start_arc.distances) - 1 > start_point:
            dist += abs((start_arc.distance_trail.points[start_point + 1] - beacon_points[0]).dot(beacon_normal))
        if dist > self.circuit_length:
            self.circuit_length = dist

        # Calculate corrected start arc distances from lap beacon to arc end.
        dist = self.circuit_length
        if len(start_arc.distances) - 1 > start_point:
            dist -= abs((start_arc.distance_trail.points[start_point + 1] - beacon_points[0]).dot(beacon_normal))
            start_arc.distances[start_point + 1] = dist
            for i in range(start_point + 2, len(start_arc.distances)):
                dist -= (start_arc.distance_trail.points[i] - start_arc.distance_trail.points[i - 1]).length
                start_arc.distances[i] = dist

        # Fill trailing arc distances.
        def set_node_distance(node: Node, dist: float) -> None:
            node.distance = dist
            for in_arc in node.in_arcs:
                in_arc.distances = [dist] * len(in_arc.distances)
                if not in_arc.parent_node.distance:
                    set_node_distance(in_arc.parent_node, dist)

        for node in self.nodes:
            if not len(node.out_arcs) and len(node.in_arcs) > 0:
                # Find previous node that has a distance.
                dist_node = node
                while len(dist_node.in_arcs) > 0 and not dist_node.distance:
                    dist_node = dist_node.in_arcs[0].parent_node
                if len(dist_node.in_arcs):
                    set_node_distance(node, dist_node.distance)

    def _calc_cp_distances(self) -> None:
        def calc_cp(cp: Cp) -> float:
            beacon_points, beacon_normal = utils.get_beacon_model(self.context, cp.beacon_ob)
            arc = self.arcs[0]
            while arc:
                # Check checkpoint beacon intersection between two points on arc.
                point = arc.distance_trail.points[0]
                for i in range(len(arc.distances) - 1):
                    point = arc.distance_trail.points[i]
                    dir = utils.get_beacon_pass_dir(beacon_points, beacon_normal, point, arc.distance_trail.points[i + 1])
                    if dir == cp.beacon_dir:
                        return (arc.distances[i + 1] + arc.distances[i]) / 2.0
                # Check checkpoint beacon intersection between this and out-arc.
                if not len(arc.child_node.out_arcs):
                    return 0
                prev_last_dist = arc.distances[i]
                arc = arc.child_node.out_arcs[0]
                dir = utils.get_beacon_pass_dir(beacon_points, beacon_normal, point, arc.distance_trail.points[0])
                if dir == cp.beacon_dir:
                    return (arc.distances[0] + prev_last_dist) / 2.0
            return 0.0

        self.cps[0].distance = self.circuit_length
        for cp in self.cps[1:]:
            cp.distance = calc_cp(cp)
        self.cps[1:] = sorted(self.cps[1:], key=lambda x: x.distance, reverse=True)

    def _calc_neighbors(self) -> None:
        # Initialize nodes as needed.
        def init_node(node: Node) -> None:
            node.subgraph_count += 1
            if node.subgraph_count >= len(node.in_arcs):
                node.neighbor = -1
                for out_arc in node.out_arcs:
                    init_node(out_arc.child_node)

        for node in self.nodes:
            if not len(node.in_arcs):
                init_node(node)

        # Get a node to start iteration with, if any is needed.
        def get_start_node() -> Node | None:
            node = self._start_node
            found = False
            handled = False
            handled_nodes: set[Node] = set()
            while not found and len(handled_nodes) < len(self.nodes):
                while not found and not handled:
                    # Find unhandled node with more than 1 in-arc and at least 1 out-arc.
                    while not node in handled_nodes and len(node.in_arcs) > 1 and len(node.out_arcs) > 0:
                        handled_nodes.add(child_node)
                        # Fix self-referencing nodes. Seems to be a hack with inaccurate length check.
                        if node.out_arcs[0].child_node == node and len(node.out_arcs) > 0:
                            node = node.out_arcs[1].child_node
                        else:
                            node = node.out_arcs[0].child_node
                    handled = node in handled_nodes
                    if not handled:
                        # Skip forward to a node with more than 1 in- and out-arc (mark others as handled).
                        child_node = node
                        while not child_node in handled_nodes and len(child_node.in_arcs) == 1 and len(child_node.out_arcs) == 1:
                            handled_nodes.add(child_node)
                            child_node = child_node.out_arcs[0].child_node
                        found = len(child_node.in_arcs) == 1
                        if len(child_node.in_arcs) != 1:
                            node = child_node
                        handled_nodes.add(child_node)
                # If nothing found, take first unhandled node.
                if not found and len(handled_nodes) < len(self.nodes):
                    for it in self.nodes:
                        if it not in handled_nodes:
                            node = it
                            break
                    handled = False
            return node if found else None

        self.start_node = get_start_node()
        if not self.start_node:
            return

        # Iterate over nodes.
        queued_nodes: set[Node] = set()
        handled_nodes = []

        def queue_node(nodes: list[Node], node: Node) -> None:
            def queue_list(last_node: Node, nodes: list[Node]) -> None:
                for node in reversed(nodes):
                    queued_nodes.add(node)
                    if node == last_node:
                        break

            if node in nodes:
                queue_list(node, nodes)
            else:
                parent_nodes = nodes.copy()
                parent_nodes.append(node)
                # Iterate backwards over nodes.
                while len(node.in_arcs) == 1:
                    node = node.in_arcs[0].parent_node
                    if node == self.start_node:
                        return
                    if node in nodes:
                        queue_list(node, nodes)
                        return
                    parent_nodes.append(node)
                # Recursively queue parent nodes until start is reached again.
                for in_arc in node.in_arcs:
                    if in_arc.parent_node != self.start_node:
                        queue_node(parent_nodes, in_arc.parent_node)

        def handle_node(nodes: list[Node], node: Node) -> None:
            def finish_node(node: Node, final_node: Node) -> None:
                node.total_out_arc_count -= 1
                if node.total_out_arc_count >= 0:
                    node.neighbor.final_node = final_node

            def create_neighbor(parent_nodes: list[Node], initial_node: Node) -> None:
                neighbor = Neighbor()
                neighbor.initial_node = initial_node
                self.neighbors.append(neighbor)
                initial_node.neighbor = neighbor
                if len(parent_nodes):
                    neighbor.parent = parent_nodes[-1].neighbor
                else:
                    neighbor.parent = -1

            # If node was already handled, it is the final node of all nodes in the list.
            if node in handled_nodes:
                for it in nodes:
                    finish_node(it, node)
                return
            handled_nodes.append(node)

            if node.neighbor and (node.neighbor == -1 or node.neighbor.final_node):
                handled_nodes.pop()
                return

            parent_nodes: list[Node] = []
            other_nodes: set[Node] = set()
            final_nodes: list[Node] = []
            if len(node.in_arcs) <= 1:
                parent_nodes = nodes.copy()
            else:
                node.subgraphs[node.subgraph_count] = nodes.copy()
                node.subgraph_count += 1
                if node.subgraph_count < len(node.in_arcs):
                    handled_nodes.pop()
                    return
                # Iterate subgraphs of node.
                for i in range(len(node.in_arcs)):
                    for subnode in node.subgraphs[i] or []:
                        if subnode not in final_nodes:
                            final_nodes.append(subnode)
                        # Find other subgraph.
                        for j in range(i + 1, len(node.in_arcs)):
                            for other_subnode in node.subgraphs[j] or []:
                                if other_subnode not in other_nodes and subnode == other_subnode:
                                    finish_node(subnode, node)
                        other_nodes.add(subnode)
                for final_node in final_nodes:
                    if final_node.total_out_arc_count > 0:
                        parent_nodes.append(final_node)

            if len(node.out_arcs):
                if len(node.out_arcs) > 1:
                    create_neighbor(parent_nodes, node)
                    parent_nodes.append(node)
                elif len(parent_nodes):
                    node.neighbor = parent_nodes[len(parent_nodes) - 1].neighbor if len(parent_nodes) else -1
                else:
                    node.neighbor = -1
                for it in parent_nodes:
                    it.total_out_arc_count += len(node.out_arcs) - 1
                for out_arc in node.out_arcs:
                    handle_node(parent_nodes, out_arc.child_node)
                handled_nodes.pop()
            else:
                for parent_node in parent_nodes:
                    finish_node(parent_node, node)
                node.neighbor = parent_nodes[len(parent_nodes) - 1].neighbor if len(parent_nodes) else -1
                handled_nodes.pop()

        queue_node([], self.start_node)

        handle_node([], self.start_node)
        for node in self.nodes:
            if not node.neighbor and node in queued_nodes and node.subgraph_count:
                node.subgraph_count = len(node.in_arcs) - 1
                handle_node([], node)

        # Validate all neighbors.
        for n, node in enumerate(self.nodes):
            if not node.neighbor:
                node.neighbor = -1
                self.op.report({'WARNING'}, f"Neighbor of node {n} not initialized.")
            elif node.neighbor != -1 and not node.neighbor.final_node:
                self.op.report({'WARNING'}, f"Neighbor of node {n} without a final node.")

        # Configure for export.
        for node in self.nodes:
            if node.neighbor == -1:
                node.neighbor = None
        for neighbor in self.neighbors:
            if neighbor.parent == -1:
                neighbor.parent = None
