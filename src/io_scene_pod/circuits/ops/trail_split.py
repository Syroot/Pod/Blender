from typing import Union
import bpy

from ... import butils
from .. import utils


class POD_OT_CircuitTrailSplit(bpy.types.Operator):
    """Splits the trail at beacons with section events"""
    bl_idname = "object.pod_circuit_trail_split"
    bl_label = "Split Sections"
    bl_options = {'UNDO'}

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        ob = context.object
        return ob != None and ob.pod_circuit.type == 'TRAIL'

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        beacon_ob = context.object
        spline: bpy.types.Spline = beacon_ob.data.splines[0]
        trail = beacon_ob.pod_circuit.trail
        col = beacon_ob.users_collection[0]
        dir_suffix = "R" if trail.reverse else "F"
        difs_suffix = "".join(utils.Dif(y).name[0] for y in sorted(utils.Dif[x].value for x in trail.difficulties))
        lap = 0
        section = 0

        def create_trail(start: int, stop: int, beacon: bpy.types.Object, dir: int) -> None:
            # Split into new curve.
            name = f"Trail{dir_suffix}{difs_suffix}{lap}s{section}"
            section_curve = bpy.data.curves.new(name, 'CURVE')
            section_curve.dimensions = '3D'
            section_spline = section_curve.splines.new('POLY')
            section_spline.points.add(stop - start)
            origin = spline.points[start].co.xyz
            for dst, src in enumerate(range(start, stop + 1)):
                section_spline.points[dst].co.xyz = spline.points[src].co.xyz - origin
                section_spline.points[dst].co.w = 1.0
            # Create object and copy properties.
            section_ob = bpy.data.objects.new(name, section_curve)
            section_ob.pod_circuit.type = 'TRAIL'
            section_ob.location = origin
            section_trail = section_ob.pod_circuit.trail
            section_trail.reverse = trail.reverse
            section_trail.difficulties = trail.difficulties
            if beacon:
                section_trail.beacon = beacon
                butils.set_enum_value(section_trail, "beacon_direction", dir)
            section_trail.level = trail.level
            section_trail.constraint_type = trail.constraint_type
            section_trail.constraint_param = trail.constraint_param
            section_trail.constraint_number = trail.constraint_number
            section_trail.graph = trail.graph
            col.objects.link(section_ob)

        # Split curve at section beacons and conut approximate lap number.
        beacon_obs = [ob for ob in context.scene.objects if utils.is_beacon(ob, trail.reverse)
                      and (utils.get_beacon_event_dir(ob, 'TIME_P_LAP') or utils.get_beacon_event_dir(ob, 'RACE_SECTION'))]
        prev_stop = 0
        prev_beacon_ob = None
        prev_dir = 0
        pass_lap = False
        for i in range(1, len(spline.points)):
            for beacon_ob in beacon_obs:
                beacon_points, beacon_normal = utils.get_beacon_model(context, beacon_ob)
                start = spline.points[i - 1].co.xyz
                end = spline.points[i].co.xyz
                dir = utils.get_beacon_pass_dir(beacon_points, beacon_normal, start, end)
                if dir:
                    if utils.get_beacon_event_dir(beacon_ob, 'TIME_P_LAP', dir):
                        pass_lap = True
                    if utils.get_beacon_event_dir(beacon_ob, 'RACE_SECTION', dir):
                        create_trail(prev_stop, i - 1, prev_beacon_ob, prev_dir)
                        prev_stop = i
                        prev_beacon_ob = beacon_ob
                        prev_dir = dir
                        if pass_lap:
                            lap += 1
                            section = 0
                            pass_lap = False
                        else:
                            section += 1
        # Split remaining points.
        if prev_stop and prev_stop != i:
            create_trail(prev_stop, i, prev_beacon_ob, prev_dir)
        return {'FINISHED'}
