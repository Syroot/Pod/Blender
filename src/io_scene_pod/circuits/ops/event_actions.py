from typing import Union

import bpy

from ..props.events import POD_CircuitEvent


class POD_OT_CircuitEventActions(bpy.types.Operator):
    bl_idname = "scene.pod_scene_event_actions"
    bl_label = "Event Actions"
    bl_description = "Add, remove, or sort event items"
    bl_options = {'INTERNAL', 'UNDO'}
    action: bpy.props.EnumProperty(items=(
        ('ADD', "Add", ""),
        ('REMOVE', "Remove", ""),
        ('UP', "Up", ""),
        ('DOWN', "Down", ""),
    ))

    def invoke(self, context: bpy.types.Context, event: bpy.types.Event) -> Union[set[int], set[str]]:
        data: bpy.types.AnyType = context.pod_circuit_events_data
        events: bpy.types.bpy_prop_collection_idprop[POD_CircuitEvent] = getattr(data, context.pod_circuit_events_prop.identifier)
        index: int = getattr(data, context.pod_circuit_events_active_prop.identifier)

        if self.action == 'ADD':
            events.add()
            if len(events) > 1:
                index += 1
                events.move(len(events) - 1, index)
        elif 0 <= index < len(events):
            if self.action == 'REMOVE':
                events.remove(index)
                if index >= len(events):
                    index = max(0, index - 1)
            elif self.action == 'UP' and index > 0:
                events.move(index, index - 1)
                index -= 1
            elif self.action == 'DOWN' and index < len(events) - 1:
                events.move(index, index + 1)
                index += 1

        setattr(data, context.pod_circuit_events_active_prop.identifier, index)
        return {'FINISHED'}
