from typing import Union

import bpy

from ..props.scene import POD_CircuitCompetitor, POD_CircuitScene
from ..props.winman import POD_CircuitWinman


class POD_OT_CircuitCompetitorRemove(bpy.types.Operator):
    """Removes the selected competitor"""
    bl_idname = "object.pod_circuit_competitor_remove"
    bl_label = "Remove Competitor"
    bl_options = {'INTERNAL', 'UNDO'}

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        props: POD_CircuitScene = context.scene.pod_circuit
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        if 0 <= wm_props.competitor_index < len(props.competitors):
            competitor: POD_CircuitCompetitor = props.competitors[wm_props.competitor_index]
            return competitor.difficulty == wm_props.competitor_difficulty
        return False

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        props: POD_CircuitScene = context.scene.pod_circuit
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        props.competitors.remove(wm_props.competitor_index)
        return {'FINISHED'}
