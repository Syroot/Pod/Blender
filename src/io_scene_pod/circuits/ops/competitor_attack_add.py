from typing import Union

import bpy

from ..props.scene import POD_CircuitCompetitor, POD_CircuitScene
from ..props.winman import POD_CircuitWinman


class POD_OT_CircuitCompetitorAttackAdd(bpy.types.Operator):
    """Adds an attack to the current competitor"""
    bl_idname = "object.pod_circuit_competitor_attack_add"
    bl_label = "Add Competitor Attack"
    bl_options = {'INTERNAL', 'UNDO'}

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        # Do not allow more than 10 attacks per competitor.
        props: POD_CircuitScene = context.scene.pod_circuit
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        competitor: POD_CircuitCompetitor = props.competitors[wm_props.competitor_index]
        return len(competitor.attacks) < 10

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        props: POD_CircuitScene = context.scene.pod_circuit
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        competitor: POD_CircuitCompetitor = props.competitors[wm_props.competitor_index]
        competitor.attacks.add()
        wm_props.competitor_attack_index = len(competitor.attacks) - 1
        return {'FINISHED'}
