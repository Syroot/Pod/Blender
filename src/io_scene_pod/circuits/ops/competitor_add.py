from typing import Union

import bpy

from ..props.scene import POD_CircuitCompetitor, POD_CircuitScene
from ..props.winman import POD_CircuitWinman


class POD_OT_CircuitCompetitorAdd(bpy.types.Operator):
    """Adds a competitor to the current difficulty"""
    bl_idname = "object.pod_circuit_competitor_add"
    bl_label = "Add Competitor"
    bl_options = {'INTERNAL', 'UNDO'}

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        # Do not allow more than 7 competitors per difficulty.
        props: POD_CircuitScene = context.scene.pod_circuit
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        count = sum(1 for c in props.competitors if c.difficulty == wm_props.competitor_difficulty)
        return count < 7

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        props: POD_CircuitScene = context.scene.pod_circuit
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        count = sum(1 for c in props.competitors if c.difficulty == wm_props.competitor_difficulty)
        competitor: POD_CircuitCompetitor = props.competitors.add()
        competitor.difficulty = wm_props.competitor_difficulty
        competitor.name = f"Player {count + 1}"
        wm_props.competitor_index = len(props.competitors) - 1
        return {'FINISHED'}
