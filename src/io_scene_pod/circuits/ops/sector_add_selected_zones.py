from typing import Union

import bpy

from .. import utils


class POD_OT_CircuitSectorAddSelectedZones(bpy.types.Operator):
    """Shows selected sectors when the car touches the active sector"""
    bl_idname = "object.pod_circuit_sector_add_selected_zones"
    bl_label = "Show Selected Pod Sectors"

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        ob = context.object
        for selected_ob in context.selected_objects:
            utils.add_sector_zone(ob, selected_ob)
        return {'FINISHED'}
