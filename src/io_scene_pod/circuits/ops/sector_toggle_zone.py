from typing import Union

import bpy


class POD_OT_CircuitSectorToggleZone(bpy.types.Operator):
    """Toggles whether a sector will be visible when the car touches the current sector"""
    bl_idname = "object.pod_circuit_sector_toggle_zone"
    bl_label = "Toggle Pod Sector Visibility"

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        ob = context.object
        zones = ob.pod_circuit.sector.zones
        zone_ob = context.pod_circuit_sector_zone_ob
        # Remove if visible.
        for i, zone in enumerate(zones):
            if zone.ob == zone_ob:
                zones.remove(i)
                return {'FINISHED'}
        # Add since not visible.
        zones.add().ob = zone_ob
        return {'FINISHED'}
