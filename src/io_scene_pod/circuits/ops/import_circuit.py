import struct
from typing import Union

import bmesh
import bpy
import bpy_extras
from mathutils import Matrix, Vector
from ubipod.circuit import (Circuit, CircuitConcurrent, CircuitDirection, CircuitDirectionDifficulty,
                            CompetitorBehavior, EventList, TimeFile)
from ubipod.pbdf import PbdfFormat, PbdfReader

from ... import butils, colors, objects, textures
from .. import utils
from ..props.events import POD_CircuitEvent
from ..props.scene import (POD_CircuitCompetitor, POD_CircuitPhases, POD_CircuitPhasesDirection, POD_CircuitPits,
                           POD_CircuitCompetitorAttack, POD_CircuitTimes, POD_CircuitTimesDirection)
from ..props.world import POD_CircuitWorld
from ..utils import Dif, EventName


class POD_OT_CircuitImport(bpy.types.Operator, bpy_extras.io_utils.ImportHelper):
    """Load a Pod BLx circuit file"""
    bl_idname = "import_scene.blx"
    bl_label = "Import Pod Circuit"
    bl_options = {'UNDO'}
    filename_ext = ".bl?"
    filter_glob: bpy.props.StringProperty(default="*.bl?", options={'HIDDEN'})
    import_disabled_trails: bpy.props.BoolProperty(name="Disabled Trails")

    @staticmethod
    def menu(menu: bpy.types.Menu, context: bpy.types.Context) -> None:
        menu.layout.operator(POD_OT_CircuitImport.bl_idname, text="Pod Circuit (.bl*)")

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        layout.use_property_decorate = False
        layout.use_property_split = True
        layout.prop(self, "import_disabled_trails")

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        self.context = context
        # Load circuit.
        self.format = PbdfFormat(self.properties.filepath)
        with PbdfReader(open(self.properties.filepath, "rb"), self.format.pbdf_key, self.format.pbdf_buf_size) as read:
            read.format = self.format
            self.circuit = read.any(Circuit)
        # Convert contents.
        self.import_events()
        self.import_route()
        self.import_zone()
        self.import_lights()
        self.import_anims()
        self.import_speakers()
        self.import_world()
        self.import_pits()
        self.import_direction(self.circuit.direction_forward, False)
        self.import_direction(self.circuit.direction_reverse, True)
        self.import_concurrent(self.circuit.concurrent_easy, Dif.EASY)
        self.import_concurrent(self.circuit.concurrent_medium, Dif.MEDIUM)
        self.import_concurrent(self.circuit.concurrent_hard, Dif.HARD)
        return {'FINISHED'}

    def convert_event_list(self, event_list: EventList, index: int, prop: bpy.types.CollectionProperty) -> None:
        while index != -1:
            event = event_list.events[index]
            # Add event.
            bevent: POD_CircuitEvent = prop.add()
            butils.set_enum_value(bevent, "type", event.type)
            # Add parameters, if any.
            if event.type == EventName.COND_LAP:
                butils.set_enum_value(bevent, "cond_lap", self.event_params[event.type][event.param])
            elif event.type == EventName.COND_PROB:
                bevent.params.cond_prob = self.event_params[event.type][event.param]
            elif event.type in [EventName.SOUND_PLAY_LOCAL, EventName.SOUND_STOP_LOCAL]:
                speaker_index = self.event_params[event.type][event.param] - 1
                bevent.params.speaker = self.speaker_obs[speaker_index]
            elif event.type in [EventName.SOUND_PLAY_SOURCE, EventName.SOUND_STOP_SOURCE,
                                EventName.SOUND_PLAY_STATIC, EventName.SOUND_STOP_STATIC]:
                bevent.params.sound.set(self.event_params[event.type][event.param])
            elif event.type in [EventName.SOUND_REVERB_UPDATE, EventName.SOUND_REVERB_RESET]:
                value = self.event_params[event.type][event.param]
                butils.set_enum_value(bevent.params, "sound_effect", value)
            elif event.type == EventName.RACE_CAMERA_CEILING:
                bevent.params.camera_ceiling = self.event_params[event.type][event.param]
            # Continue with next linked event.
            index = event.link

    def import_events(self) -> None:
        # Convert event type parameters.
        self.event_params = {}

        def unpack_i32(type: EventName) -> None:
            param_buffer = self.circuit.event_type_file.types[type].param_buffer
            params = struct.unpack(f"<{len(param_buffer) // 4}i", param_buffer)
            self.event_params[type] = list(params)

        unpack_i32(EventName.COND_LAP)
        unpack_i32(EventName.COND_PROB)
        unpack_i32(EventName.SOUND_PLAY_SOURCE)
        unpack_i32(EventName.SOUND_PLAY_STATIC)
        unpack_i32(EventName.SOUND_PLAY_LOCAL)
        unpack_i32(EventName.SOUND_STOP_LOCAL)
        unpack_i32(EventName.SOUND_REVERB_UPDATE)
        unpack_i32(EventName.SOUND_REVERB_RESET)
        unpack_i32(EventName.RACE_CAMERA_CEILING)

    def import_route(self) -> None:
        route_file = self.circuit.route_file
        # Convert route sectors.
        col_sectors = butils.create_collection("Sectors")
        images = textures.load_project(route_file.texture_project_name, route_file.texture_project, 256, self.format.tex_bpp)
        self.sector_obs = []
        for i, sector in enumerate(route_file.sectors):
            ob = objects.load(sector.object, f"Sector{i}", col_sectors, images)
            utils.set_ob_brightness(self.context, ob, sector.vertex_brightness)
            ob.pod_circuit.type = 'SECTOR'
            self.sector_obs.append(ob)

    def import_zone(self) -> None:
        zone_file = self.circuit.zone_file
        for i, sector in enumerate(zone_file.sectors):
            ob = self.sector_obs[i]
            for visible_sector_idx in sector.visible_sectors:
                ob.pod_circuit.sector.zones.add().ob = self.sector_obs[visible_sector_idx]

    def import_lights(self) -> None:
        pass

    def import_anims(self) -> None:
        pass

    def import_speakers(self) -> None:
        col_speakers = butils.create_collection("Speakers")
        self.speaker_obs: list[bpy.types.Object] = []
        # Convert speakers.
        for s, speaker in enumerate(self.circuit.speaker_file.speakers):
            speaker_name = f"Speaker{s}"
            bspeaker = bpy.data.speakers.new(speaker_name)
            ob = bpy.data.objects.new(speaker_name, bspeaker)
            ob.pod_circuit.type = 'SPEAKER'
            ob.pod_circuit.speaker.sound.set(speaker.sound_id)
            ob.location = Vector(speaker.position)
            col_speakers.objects.link(ob)
            self.speaker_obs.append(ob)

    def import_world(self) -> None:
        world = self.circuit.world
        world2 = self.circuit.world2
        props: POD_CircuitWorld = self.context.scene.world.pod_circuit
        # Convert fog settings.
        props.fog_distance = world.fog_distance
        props.fog_intensity = world.fog_intensity
        props.fog_mist_z = world.fog_mist_z
        props.fog_mist_intensity = world.fog_mist_intensity
        # Convert background settings.
        props.bg_on = world.bg_on
        butils.set_enum_value(props, "bg_color", world.bg_color)
        images = textures.load_project(world2.bg_texture_project_name, world2.bg_texture_project, 256, self.format.tex_bpp)
        images[0].name = images[0].name[:-1]
        props.bg_texture = images[0]
        props.bg_texture_start = world2.bg_texture_start
        props.bg_texture_end = world2.bg_texture_end
        # Convert sky settings.
        sky_type = world2.sky_type & 0xF
        if sky_type:
            props.sky_on = True
            butils.set_enum_value(props, "sky_type", sky_type)
        else:
            props.sky_on = False
        props.sky_z = world2.sky_z
        props.sky_zoom = world2.sky_zoom
        props.sky_gouraud_intensity = world2.sky_gouraud_intensity
        props.sky_gouraud_start = world2.sky_gouraud_start
        props.sky_speed = world2.sky_speed
        images = textures.load_project(world2.sky_texture_project_name, world2.sky_texture_project, self.format.tex_size_small, self.format.tex_bpp)
        images[0].name = images[0].name[:-1]
        props.sky_texture = images[0]
        # Convert sun settings.
        props.sun_on = bool(world2.sky_type & 0x10)
        if self.format.voodoo:
            props.sun_texture = textures.load_rgb565("Sun", world2.sky_lensflare_texture, 128)
        props.sun_color = colors.from_rgb888(world2.sky_sun_color)

    def import_pits(self) -> None:
        pit_file = self.circuit.pit_file
        pits_props: POD_CircuitPits = self.context.scene.pod_circuit.pits
        pits_props.repair_seconds = pit_file.repair_seconds
        # Convert pits.
        col_pits = butils.create_collection("Pits")
        for i, pit in enumerate(pit_file.pits):
            name = f"Pit{i}"
            # Create object.
            mesh = bpy.data.meshes.new(name)
            ob = bpy.data.objects.new(name, mesh)
            ob.display_type = 'WIRE'
            ob.show_name = True
            ob.pod_circuit.type = 'PIT'
            ob.location = pit.center
            # Create mesh.
            bm = bmesh.new()
            height = Vector((0, 0, pit.height))
            btm = [Vector(p) - ob.location for p in pit.positions]
            top = [p + height for p in btm]
            verts = [bm.verts.new(p) for p in btm]
            verts.extend(bm.verts.new(p) for p in top)
            bm.faces.new((verts[0], verts[1], verts[5], verts[4]))
            bm.faces.new((verts[0], verts[3], verts[2], verts[1]))
            bm.faces.new((verts[0], verts[4], verts[7], verts[3]))
            bm.faces.new((verts[1], verts[2], verts[6], verts[5]))
            bm.faces.new((verts[2], verts[3], verts[7], verts[6]))
            bm.faces.new((verts[4], verts[5], verts[6], verts[7]))
            bm.to_mesh(mesh)
            bm.free()
            col_pits.objects.link(ob)

    def import_direction(self, direction: CircuitDirection, reverse: bool) -> None:
        dir_name = "Reverse" if reverse else "Forward"
        col_dir = butils.create_collection(f"Direction {dir_name}")
        section_beacon_obs = []
        section_beacon_dirs = []

        def import_times() -> None:
            time_file: TimeFile = direction.time_file
            time_props: POD_CircuitTimes = self.context.scene.pod_circuit.times
            props: POD_CircuitTimesDirection = time_props.reverse if reverse else time_props.forward
            props.min_part_1 = time_file.parts_min[0]
            props.min_part_2 = time_file.parts_min[1]
            props.min_part_3 = time_file.parts_min[2]
            props.min_part_4 = time_file.parts_min[3]
            props.avg_part_1 = time_file.parts_avg[0]
            props.avg_part_2 = time_file.parts_avg[1]
            props.avg_part_3 = time_file.parts_avg[2]
            props.avg_part_4 = time_file.parts_avg[3]

        def import_beacons() -> None:
            col_beacons = butils.create_collection(f"Beacons {dir_name}", col_dir)
            valid_path_file = direction.difficulty_easy.path_file if direction.difficulty_easy.path_filename.upper() != "NEANT" \
                else direction.difficulty_medium.path_file if direction.difficulty_medium.path_filename.upper() != "NEANT" \
                else direction.difficulty_hard.path_file if direction.difficulty_hard.path_filename.upper() != "NEANT" \
                else None
            # Convert beacons.
            for i, beacon in enumerate(direction.beacon_file.beacons):
                name = f"Beacon{dir_name[0]}{i}"
                # Create object.
                mesh = bpy.data.meshes.new(name)
                ob = bpy.data.objects.new(name, mesh)
                ob.display_type = 'SOLID'
                ob.show_name = True
                ob.show_wire = True
                ob.pod_circuit.type = 'BEACON'
                ob.pod_circuit.beacon.reverse = reverse
                self.convert_event_list(direction.beacon_file.event_list, beacon.positive_event, ob.pod_circuit.beacon.positive_events)
                self.convert_event_list(direction.beacon_file.event_list, beacon.negative_event, ob.pod_circuit.beacon.negative_events)
                # Keep track of section beacons and use their section's node position as origin.
                sections = []
                if utils.get_beacon_event_dir(ob, 'RACE_SECTION', 1):
                    sections.append(len(section_beacon_obs))
                    section_beacon_obs.append(ob)
                    section_beacon_dirs.append('POSITIVE')
                if utils.get_beacon_event_dir(ob, 'RACE_SECTION', -1):
                    sections.append(len(section_beacon_obs))
                    section_beacon_obs.append(ob)
                    section_beacon_dirs.append('NEGATIVE')
                if sections and valid_path_file:
                    for section in sections:
                        ob.name += f"s{section}"
                        mesh.name += f"s{section}"
                    ob.location = Vector(valid_path_file.nodes[sections[0]].position)
                else:
                    ob.location = (Vector(beacon.points[0]) + Vector(beacon.points[1])) / 2
                # Create mesh.
                bm = bmesh.new()
                bm.faces.new([bm.verts.new(Vector(x) - ob.location) for x in beacon.points])
                bm.to_mesh(mesh)
                bm.free()
                col_beacons.objects.link(ob)

        def import_phases() -> None:
            phase_file = direction.phase_file
            phases = phase_file.phases
            event_list = phase_file.event_list
            phases_props: POD_CircuitPhases = self.context.scene.pod_circuit.phases
            props: POD_CircuitPhasesDirection = phases_props.reverse if reverse else phases_props.forward
            # Convert phase events.
            if len(phases):  # either has all 4 with appropriate names per index, or none
                self.convert_event_list(event_list, phases[0].event_index, props.arrival_events)
                self.convert_event_list(event_list, phases[1].event_index, props.load_events)
                self.convert_event_list(event_list, phases[2].event_index, props.start_events)
                self.convert_event_list(event_list, phases[3].event_index, props.finish_events)

        def import_gates() -> None:
            col_gates = butils.create_collection(f"Gates {dir_name}", col_dir)
            # Convert gates.
            for g, gate in enumerate(direction.gate_file.gates):
                gate_name = f"Gate{dir_name[0]}{g}"
                ob = bpy.data.objects.new(gate_name, None)
                ob.pod_circuit.type = 'GATE'
                ob.pod_circuit.gate.reverse = reverse
                ob.empty_display_size = 3
                ob.empty_display_type = 'ARROWS'
                # Calculate position and rotation.
                x_axis = Vector(gate.axis_x)
                z_axis = Vector((0.0, 0.0, 1.0))
                y_axis = z_axis.cross(x_axis)
                world = Matrix()
                for i in range(3):
                    world[i].xyzw = x_axis[i], y_axis[i], z_axis[i], gate.position[i]
                ob.matrix_world = world
                col_gates.objects.link(ob)

        def import_difficulty(difficulty: CircuitDirectionDifficulty, dif: Dif) -> None:
            if difficulty.map_filename.upper() == "NEANT":
                return

            # Convert map trails.
            col_trails = butils.create_collection(f"Trails {dir_name} {dif.name.title()}", col_dir)
            for s, section in enumerate(difficulty.map_file.sections):
                for t, trail in enumerate(section.trails):
                    trail_data = trail.get_constraint_data()
                    if not self.import_disabled_trails and trail_data.disabled:
                        continue
                    name = f"Trail{dir_name[0]}{dif.name[0]}{t}s{s}"
                    origin = Vector(difficulty.map_file.positions[trail.position_start])
                    # Create curve.
                    curve = bpy.data.curves.new(name, 'CURVE')
                    curve.dimensions = '3D'
                    spline = curve.splines.new('POLY')
                    spline.points.add(trail.position_count - 1)
                    position_end = trail.position_start + trail.position_count
                    for dst, src in enumerate(range(trail.position_start, position_end)):
                        spline.points[dst].co.xyz = Vector(difficulty.map_file.positions[src]) - origin
                        spline.points[dst].co.w = 1.0
                    # Create object.
                    ob = bpy.data.objects.new(name, curve)
                    ob.display_type = 'WIRE'
                    ob.location = origin
                    ob.pod_circuit.type = 'TRAIL'
                    ob.pod_circuit.trail.difficulties = {dif.name}
                    ob.pod_circuit.trail.reverse = reverse
                    ob.pod_circuit.trail.beacon = section_beacon_obs[s]
                    ob.pod_circuit.trail.beacon_direction = section_beacon_dirs[s]
                    ob.pod_circuit.trail.level = trail.level
                    ob.pod_circuit.trail.constraint_type = trail_data.type.name
                    ob.pod_circuit.trail.constraint_param = trail_data.param
                    ob.pod_circuit.trail.constraint_number = trail_data.number
                    ob.pod_circuit.trail.graph = trail_data.graph.name
                    ob.pod_circuit.trail.shortcut = trail_data.hidden
                    col_trails.objects.link(ob)
                    ob.hide_set(trail_data.disabled)

        # Convert direction.
        import_times()
        import_beacons()
        import_phases()
        import_gates()
        import_difficulty(direction.difficulty_easy, Dif.EASY)
        import_difficulty(direction.difficulty_medium, Dif.MEDIUM)
        import_difficulty(direction.difficulty_hard, Dif.HARD)

    def import_concurrent(self, concurrent: CircuitConcurrent, dif: Dif) -> None:
        if concurrent.difficulty_name.upper() == "NEANT":
            return
        # Convert competitors.
        for competitor in concurrent.competitor_file.competitors:
            bcompetitor: POD_CircuitCompetitor = self.context.scene.pod_circuit.competitors.add()
            bcompetitor.difficulty = dif.name
            bcompetitor.name = competitor.name
            bcompetitor.level = competitor.level
            if competitor.behavior in iter(CompetitorBehavior):
                bcompetitor.behavior = CompetitorBehavior(competitor.behavior).name
            else:
                bcompetitor.behavior = CompetitorBehavior.PASSIVE.name
                self.report({'WARNING'}, f"Competitor {bcompetitor.name} has invalid behavior {competitor.behavior},"
                            f" reset to {bcompetitor.behavior.title()}")
            bcompetitor.car_index = int(competitor.car_index)
            for attack in competitor.attacks:
                battack: POD_CircuitCompetitorAttack = bcompetitor.attacks.add()
                battack.type = attack.type.name
                butils.set_enum_value(battack, "context", attack.context)
                battack.percentage = attack.percentage


def register() -> None:
    bpy.types.TOPBAR_MT_file_import.append(POD_OT_CircuitImport.menu)


def unregister() -> None:
    bpy.types.TOPBAR_MT_file_import.remove(POD_OT_CircuitImport.menu)
