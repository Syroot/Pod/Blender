from typing import Union

import bpy
from ubipod.circuit import CompetitorAttackContext

from ..props.scene import POD_CircuitCompetitor, POD_CircuitScene
from ..props.winman import POD_CircuitWinman


class POD_OT_CircuitCompetitorAttackSort(bpy.types.Operator):
    """Sorts the attacks of the current competitor"""
    bl_idname = "object.pod_circuit_competitor_attack_sort"
    bl_label = "Sort Competitor Attacks"
    bl_options = {'INTERNAL', 'UNDO'}

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        props: POD_CircuitScene = context.scene.pod_circuit
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        competitor: POD_CircuitCompetitor = props.competitors[wm_props.competitor_index]
        # Clone attacks and sort them by context and percentage.
        attacks = [(a.type, a.context, a.percentage) for a in competitor.attacks]
        attacks.sort(key=lambda a: a[2])
        attacks.sort(key=lambda a: sum(CompetitorAttackContext[c] for c in a[1]))
        # Clear attack collection.
        competitor.attacks.clear()
        # Add clones back.
        for attack in attacks:
            battack = competitor.attacks.add()
            battack.type = attack[0]
            battack.context = attack[1]
            battack.percentage = attack[2]
        return {'FINISHED'}
