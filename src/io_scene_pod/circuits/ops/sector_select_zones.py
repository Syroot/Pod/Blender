from typing import Union

import bpy


class POD_OT_CircuitSectorSelectZones(bpy.types.Operator):
    """Selects all visible sectors when the car touches the active sector"""
    bl_idname = "object.pod_circuit_sector_select_zones"
    bl_label = "Select Visible Pod Sectors"

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        ob = context.object
        zones = ob.pod_circuit.sector.zones
        for selected_ob in context.selected_objects:
            selected_ob.select_set(False)
        for zone_ob in zones:
            zone_ob.ob.select_set(True)
        ob.select_set(True)
        return {'FINISHED'}
