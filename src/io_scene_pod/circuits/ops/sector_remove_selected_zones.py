from typing import Union

import bpy


class POD_OT_CircuitSectorRemoveSelectedZones(bpy.types.Operator):
    """Hides selected sectors when the car touches the active sector"""
    bl_idname = "object.pod_circuit_sector_remove_selected_zones"
    bl_label = "Hide Selected Pod Sectors"

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        ob = context.object
        zones = ob.pod_circuit.sector.zones
        for i in reversed(range(len(zones))):
            if zones[i].ob in context.selected_objects:
                zones.remove(i)
        return {'FINISHED'}
