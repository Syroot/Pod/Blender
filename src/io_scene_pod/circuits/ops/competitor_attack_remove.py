from typing import Union

import bpy

from ..props.scene import POD_CircuitCompetitor, POD_CircuitScene
from ..props.winman import POD_CircuitWinman


class POD_OT_CircuitCompetitorAttackRemove(bpy.types.Operator):
    """Removes the selected attack"""
    bl_idname = "object.pod_circuit_competitor_attack_remove"
    bl_label = "Remove Competitor Attack"
    bl_options = {'INTERNAL', 'UNDO'}

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        props: POD_CircuitScene = context.scene.pod_circuit
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        competitor: POD_CircuitCompetitor = props.competitors[wm_props.competitor_index]
        return 0 <= wm_props.competitor_attack_index < len(competitor.attacks)

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        props: POD_CircuitScene = context.scene.pod_circuit
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        competitor: POD_CircuitCompetitor = props.competitors[wm_props.competitor_index]
        competitor.attacks.remove(wm_props.competitor_attack_index)
        wm_props.competitor_attack_index = min(wm_props.competitor_attack_index, len(competitor.attacks) - 1)
        return {'FINISHED'}
