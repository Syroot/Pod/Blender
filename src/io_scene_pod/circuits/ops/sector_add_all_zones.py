from typing import Union

import bpy

from .. import utils


class POD_OT_CircuitSectorAddAllZones(bpy.types.Operator):
    """Shows all sectors anywhere. The game may crash in case of high-poly sectors"""
    bl_idname = "object.pod_circuit_sector_add_all_zones"
    bl_label = "Show All Pod Sectors Anywhere"

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        sector_obs = [ob for ob in context.scene.objects if utils.is_sector(ob)]
        for sector_ob in sector_obs:
            for zone_ob in sector_obs:
                utils.add_sector_zone(sector_ob, zone_ob)
        return {'FINISHED'}
