import pathlib
import struct
from typing import Any, Union

import bpy
import bpy_extras
from mathutils import Vector
from ubipod.binary import BinaryReader, BinaryWritable, BinaryWriter
from ubipod.circuit import (AnimInstanceList, Beacon, Checkpoint, Circuit, CircuitConcurrent, CircuitDirection,
                            CircuitDirectionDifficulty, Competitor, CompetitorAttack, CompetitorAttackContext,
                            CompetitorAttackType, CompetitorBehavior, Event, EventList, EventType, Gate, LightList,
                            MapConstraintData, MapConstraintType, MapGraph, MapSection, MapTrail, PathArc,
                            PathNeighbor, PathNode, Phase, Pit, Sector, Speaker, TimeFile, ZoneSector)
from ubipod.images import Image, ImageFile
from ubipod.pbdf import PbdfFormat, PbdfReader, PbdfWriter
from ubipod.scripts import CircuitInfo, CircuitScript
from ubipod.sounds import MegaFile

from ... import butils, colors, imgconv, objects, textures
from .. import utils
from ..graph import Graph
from ..props.events import POD_CircuitEvent
from ..props.object import POD_CircuitBeacon, POD_CircuitSpeaker
from ..props.scene import (POD_CircuitCompetitor, POD_CircuitCompetitorAttack, POD_CircuitPatcher, POD_CircuitPhases,
                           POD_CircuitPhasesDirection, POD_CircuitPits, POD_CircuitScene, POD_CircuitTimes,
                           POD_CircuitTimesDirection)
from ..props.world import POD_CircuitWorld
from ..utils import Dif, EventName


class POD_OT_CircuitExport(bpy.types.Operator, bpy_extras.io_utils.ExportHelper):
    """Save a Pod BLx circuit file"""
    bl_idname = "export_scene.blx"
    bl_label = "Export Pod Circuit"
    bl_options = {'UNDO'}
    filename_ext = ".bl4"
    filter_glob: bpy.props.StringProperty(default="*.bl?", options={'HIDDEN'})

    @staticmethod
    def menu(menu: bpy.types.Menu, context: bpy.types.Context) -> None:
        menu.layout.operator(POD_OT_CircuitExport.bl_idname, text="Pod Circuit (.bl*)")

    def export_format_update(self, context: bpy.types.Context) -> None:
        ext = 'bl4' if self.export_format == 'BL4ARCADE' else self.export_format.lower()
        POD_OT_CircuitExport.filename_ext = "." + ext

    export_format: bpy.props.EnumProperty(name="Format", items=(
        ('BL3', "BL3", "", 3),
        ('BL4', "BL4", "", 4),
        ('BL4ARCADE', "BL4 Arcade", "", 5),
        ('BL6', "BL6", "", 6),
        ('BL7', "BL7", "", 7),
        ('BL8', "BL8", "", 8),
        ('BL9', "BL9", "", 9),
    ), default='BL4', update=export_format_update)
    export_trails_forward: bpy.props.BoolProperty(name="Trails (Forward)", default=True)
    export_trails_reverse: bpy.props.BoolProperty(name="Trails (Reverse)", default=True)
    install_game: bpy.props.BoolProperty(name="Install to Game")
    install_patch: bpy.props.BoolProperty(name="Create Patch Files")

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        layout.use_property_decorate = False
        layout.use_property_split = True
        layout.prop(self, "export_format")
        layout.prop(self, "export_trails_forward")
        layout.prop(self, "export_trails_reverse")
        # Draw files panel.
        header, body = layout.panel("POD_OT_CircuitExportInstall", default_closed=False)
        header.label(text="Additional Files")
        if body:
            body.prop(self, "install_game")
            body.prop(self, "install_patch")

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        self.context = context
        if context.mode != 'OBJECT':
            bpy.ops.object.mode_set(mode='OBJECT')
        # Load template circuit.
        tmp_path = str(pathlib.Path(__file__).resolve().parent / "template.bl9")
        tmp_format = PbdfFormat(tmp_path)
        with PbdfReader(open(tmp_path, "rb"), tmp_format.pbdf_key, tmp_format.pbdf_buf_size) as read:
            read.format = tmp_format
            self.template = read.any(Circuit)
        # Collect information.
        self.circuit = Circuit()
        self.circuit_name = pathlib.Path(self.properties.filepath).stem
        self.format = PbdfFormat(self.properties.filepath)
        self.depsgraph = context.evaluated_depsgraph_get()
        self.event_params = {}
        self.speaker_obs = [ob for ob in self.context.scene.objects if utils.is_speaker(ob)]
        # Convert contents.
        self.export_voodoo()
        self.export_route()
        self.export_zone()
        self.export_lights()
        self.export_anims()
        self.export_speakers()
        self.export_world()
        self.export_pits()
        self.export_direction(self.circuit.direction_forward, False)
        self.export_direction(self.circuit.direction_reverse, True)
        self.export_concurrent(self.circuit.concurrent_easy, Dif.EASY)
        self.export_concurrent(self.circuit.concurrent_medium, Dif.MEDIUM)
        self.export_concurrent(self.circuit.concurrent_hard, Dif.HARD)
        self.export_events()
        # Save circuit.
        with PbdfWriter(open(self.properties.filepath, "w+b"),
                        self.format.pbdf_key, self.format.pbdf_buf_size, self.format.pbdf_ofs_count) as write:
            write.format = self.format
            write.any(self.circuit)
        # Save / modify additional files.
        if self.install_game or self.install_patch:
            self.install()
        return {'FINISHED'}

    def convert_event_list(self, event_list: EventList, bevents: list[POD_CircuitEvent]) -> int:
        def set_param(type: EventName, param: Any) -> int:
            params: list[Any] = self.event_params.setdefault(type, [])
            if param not in params:
                params.append(param)
            return params.index(param)

        first_index = -1
        for i, bevent in enumerate(bevents):
            if i == 0:
                first_index = len(event_list.events)
            # Add event.
            event = Event()
            event.type = butils.get_enum_value(bevent, "type")
            # Add parameters, if any.
            if event.type == EventName.COND_LAP:
                event.param = set_param(event.type, butils.get_enum_value(bevent, "cond_lap"))
            elif event.type == EventName.COND_PROB:
                event.param = set_param(event.type, bevent.params.cond_prob)
            elif event.type in [EventName.SOUND_PLAY_LOCAL, EventName.SOUND_STOP_LOCAL]:
                speaker_index = self.speaker_obs.index(bevent.params.speaker) + 1
                event.param = set_param(event.type, speaker_index)
            elif event.type in [EventName.SOUND_PLAY_SOURCE, EventName.SOUND_STOP_SOURCE,
                                EventName.SOUND_PLAY_STATIC, EventName.SOUND_STOP_STATIC]:
                event.param = set_param(event.type, bevent.params.sound.get())
            elif event.type == EventName.SOUND_REVERB_UPDATE:
                event.param = set_param(event.type, butils.get_enum_value(bevent.params, "sound_effect"))
            elif event.type == EventName.RACE_CAMERA_CEILING:
                event.param = set_param(event.type, bevent.params.camera_ceiling)
            # Link with next event.
            event.link = -1 if i == len(bevents) - 1 else len(event_list.events) + 1
            event_list.events.append(event)
        return first_index

    def create_filename(self, ext: str, reverse: bool = False, dif: Dif = None) -> str:
        chars = list(self.circuit_name[0:8])
        if reverse:
            chars[0] = '-'
        if dif:
            chars += ['_'] * (8 - len(chars))
            chars[7] = dif.name[0]
        return f"{''.join(chars)}.{ext}"

    def export_events(self) -> None:
        def pack_i32(type: EventName) -> bytes:
            params = self.event_params.get(type, [])
            return struct.pack(f"<{len(params)}i", *params)

        types = self.circuit.event_type_file.types
        types.append(EventType("...Sinon..."))
        types.append(EventType("? Concurrent"))
        types.append(EventType("? Joueur"))
        types.append(EventType("? Proba Unique", 4, pack_i32(EventName.COND_PROB)))
        types.append(EventType("? Tour", 4, pack_i32(EventName.COND_LAP)))
        types.append(EventType("Active Macro", 4))
        types.append(EventType("Aff Env OFF"))
        types.append(EventType("Aff Env ON"))
        types.append(EventType("All Macros OFF"))
        types.append(EventType("All Macros ON"))
        types.append(EventType("Arret Anim", 44))
        types.append(EventType("Bloque Anim"))
        types.append(EventType("Camera Libre"))
        types.append(EventType("Camera Plafond", 4, pack_i32(EventName.RACE_CAMERA_CEILING)))
        types.append(EventType("Contrainte"))
        types.append(EventType("DEBUT-COND"))
        types.append(EventType("Decision PIT"))
        types.append(EventType("Degats Off"))
        types.append(EventType("Degats On"))
        types.append(EventType("Deplace Anim", 12))
        types.append(EventType("Desactive Macr", 4))
        types.append(EventType("Echange Macros", 8))
        types.append(EventType("Fige Anim", 44))
        types.append(EventType("Fin Course"))
        types.append(EventType("Fin Dep L"))
        types.append(EventType("FIN-COND"))
        types.append(EventType("Init Macro", 4))
        types.append(EventType("Lance Anim", 44))
        types.append(EventType("Macro", 8))
        types.append(EventType("N-Chrono Inter"))
        types.append(EventType("N-Chrono Tour"))
        types.append(EventType("P-Chrono Inter"))
        types.append(EventType("P-Chrono Tour"))
        types.append(EventType("Pistes cachees"))
        types.append(EventType("Reinit Anim"))
        types.append(EventType("Relance Anim"))
        types.append(EventType("Remplace Macro", 8))
        types.append(EventType("Reset Reverb V", 4, pack_i32(EventName.SOUND_REVERB_RESET)))
        types.append(EventType("Reverb Voiture", 4, pack_i32(EventName.SOUND_REVERB_UPDATE)))
        types.append(EventType("Son Localisé", 4, pack_i32(EventName.SOUND_PLAY_LOCAL)))
        types.append(EventType("Son Source", 4, pack_i32(EventName.SOUND_PLAY_SOURCE)))
        types.append(EventType("Son Statique", 4, pack_i32(EventName.SOUND_PLAY_STATIC)))
        types.append(EventType("Stop Anim"))
        types.append(EventType("Troncon"))
        types.append(EventType("Tue Sons Local", 4, pack_i32(EventName.SOUND_STOP_LOCAL)))
        types.append(EventType("Tue Sons Sourc"))
        types.append(EventType("Tue Sons Stati"))
        types.append(EventType("Tunnel Entree"))
        types.append(EventType("Tunnel Sortie"))
        types.append(EventType("Wrong-Way OFF"))
        types.append(EventType("Wrong-Way ON"))
        types.append(EventType("}}} (Rupture)"))

    def export_voodoo(self) -> None:
        self.circuit.voodoo_file.face_lod = [0] * 16

    def export_route(self) -> None:
        self.circuit.route_filename = self.create_filename("rte")
        route = self.circuit.route_file
        # Convert sectors.
        mats = []
        self.sector_obs = [ob for ob in self.context.scene.objects if utils.is_sector(ob)]
        for sector_ob in self.sector_obs:
            object = objects.save(self.context, sector_ob, mats)
            brightness = utils.get_ob_brightness(sector_ob)
            sector = Sector(object, brightness, object.calc_bbox())
            sector.bbox.min = (sector.bbox.min[0], sector.bbox.min[1], sector.bbox.min[2] - 2)
            sector.bbox.max = (sector.bbox.max[0], sector.bbox.max[1], sector.bbox.max[2] + 10)
            route.sectors.append(sector)
        # Convert textures.
        route.texture_project_name = self.circuit_name
        textures.save_project(route.texture_project_name, route.texture_project, 256, self.format.tex_bpp, mats)

    def export_zone(self) -> None:
        zone = self.circuit.zone_file
        # Convert zones.
        for sector_ob in self.sector_obs:
            zone_sector = ZoneSector([self.sector_obs.index(zone.ob) for zone in sector_ob.pod_circuit.sector.zones])
            zone.sectors.append(zone_sector)

    def export_lights(self) -> None:
        self.circuit.light_filename = self.create_filename("lum")
        self.circuit.light_file = self.template.light_file
        self.circuit.light_file.sector_lights = [LightList()] * len(self.circuit.route_file.sectors)

    def export_anims(self) -> None:
        self.circuit.anim_filename = self.create_filename("anc")
        self.circuit.anim_file = self.template.anim_file
        self.circuit.anim_file.sector_instances = [AnimInstanceList()] * len(self.circuit.route_file.sectors)

    def export_speakers(self) -> None:
        self.circuit.speaker_filename = self.create_filename("hp")
        speakers = self.circuit.speaker_file.speakers
        # Convert gates.
        for speaker_ob in self.speaker_obs:
            props: POD_CircuitSpeaker = speaker_ob.pod_circuit.speaker
            speaker = Speaker(True, speaker_ob.location, sound_id=props.sound.get())
            speakers.append(speaker)

    def export_world(self) -> None:
        world = self.circuit.world
        world2 = self.circuit.world2
        props: POD_CircuitWorld = self.context.scene.world.pod_circuit
        # Convert fog settings.
        world.fog_distance = props.fog_distance
        world.fog_intensity = props.fog_intensity
        world.fog_mist_z = props.fog_mist_z
        world.fog_mist_intensity = props.fog_mist_intensity
        # Convert background settings.
        world.bg_on = props.bg_on
        world.bg_color = butils.get_enum_value(props, "bg_color")
        if props.bg_texture:
            world2.bg_texture_project_name = props.bg_texture.name
            textures.save_project(world2.bg_texture_project_name, world2.bg_texture_project, 256, self.format.tex_bpp, [props.bg_texture])
        world2.bg_texture_start = props.bg_texture_start
        world2.bg_texture_end = props.bg_texture_end
        # Convert sky settings.
        if props.sky_on:
            world2.sky_type = butils.get_enum_value(props, "sky_type")
        else:
            world2.sky_type = 0
        world2.sky_z = props.sky_z
        world2.sky_zoom = props.sky_zoom
        world2.sky_gouraud_intensity = props.sky_gouraud_intensity
        world2.sky_gouraud_start = props.sky_gouraud_start
        world2.sky_speed = props.sky_speed
        if props.sky_texture:
            world2.sky_texture_project_name = props.sky_texture.name
            textures.save_project(world2.sky_texture_project_name, world2.sky_texture_project, self.format.tex_size_small, self.format.tex_bpp, [props.sky_texture])
        # Convert sun settings.
        if props.sun_on:
            world2.sky_type |= 0x10
        if self.format.voodoo:
            world2.sky_lensflare_texture = textures.save_rgb565(props.sun_texture)
        world2.sky_sun_color = colors.to_rgb888(props.sun_color)

    def export_pits(self) -> None:
        self.circuit.pit_filename = self.create_filename("lev")
        pit_file = self.circuit.pit_file
        # Convert repair time.
        pits_props: POD_CircuitPits = self.context.scene.pod_circuit.pits
        pit_file.repair_seconds = pits_props.repair_seconds
        # Convert pits.
        for pit_ob in (ob for ob in self.context.scene.objects if utils.is_pit(ob)):
            # Find bottom most face to use for positions.
            bm = butils.bmesh_from_object(pit_ob, self.context.evaluated_depsgraph_get())
            min_z = float("inf")
            for face in bm.faces:
                z = face.calc_center_bounds().z
                if z < min_z:
                    min_z = z
                    min_face = face
            positions = [v.co for v in reversed(min_face.verts)]  # ensure normal points upwards
            center = sum(positions, Vector()) / len(positions)
            height = max(1.0, pit_ob.dimensions.z)
            bm.free()
            # Create pit.
            pit = Pit()
            pit.positions = [tuple(p) for p in positions]
            pit.center = tuple(center)
            pit.height = height
            # Calculate unknown field.
            a = positions[0].x - center.x
            pit.size = positions[0].y - center.y + a
            a = abs(positions[1].x - center.x)
            b = positions[1].y - center.y
            if a + b > center.z:
                pit.size = a + b
            a = positions[2].x - center.x
            b = positions[2].y - center.y
            if a + b > center.z:
                pit.size = a + b
            a = positions[3].x - center.x
            b = positions[3].y - center.y
            if a + b > center.z:
                pit.size = a + b
            # Append to collection.
            pit_file.pits.append(pit)

    def export_direction(self, direction: CircuitDirection, reverse: bool) -> None:
        def export_times() -> None:
            time_file: TimeFile = direction.time_file
            time_props: POD_CircuitTimes = self.context.scene.pod_circuit.times
            props: POD_CircuitTimesDirection = time_props.reverse if reverse else time_props.forward
            # Convert part times.
            part_count = len(direction.beacon_file.checkpoints)
            for i in range(part_count):
                time_file.parts_min[i] = getattr(props, f"min_part_{i + 1}")
                time_file.parts_avg[i] = getattr(props, f"avg_part_{i + 1}")
            time_file.has_part_times = all(time_file.parts_min[:part_count]) and all(time_file.parts_avg[:part_count])
            # Convert lap times.
            time_file.lap_min = sum(time_file.parts_min)
            time_file.lap_avg = sum(time_file.parts_avg)
            time_file.has_lap_times = all((time_file.lap_min, time_file.lap_avg))

        def export_beacons() -> None:
            direction.beacon_filename = self.create_filename("bal", reverse)
            beacon_file = direction.beacon_file
            # Convert beacons.
            for beacon_ob in (x for x in self.context.scene.objects if utils.is_beacon(x, reverse)):
                props: POD_CircuitBeacon = beacon_ob.pod_circuit.beacon
                points, normal = utils.get_beacon_model(self.context, beacon_ob)
                beacon = Beacon()
                beacon.points = [tuple(x) for x in points]
                beacon.normal = tuple(normal)
                beacon.positive_event = self.convert_event_list(direction.beacon_file.event_list, props.positive_events)
                beacon.negative_event = self.convert_event_list(direction.beacon_file.event_list, props.negative_events)
                beacon_file.beacons.append(beacon)

        def export_phases() -> None:
            phase_file = direction.phase_file
            phases = phase_file.phases
            event_list = phase_file.event_list
            phases_props: POD_CircuitPhases = self.context.scene.pod_circuit.phases
            props: POD_CircuitPhasesDirection = phases_props.reverse if reverse else phases_props.forward
            # Convert phase events.
            phases.append(Phase("Arrivee", self.convert_event_list(event_list, props.arrival_events)))
            phases.append(Phase("Charge", self.convert_event_list(event_list, props.load_events)))
            phases.append(Phase("Depart", self.convert_event_list(event_list, props.start_events)))
            phases.append(Phase("FinCourse", self.convert_event_list(event_list, props.finish_events)))

        def export_gates() -> None:
            gate_file = direction.gate_file
            # Convert gates.
            for gate_ob in (x for x in self.context.scene.objects if utils.is_gate(x, reverse)):
                world = gate_ob.matrix_world
                position = world[0][3], world[1][3], world[2][3]
                axis_x = world[0][0], world[1][0], world[2][0]
                gate_file.gates.append(Gate(position, axis_x))

        def export_difficulty(difficulty: CircuitDirectionDifficulty, dif: Dif) -> None:
            def export_map() -> None:
                difficulty.map_filename = self.create_filename("lev", reverse, dif)
                map_file = difficulty.map_file
                # Convert sections and their trails for each node.
                for node in graph.nodes:
                    map_section = MapSection()
                    map_file.sections.append(map_section)
                    for trail in node.trails:
                        map_trail = MapTrail()
                        map_section.trails.append(map_trail)
                        map_trail.position_start = len(map_file.positions)
                        map_trail.position_count = len(trail.points)
                        for point in trail.points:
                            map_file.positions.append(tuple(point))
                        # Add unreferenced point extending 32 units from the end.
                        if len(trail.points) >= 2:
                            normal = trail.points[-1] - trail.points[-2]
                            normal.normalize()
                            map_file.positions.append(tuple(trail.points[-1] + normal * 32))
                        # Set properties.
                        map_trail.level = trail.props.level
                        constraint = MapConstraintData()
                        constraint.type = MapConstraintType[trail.props.constraint_type]
                        constraint.graph = MapGraph[trail.props.graph]
                        constraint.hidden = trail.props.shortcut
                        constraint.number = trail.props.constraint_number
                        constraint.param = trail.props.constraint_param
                        map_trail.set_constraint_data(constraint)

            def export_path() -> None:
                difficulty.path_filename = self.create_filename("lev", reverse, dif)
                path_file = difficulty.path_file
                path_file.circuit_length = graph.circuit_length
                # Convert nodes.
                for node in graph.nodes:
                    path_node = PathNode()
                    path_node.position = tuple(node.beacon_ob.location)
                    path_node.distance = node.distance
                    path_node.neighbor = graph.neighbors.index(node.neighbor) if node.neighbor else -1
                    for out_arc in node.out_arcs:
                        path_node.out_arcs.append(graph.arcs.index(out_arc) + 1)
                    for in_arc in node.in_arcs:
                        path_node.in_arcs.append(graph.arcs.index(in_arc) + 1)
                    path_file.nodes.append(path_node)
                # Convert arcs.
                for arc in graph.arcs:
                    path_arc = PathArc()
                    path_arc.parent_node = graph.nodes.index(arc.parent_node) + 1
                    path_arc.child_node = graph.nodes.index(arc.child_node) + 1
                    for trail in arc.trails:
                        path_arc.trails.append(arc.parent_node.trails.index(trail))
                    path_arc.distance_trail = arc.trails.index(arc.distance_trail)
                    path_arc.distances = arc.distances[:]
                    path_file.arcs.append(path_arc)
                # Convert neighbors.
                for neighbor in graph.neighbors:
                    path_neighbor = PathNeighbor()
                    path_neighbor.initial_node = graph.nodes.index(neighbor.initial_node) + 1
                    path_neighbor.final_node = graph.nodes.index(neighbor.final_node) + 1
                    path_neighbor.parent_neighbor = graph.neighbors.index(neighbor.parent) if neighbor.parent else -1
                    path_file.neighbors.append(path_neighbor)

            def export_checkpoints() -> None:
                beacon_file = direction.beacon_file
                # Convert checkpoints, calculating their distance to the next.
                for cp in graph.cps:
                    checkpoint = Checkpoint()
                    checkpoint.beacon_index = graph.beacon_obs.index(cp.beacon_ob)
                    checkpoint.positive = cp.beacon_dir
                    checkpoint.distance_to_next = int(cp.distance)
                    # Span line from bottom vertices of checkpoint beacon (guides are not available).
                    beacon_points, _ = utils.get_beacon_model(self.context, cp.beacon_ob)
                    checkpoint.map_line_from = tuple(beacon_points[0])
                    checkpoint.map_line_to = tuple(beacon_points[1])
                    beacon_file.checkpoints.append(checkpoint)
                # Calculate relative distances.
                for i in range(len(beacon_file.checkpoints) - 1):
                    beacon_file.checkpoints[i].distance_to_next -= beacon_file.checkpoints[i + 1].distance_to_next

            def export_dummy_map() -> None:
                difficulty.map_filename = self.create_filename("lev", reverse, dif)
                map_file = difficulty.map_file
                # Require a section for each section beacon.
                for beacon_ob in (x for x in self.context.scene.objects if utils.is_beacon(x, reverse)):
                    for beacon_dir in [1, -1]:
                        if utils.get_beacon_event_dir(beacon_ob, 'RACE_SECTION', beacon_dir):
                            section = MapSection()
                            map_file.sections.append(section)
                # Require at least one point.
                map_file.positions.append((0.0, 0.0, 0.0))

            def export_dummy_path() -> None:
                difficulty.path_filename = self.create_filename("lev", reverse, dif)
                path_file = difficulty.path_file
                # Require a node for each section.
                path_file.nodes = [PathNode()] * len(difficulty.map_file.sections)

            def export_dummy_checkpoints() -> None:
                beacon_file = direction.beacon_file
                # Convert checkpoints determined by order of beacons.
                for b, beacon_ob in enumerate(x for x in self.context.scene.objects if utils.is_beacon(x, reverse)):
                    points, _ = utils.get_beacon_model(self.context, beacon_ob)
                    if event_dir := utils.get_beacon_event_dir(beacon_ob, 'TIME_P_LAP'):
                        cp = Checkpoint(b, event_dir == 1, 0, tuple(points[0]), tuple(points[1]))
                        direction.beacon_file.checkpoints.insert(0, cp)
                    elif event_dir := utils.get_beacon_event_dir(beacon_ob, 'TIME_P_PART'):
                        cp = Checkpoint(b, event_dir == 1, 0, tuple(points[0]), tuple(points[1]))
                        beacon_file.checkpoints.append(cp)

            # Convert difficulty.
            difficulty.difficulty_name = dif.name
            if self.export_trails_forward and not reverse or self.export_trails_reverse and reverse:
                graph = Graph(self.context, self, reverse, dif)
                export_map()
                export_path()
                if dif == Dif.HARD:
                    export_checkpoints()
            else:
                export_dummy_map()
                export_dummy_path()
                if dif == Dif.HARD:
                    export_dummy_checkpoints()

        # Convert direction.
        export_beacons()
        export_phases()
        export_gates()
        export_difficulty(direction.difficulty_easy, Dif.EASY)
        export_difficulty(direction.difficulty_medium, Dif.MEDIUM)
        export_difficulty(direction.difficulty_hard, Dif.HARD)
        export_times()

    def export_concurrent(self, concurrent: CircuitConcurrent, dif: Dif):
        concurrent.difficulty_name = dif.name
        concurrent.competitor_filename = self.create_filename("lev", False, dif)
        competitors = concurrent.competitor_file.competitors
        # Convert competitors.
        props: POD_CircuitScene = self.context.scene.pod_circuit
        bcompetitors: list[POD_CircuitCompetitor] = props.competitors
        for bcompetitor in bcompetitors:
            if Dif[bcompetitor.difficulty] != dif:
                continue
            competitor = Competitor()
            competitor.name = bcompetitor.name
            competitor.level = bcompetitor.level
            competitor.behavior = CompetitorBehavior[bcompetitor.behavior].value
            competitor.car_index = str(bcompetitor.car_index)
            competitors.append(competitor)
            # Convert competitor attacks.
            for battack in bcompetitor.attacks:
                battack: POD_CircuitCompetitorAttack
                if not battack.percentage:
                    continue
                attack = CompetitorAttack()
                attack.type = CompetitorAttackType[battack.type]
                attack.context = CompetitorAttackContext(butils.get_enum_value(battack, "context"))
                attack.percentage = battack.percentage
                competitor.attacks.append(attack)

    def install(self) -> None:
        # Generate circuit info.
        props: POD_CircuitPatcher = self.context.scene.pod_circuit.patcher
        info = CircuitInfo()
        info.name = self.circuit_name
        info.lev_name = self.circuit_name
        info.tga_name = f"sm_{self.circuit_name[:5]}.tga"
        info.scotch = f"sk_{self.circuit_name[:5]}.tga"
        info.scotch_mirror = f"sk_{self.circuit_name[:4]}_2.tga"
        info.visible = True
        info.mirror = props.reversable
        info.version = 1
        info.id = props.id
        info.length = int(self.circuit.direction_forward.difficulty_easy.path_file.circuit_length)
        info.laps = props.laps
        info.parts = len(self.circuit.direction_forward.beacon_file.checkpoints)

        # Generate image file.
        image_file = ImageFile()

        def add_image(pixels: list[float], width: int, height: int, palette: None | imgconv.Colors = None) -> None:
            image = Image(width, height, len(image_file.images), len(image_file.data))
            image_file.images.append(image)
            if palette is None:
                image_file.data += imgconv.save_pixels_555(pixels, (width, height))
            else:
                image.palette = len(image_file.palettes)
                image_file.palettes.append(imgconv.save_palette_555(palette))
                image_file.data += imgconv.save_pixels_pal(pixels, (width, height), palette)

        palette_path = pathlib.Path(__file__).resolve().parent / "img_palette.555"
        with open(palette_path, "rb") as palette_file:
            palette = imgconv.load_palette_555(palette_file.read())
        add_image(props.img.pixels, 288, 167, palette)
        add_image(props.img_name.pixels, 68, 17)
        add_image(props.img_name_reverse.pixels, 68, 17)

        # Generate sound file.
        meg_file = MegaFile()

        # Create and update requested files.
        def write_file(path: str, data: BinaryWritable) -> None:
            with BinaryWriter(open(path, "w+b")) as write:
                write.any(data)

        circuit_folder = pathlib.Path(self.properties.filepath).parent

        if self.install_patch:
            # Create circuits.bin slot.
            script = CircuitScript()
            script.infos.append(info)
            write_file(circuit_folder / "circuits.bin", script)
            # Create image and sound file.
            write_file(circuit_folder / f"{self.circuit_name}.img", image_file)
            write_file(circuit_folder / f"{self.circuit_name}.meg", meg_file)

        if self.install_game:
            # Update circuits.bin.
            path = circuit_folder.parent / "scripts" / "circuits.bin"
            with BinaryReader(open(path, "rb")) as read:
                script = read.any(CircuitScript)
            # Check if it exists in circuits.bin and update there.
            found = False
            for i, it in enumerate(script.infos):
                if it.id == info.id:
                    script.infos[i] = info
                    found = True
                    break
            if not found:
                # Append to circuits.bin.
                script.infos.append(info)
            write_file(path, script)
            # Create image and sound file.
            write_file(circuit_folder.parent / "mimg" / f"{self.circuit_name}.img", image_file)
            write_file(circuit_folder.parent / "resson" / f"{self.circuit_name}.meg", meg_file)


def register() -> None:
    bpy.types.TOPBAR_MT_file_export.append(POD_OT_CircuitExport.menu)


def unregister() -> None:
    bpy.types.TOPBAR_MT_file_export.remove(POD_OT_CircuitExport.menu)
