import bpy

from ..props.world import POD_CircuitWorld


class POD_PT_CircuitWorld(bpy.types.Panel):
    bl_context = "world"
    bl_label = "Pod Circuit"
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'

    def draw(self, context: bpy.types.Object) -> None:
        pass


class CircuitWorldPanel:
    bl_options = {'DEFAULT_CLOSED'}
    bl_parent_id = POD_PT_CircuitWorld.__name__
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'

    def configure(self, context: bpy.types.Context) -> tuple[bpy.types.UILayout, POD_CircuitWorld]:
        layout = self.layout
        layout.use_property_decorate = False
        layout.use_property_split = True
        return layout, context.scene.world.pod_circuit


class POD_PT_CircuitWorldBackground(bpy.types.Panel, CircuitWorldPanel):
    bl_label = ""
    bl_order = 1

    def draw_header(self, context: bpy.types.Context):
        props: POD_CircuitWorld = context.scene.world.pod_circuit
        self.layout.prop(props, "bg_on", text="Background")

    def draw(self, context: bpy.types.Context) -> None:
        layout, props = self.configure(context)
        layout.active = props.bg_on
        layout.prop(props, "bg_color", text="Color", )
        layout.template_ID(props, "bg_texture", text="Texture", new="image.new", open="image.open")
        layout.prop(props, "bg_texture_start", text="Texture Start")
        layout.prop(props, "bg_texture_end", text="Texture End")


class POD_PT_CircuitWorldFog(bpy.types.Panel, CircuitWorldPanel):
    bl_label = "Fog"
    bl_order = 2

    def draw(self, context: bpy.types.Context) -> None:
        layout, props = self.configure(context)
        layout.prop(props, "fog_distance", text="Distance")
        layout.prop(props, "fog_intensity", text="Intensity")
        layout.prop(props, "fog_mist_z", text="Mist Z")
        layout.prop(props, "fog_mist_intensity", text="Mist Intensity")


class POD_PT_CircuitWorldSky(bpy.types.Panel, CircuitWorldPanel):
    bl_label = ""
    bl_order = 3

    def draw_header(self, context: bpy.types.Context):
        props: POD_CircuitWorld = context.scene.world.pod_circuit
        self.layout.prop(props, "sky_on", text="Sky")

    def draw(self, context: bpy.types.Object) -> None:
        layout, props = self.configure(context)
        layout.active = props.sky_on
        layout.prop(props, "sky_type", text="Type")
        layout.prop(props, "sky_z", text="Z")
        layout.prop(props, "sky_zoom", text="Zoom")
        layout.prop(props, "sky_gouraud_intensity", text="Gouraud Intensity")
        layout.prop(props, "sky_gouraud_start", text="Gouraud Start")
        layout.prop(props, "sky_speed", text="Speed")
        layout.template_ID(props, "sky_texture", text="Texture", new="image.new", open="image.open")


class POD_PT_CircuitWorldSun(bpy.types.Panel, CircuitWorldPanel):
    bl_label = ""
    bl_order = 4

    def draw_header(self, context: bpy.types.Context):
        props: POD_CircuitWorld = context.scene.world.pod_circuit
        self.layout.prop(props, "sun_on", text="Sun")

    def draw(self, context: bpy.types.Object) -> None:
        layout, props = self.configure(context)
        layout.active = props.sun_on
        layout.template_ID(props, "sun_texture", text="Texture", new="image.new", open="image.open")
        layout.prop(props, "sun_color", text="Color")
