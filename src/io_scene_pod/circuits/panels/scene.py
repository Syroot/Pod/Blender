import bpy

from .. import utils
from ..lists.competitor_attacks import POD_UL_CircuitCompetitorAttacks
from ..lists.competitors import POD_UL_CircuitCompetitors
from ..lists.events import draw_event_list
from ..menus.competitor_attacks import POD_MT_CircuitCompetitorAttacksMenu
from ..ops.competitor_add import POD_OT_CircuitCompetitorAdd
from ..ops.competitor_attack_add import POD_OT_CircuitCompetitorAttackAdd
from ..ops.competitor_attack_remove import POD_OT_CircuitCompetitorAttackRemove
from ..ops.competitor_remove import POD_OT_CircuitCompetitorRemove
from ..props.scene import (POD_CircuitPatcher, POD_CircuitPhases, POD_CircuitPhasesDirection, POD_CircuitScene,
                           POD_CircuitTimes, POD_CircuitTimesDirection)
from ..props.winman import POD_CircuitWinman


class POD_PT_CircuitScene(bpy.types.Panel):
    bl_context = "scene"
    bl_label = "Pod Circuit"
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'

    def draw(self, context: bpy.types.Object) -> None:
        pass


class CircuitScenePanel:
    bl_options = {'DEFAULT_CLOSED'}
    bl_parent_id = POD_PT_CircuitScene.__name__
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'

    def configure(self, context: bpy.types.Context) -> tuple[bpy.types.UILayout, POD_CircuitScene]:
        layout = self.layout
        layout.use_property_decorate = False
        layout.use_property_split = True
        return layout, context.scene.pod_circuit


class POD_PT_CircuitScenePatcher(bpy.types.Panel, CircuitScenePanel):
    bl_label = "Patcher"
    bl_order = 1

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        layout.use_property_decorate = False
        layout.use_property_split = True
        props: POD_CircuitPatcher = context.scene.pod_circuit.patcher
        layout.prop(props, "id")
        layout.prop(props, "laps")
        layout.prop(props, "reversable")
        layout.template_ID(props, "img", text="Image", new="image.new", open="image.open")
        layout.template_ID(props, "img_name", text="Name Image", new="image.new", open="image.open")
        layout.template_ID(props, "img_name_reverse", text="Reverse Name Image", new="image.new", open="image.open")


class POD_PT_CircuitSceneTimes(bpy.types.Panel, CircuitScenePanel):
    bl_label = ""
    bl_order = 2

    def draw_header(self, context: bpy.types.Object) -> None:
        layout = self.layout
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        layout.label(text="Times")
        utils.draw_partition_header(layout, wm_props, "times_reverse", None)

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        times_props: POD_CircuitTimes = context.scene.pod_circuit.times
        props: POD_CircuitTimesDirection = times_props.reverse if wm_props.times_reverse else times_props.forward
        grid = layout.grid_flow(columns=3, align=True, row_major=True)
        grid.label()
        head = grid.row()
        head.alignment = 'CENTER'
        head.label(text="Minimum")
        head = grid.row()
        head.alignment = 'CENTER'
        head.label(text="Average")
        for i in range(1, 5):
            head = grid.row()
            head.alignment = 'RIGHT'
            head.label(text=f"Part {i}")
            grid.prop(props, f"min_part_{i}", text=" ")
            grid.prop(props, f"avg_part_{i}", text=" ")


class POD_PT_CircuitScenePhases(bpy.types.Panel, CircuitScenePanel):
    bl_label = ""
    bl_order = 3

    def draw_header(self, context: bpy.types.Object) -> None:
        layout = self.layout
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        layout.label(text="Phases")
        utils.draw_partition_header(layout, wm_props, "phases_reverse", None)

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        phases_props: POD_CircuitPhases = context.scene.pod_circuit.phases
        props: POD_CircuitPhasesDirection = phases_props.reverse if wm_props.phases_reverse else phases_props.forward
        layout.label(text="Arrival Events")
        draw_event_list(layout, props, "arrival_events")
        layout.label(text="Load Events")
        draw_event_list(layout, props, "load_events")
        layout.label(text="Start Events")
        draw_event_list(layout, props, "start_events")
        layout.label(text="Finish Events")
        draw_event_list(layout, props, "finish_events")


class POD_PT_CircuitSceneCompetitors(bpy.types.Panel, CircuitScenePanel):
    bl_label = ""
    bl_order = 4

    def draw_header(self, context: bpy.types.Object) -> None:
        layout = self.layout
        props: POD_CircuitWinman = context.window_manager.pod_circuit
        layout.label(text="Competitors")
        utils.draw_partition_header(layout, props, None, "competitor_difficulty")

    def draw(self, context: bpy.types.Object) -> None:
        layout = self.layout
        props: POD_CircuitScene = context.scene.pod_circuit
        wm_props: POD_CircuitWinman = context.window_manager.pod_circuit
        # Draw competitor list.
        row = layout.row()
        table = row.column(align=True)
        table_head: bpy.types.UILayout = table.box()
        table_head.scale_y = 0.6
        table_cols = table_head.column_flow(columns=4)
        table_cols.label(text="Name")
        table_cols.label(text="Level")
        table_cols.label(text="Behavior")
        table_cols.label(text="Car Index")
        table.template_list(POD_UL_CircuitCompetitors.__name__, "",
                            props, "competitors",
                            wm_props, "competitor_index",
                            rows=7)
        col = row.column(align=True)
        col.operator(POD_OT_CircuitCompetitorAdd.bl_idname, icon='ADD', text="")
        col.operator(POD_OT_CircuitCompetitorRemove.bl_idname, icon='REMOVE', text="")
        # Draw selected competitor attack list.
        if 0 <= wm_props.competitor_index < len(props.competitors):
            competitor_props = props.competitors[wm_props.competitor_index]
            layout.label(text="Selected Competitor Attacks")
            row = layout.row()
            table = row.column(align=True)
            table_head: bpy.types.UILayout = table.box()
            table_head.scale_y = 0.6
            table_cols = table_head.column_flow(columns=3)
            table_cols.label(text="Type")
            table_cols.label(text="Context")
            table_cols.label(text="Probability")
            table.template_list(POD_UL_CircuitCompetitorAttacks.__name__, "",
                                competitor_props, "attacks",
                                wm_props, "competitor_attack_index",
                                rows=4)
            col = row.column(align=True)
            col.operator(POD_OT_CircuitCompetitorAttackAdd.bl_idname, icon='ADD', text="")
            col.operator(POD_OT_CircuitCompetitorAttackRemove.bl_idname, icon='REMOVE', text="")
            col.separator()
            col.menu(POD_MT_CircuitCompetitorAttacksMenu.__name__, icon='DOWNARROW_HLT', text="")
