import bpy

from .. import utils
from ..lists.events import draw_event_list
from ..lists.sector_zones import POD_UL_CircuitSectorZones
from ..menus.sector_zone import POD_MT_CircuitSectorZoneMenu
from ..ops.sector_add_all_zones import POD_OT_CircuitSectorAddAllZones
from ..ops.trail_split import POD_OT_CircuitTrailSplit
from ..props.object import (POD_CircuitBeacon, POD_CircuitObject, POD_CircuitSector, POD_CircuitSpeaker,
                            POD_CircuitTrail)


class POD_PT_CircuitObject(bpy.types.Panel):
    bl_context = "object"
    bl_label = ""
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'

    def draw_header(self, context: bpy.types.Object) -> None:
        layout = self.layout
        ob = context.object
        props: POD_CircuitObject = ob.pod_circuit
        layout.label(text="Pod")
        layout.prop(props, "type", text="")
        if props.type == 'BEACON':
            utils.draw_partition_header(layout, props.beacon, "reverse", "difficulties")
        elif props.type == 'GATE':
            utils.draw_partition_header(layout, props.gate, "reverse", "difficulties")
        elif props.type == 'TRAIL':
            utils.draw_partition_header(layout, props.trail, "reverse", "difficulties")

    def draw(self, context: bpy.types.Object) -> None:
        layout = self.layout
        layout.use_property_decorate = False
        layout.use_property_split = True
        props: POD_CircuitObject = context.object.pod_circuit
        if props.type == 'SECTOR':
            self.draw_sector(context, props.sector)
        elif props.type == 'SPEAKER':
            self.draw_speaker(context, props.speaker)
        elif props.type == 'PIT':
            self.draw_pit(context)
        elif props.type == 'BEACON':
            self.draw_beacon(context, props.beacon)
        elif props.type == 'TRAIL':
            self.draw_trail(context, props.trail)
        else:
            layout.label(text="No Properties")

    def draw_sector(self, context: bpy.types.Context, sector: POD_CircuitSector) -> None:
        layout = self.layout
        wm = context.window_manager
        row = layout.row()
        col = row.split()
        col.label(text="Visible Sectors")
        col.operator(POD_OT_CircuitSectorAddAllZones.bl_idname, text="Show All Anywhere")
        row = layout.row()
        row.template_list(POD_UL_CircuitSectorZones.__name__, "", context.scene, "objects", wm.pod_circuit, "zone_index")
        col = row.column(align=True)
        col.menu(POD_MT_CircuitSectorZoneMenu.__name__, icon='DOWNARROW_HLT', text="")

    def draw_speaker(self, context: bpy.types.Context, speaker: POD_CircuitSpeaker) -> None:
        layout = self.layout
        speaker.sound.draw(layout, "Sound ID")

    def draw_pit(self, context: bpy.types.Context) -> None:
        layout = self.layout
        layout.prop(context.scene.pod_circuit.pits, "repair_seconds")

    def draw_beacon(self, context: bpy.types.Context, beacon: POD_CircuitBeacon) -> None:
        layout = self.layout
        layout.label(text="Positive Events", icon='ADD')
        draw_event_list(layout, beacon, "positive_events")
        layout.label(text="Negative Events", icon='REMOVE')
        draw_event_list(layout, beacon, "negative_events")

    def draw_trail(self, context: bpy.types.Context, trail: POD_CircuitTrail) -> None:
        layout = self.layout
        col = layout.column(align=True)
        col.prop(trail, "beacon", icon='ORIENTATION_NORMAL')
        if trail.beacon:
            col.prop(trail, "beacon_direction", text="Direction")
        layout.prop(trail, "level")
        col = layout.column(align=True)
        col.prop(trail, "constraint_type")
        col.prop(trail, "constraint_param", text="Parameter")
        col.prop(trail, "constraint_number", text="Number")
        layout.prop(trail, "graph")
        layout.prop(trail, "shortcut")
        layout.operator(POD_OT_CircuitTrailSplit.bl_idname, icon='ORIENTATION_NORMAL')
