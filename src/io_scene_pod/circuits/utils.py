import enum

import bpy
from mathutils import Vector

from .. import butils


class Dif(enum.IntFlag):
    EASY = enum.auto()
    MEDIUM = enum.auto()
    HARD = enum.auto()


class EventName(enum.IntEnum):
    COND_ELSE = 0
    COND_COMPETITOR = 1
    COND_PLAYER = 2
    COND_PROB = 3
    COND_LAP = 4
    MACRO_ENABLE = 5
    INT_ENV_OFF = 6
    INT_ENV_ON = 7
    MACRO_ALL_OFF = 8
    MACRO_ALL_ON = 9
    ANI_PAUSE = 10
    ANI_BLOCK = 11
    RACE_CAMERA_FREE = 12
    RACE_CAMERA_CEILING = 13
    RACE_CONSTRAINT = 14
    COND_BEGIN = 15
    RACE_DECIDE_PIT = 16
    RACE_DAMAGE_OFF = 17
    RACE_DAMAGE_ON = 18
    ANI_MOVE = 19
    MACRO_DISABLE = 20
    MACRO_EXCHANGE = 21
    ANI_FIX = 22
    RACE_FINISH_COURSE = 23
    RACE_FINISH_ROLLING = 24
    COND_END = 25
    MACRO_INIT = 26
    ANI_START = 27
    MACRO_TRIGGER = 28
    TIME_N_PART = 29
    TIME_N_LAP = 30
    TIME_P_PART = 31
    TIME_P_LAP = 32
    RACE_SHORTCUT = 33
    ANI_RESET = 34
    ANI_RESTART = 35
    MACRO_REPLACE = 36
    SOUND_REVERB_RESET = 37
    SOUND_REVERB_UPDATE = 38
    SOUND_PLAY_LOCAL = 39
    SOUND_PLAY_SOURCE = 40
    SOUND_PLAY_STATIC = 41
    ANI_STOP = 42
    RACE_SECTION = 43
    SOUND_STOP_LOCAL = 44
    SOUND_STOP_SOURCE = 45
    SOUND_STOP_STATIC = 46
    RACE_TUNNEL_ENTER = 47
    RACE_TUNNEL_EXIT = 48
    RACE_WRONGWAY_OFF = 49
    RACE_WRONGWAY_ON = 50
    INT_BREAK = 51


def is_beacon(ob: bpy.types.Object, reverse: bool) -> bool:
    return ob and ob.pod_circuit.type == 'BEACON' \
        and ob.pod_circuit.beacon.reverse == reverse


def is_gate(ob: bpy.types.Object, reverse: bool) -> bool:
    return ob and ob.pod_circuit.type == 'GATE' \
        and ob.pod_circuit.gate.reverse == reverse


def is_pit(ob: bpy.types.Object) -> bool:
    return ob and ob.pod_circuit.type == 'PIT'


def is_sector(ob: bpy.types.Object) -> bool:
    return ob and ob.pod_circuit.type == 'SECTOR'


def is_speaker(ob: bpy.types.Object) -> bool:
    return ob and ob.pod_circuit.type == 'SPEAKER'


def is_trail(ob: bpy.types.Object, reverse: bool, dif: Dif) -> bool:
    return ob and ob.pod_circuit.type == 'TRAIL' \
        and ob.pod_circuit.trail.reverse == reverse \
        and dif.name in ob.pod_circuit.trail.difficulties


def add_sector_zone(sector_ob: bpy.types.Object, zone_ob: bpy.types.Object) -> bool:
    if sector_ob == zone_ob:  # must not be itself
        return False
    if not is_sector(zone_ob):  # must be a sector
        return False
    if zone_ob in (zone.ob for zone in sector_ob.pod_circuit.sector.zones):  # must be unique
        return False
    sector_ob.pod_circuit.sector.zones.add().ob = zone_ob
    return True


def draw_partition_header(layout: bpy.types.UILayout, data: bpy.types.AnyType, reverse_propname: str, difficulty_propname: str) -> None:
    if reverse_propname:
        row = layout.row()
        row.scale_x = 0.8
        row.prop(data, reverse_propname, text="Reverse", toggle=True)
    if difficulty_propname:
        row = layout.row()
        row.scale_x = 0.6
        row.prop(data, difficulty_propname, expand=True)


def get_beacon_event_dir(ob: bpy.types.Object, event_type: str, dir: int = 0) -> int:
    beacon = ob.pod_circuit.beacon
    if dir >= 0:
        for event in beacon.positive_events:
            if event.type == event_type:
                return 1
    if dir <= 0:
        for event in beacon.negative_events:
            if event.type == event_type:
                return -1
    return 0


def get_beacon_model(context: bpy.types.Context, ob: bpy.types.Object) -> tuple[list[Vector], Vector]:
    bm = butils.bmesh_from_object(ob, context.evaluated_depsgraph_get())
    points = [v.co for v in bm.verts]
    normal = next(f.normal for f in bm.faces)
    bm.free()
    return points, normal


def get_beacon_pass_dir(points: list[Vector], normal: Vector, start: Vector, end: Vector) -> int:
    # Test start and end to be in front of / behind the beacon.
    ds = (points[0] - start).dot(normal)
    de = (points[0] - end).dot(normal)
    if ds < 0:
        if de <= 0:
            return 0
        dir = -1
    else:
        if de >= 0:
            return 0
        dir = 1
    # Test intersection.
    TOLERANCE = 32 / (1 << 16)
    p = start + (end - start) * (ds / (ds - de))
    w = points[1].x - points[0].x
    d = points[1].y - points[0].y
    if w > +TOLERANCE and (points[0].x - TOLERANCE > p.x or points[1].x + TOLERANCE < p.x):
        return 0
    if w < -TOLERANCE and (points[1].x - TOLERANCE > p.x or points[0].x + TOLERANCE < p.x):
        return 0
    if d > +TOLERANCE and (points[0].y - TOLERANCE > p.y or points[1].y + TOLERANCE < p.y):
        return 0
    if d < -TOLERANCE and (points[1].y - TOLERANCE > p.y or points[0].y + TOLERANCE < p.y):
        return 0
    if p.z < points[0].z or p.z > points[2].z:
        return 0
    return dir


def get_ob_brightness(ob: bpy.types.Object) -> list[int]:
    brightness = [64] * len(ob.data.vertices)
    for attr in ob.data.color_attributes:
        if attr.name == "Brightness" and attr.data_type == 'FLOAT_COLOR' and attr.domain == 'POINT':
            for i in range(len(brightness)):
                brightness[i] = round(attr.data[i].color_srgb[0] * 128)
            break
    return brightness


def set_ob_brightness(context: bpy.types.Context, ob: bpy.types.Object, brightness: list[int] = None) -> None:
    assert isinstance(ob.data, bpy.types.Mesh)
    mesh: bpy.types.Mesh = ob.data
    try:
        attr = next(a for a in mesh.color_attributes if a.name == "Brightness" and a.type == 'FLOAT_COLOR' and a.domain == 'POINT')
    except StopIteration:
        if context.mode == 'EDIT_MESH':
            bpy.ops.object.mode_set(mode='OBJECT')  # as otherwise len(attr.data) == 0
        attr = mesh.color_attributes.new(name="Brightness", type='FLOAT_COLOR', domain='POINT')
        mesh.attributes.active_color_index = 0
    brightness = brightness or [64] * len(mesh.vertices)
    for i, g in enumerate(brightness):
        v = g / 128
        attr.data[i].color_srgb = (v, v, v, 1)
