import bpy

from ..ops.competitor_attack_sort import POD_OT_CircuitCompetitorAttackSort


class POD_MT_CircuitCompetitorAttacksMenu(bpy.types.Menu):
    bl_label = "Competitor Attacks"

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        layout.operator(POD_OT_CircuitCompetitorAttackSort.bl_idname, icon='SORT_ASC', text="Sort by Context")
