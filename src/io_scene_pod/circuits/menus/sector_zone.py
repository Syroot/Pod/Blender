import bpy

from ..ops.sector_add_selected_zones import POD_OT_CircuitSectorAddSelectedZones
from ..ops.sector_remove_selected_zones import POD_OT_CircuitSectorRemoveSelectedZones
from ..ops.sector_select_zones import POD_OT_CircuitSectorSelectZones


class POD_MT_CircuitSectorZoneMenu(bpy.types.Menu):
    bl_label = "Visible Sector Actions"

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        layout.operator(POD_OT_CircuitSectorAddSelectedZones.bl_idname, icon='HIDE_OFF', text="Show Selected")
        layout.operator(POD_OT_CircuitSectorRemoveSelectedZones.bl_idname, icon='HIDE_ON', text="Hide Selected")
        layout.separator()
        layout.operator(POD_OT_CircuitSectorSelectZones.bl_idname, text="Select Visible")
