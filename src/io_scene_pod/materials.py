from collections.abc import Iterator

import bpy
from ubipod.object import Effect, FaceProperty

# Material mapping requires a ShaderNodeBsdfPrinciple:
# - input "Base Color" determines Effect (image / color)
# - input "IOR" determines Gouraud off (1.0) or on (other values)
# - material "Backface Culling" determines "Show Backface" (inverted)
# - Transparent, Dural Effect are currently not simulated and stored explicitly per material


def _get_nodes(mat: bpy.types.Material, type: type = None) -> Iterator[bpy.types.Node]:
    if mat.use_nodes:
        for node in mat.node_tree.nodes:
            if type and not isinstance(node, type):
                continue
            yield node


def get_surface_node(mat: bpy.types.Material) -> bpy.types.Node | None:
    # Return node connected to Material Output "Surface".
    for output_node in _get_nodes(mat, bpy.types.ShaderNodeOutputMaterial):
        in_surface = output_node.inputs["Surface"]
        if len(in_surface.links) == 1:
            return in_surface.links[0].from_node


def _get_key(effect: Effect, color: tuple[float, float, float], img: bpy.types.Image, property: FaceProperty, mirror: int):
    name = ""
    if effect.is_color():
        name += ''.join(f'{int(x * 255):02X}' for x in color)
    else:
        name += img.name if img else ""
    part = ""
    if property.road:
        part += "R"
    if property.wall:
        part += "W"
    if property.slip:
        part += str(property.slip)
    if part:
        name += f"_{part}"
    part = ""
    if effect.is_gouraud():
        part += "G"
    if property.backface:
        part += "B"
    if property.trans:
        part += "T"
    if property.dural:
        part += "D"
    if property.lod:
        part += f"L{property.lod}"
    if mirror:
        part += f"M{mirror}"
    if part:
        name += f"_{part}"
    return name


def get_material_key(mat: bpy.types.Material) -> str:
    effect = get_effect(mat)
    if effect.is_color():
        color = get_color(mat)
        img = None
    else:
        color = None
        img = get_image(mat)
    property = FaceProperty(False, get_road(mat), get_wall(mat), get_slip(mat), get_backface(mat), get_trans(mat),
                            get_dural(mat), get_lod(mat))
    return _get_key(effect, color, img, property, get_mirror(mat))


def is_compatible(mat: bpy.types.Material) -> bool:
    # Require [PrincipledBSDF] -BSDF=Surface> [Output]
    # or      [TexImage] -Color=Base Color> [PrincipledBSDF] -BSDF=Surface> [Output]
    # Check for node-based material with BSDF node connected to Material Output "Surface".
    if not mat.use_nodes:
        return False
    surface_node = get_surface_node(mat)
    if not surface_node:
        return False
    color_in = surface_node.inputs["Base Color"]
    if not color_in:
        return False
    # Check if BSDF "Color" is constant or connected to a texture.
    if len(color_in.links) == 0:
        return True
    elif len(color_in.links) == 1 and isinstance(color_in.links[0].from_node, bpy.types.ShaderNodeTexImage):
        return True
    else:
        return False


def get_effect(mat: bpy.types.Material) -> Effect | None:
    surface_node = get_surface_node(mat)
    color_in = surface_node.inputs["Base Color"]
    ior_in = surface_node.inputs["IOR"]
    # Determine effect from input type and IOR.
    if len(color_in.links) == 0:
        return Effect.FLAT if ior_in.default_value == 1.0 else Effect.GOURAUD
    elif len(color_in.links) == 1 and isinstance(color_in.links[0].from_node, bpy.types.ShaderNodeTexImage):
        return Effect.TEXTURE if ior_in.default_value == 1.0 else Effect.TEXGOU
    return None


def set_effect(mat: bpy.types.Material, effect: Effect) -> None:
    nodes = mat.node_tree.nodes
    links = mat.node_tree.links
    surface_node = get_surface_node(mat)
    # Set constant color or texture.
    color_in = surface_node.inputs["Base Color"]
    if effect.is_color():
        # Remove any linked texture node.
        if color_in.links:
            for link in color_in.links:
                nodes.remove(link.from_node)
        # Remove incompatible dural effect.
        set_dural(mat, False)
    else:
        # Connect existing texture node or add new one.
        found = False
        for node in _get_nodes(mat, bpy.types.ShaderNodeTexImage):
            if len(node.outputs["Color"].links) == 0:
                found = True
                links.new(node.outputs["Color"], color_in)
                break
            elif len(node.outputs["Color"].links) == 1 and node.outputs["Color"].links[0].to_node == surface_node:
                found = True
                break
        if not found:
            found = nodes.new("ShaderNodeTexImage")
            links.new(found.outputs["Color"], color_in)
    # Set IOR.
    ior_in = surface_node.inputs["IOR"]
    ior_in.default_value = 1.075 if effect.is_gouraud() else 1.0


def get_color(mat: bpy.types.Material) -> tuple[float, float, float]:
    color_in = get_surface_node(mat).inputs["Base Color"]
    return color_in.default_value[:3]


def set_color(mat: bpy.types.Material, color: tuple[float, float, float]) -> None:
    color_in = get_surface_node(mat).inputs["Base Color"]
    color_in.default_value = color + (1.0,)


def get_image(mat: bpy.types.Material) -> bpy.types.Image:
    color_in = get_surface_node(mat).inputs["Base Color"]
    return color_in.links[0].from_node.image


def set_image(mat: bpy.types.Material, img: bpy.types.Image) -> None:
    color_in = get_surface_node(mat).inputs["Base Color"]
    color_in.links[0].from_node.image = img


def get_backface(mat: bpy.types.Material) -> bool:
    return not mat.use_backface_culling


def set_backface(mat: bpy.types.Material, backface: bool) -> None:
    mat.use_backface_culling = not backface


def get_road(mat: bpy.types.Material) -> bool:
    return mat.pod.road


def set_road(mat: bpy.types.Material, road: bool) -> None:
    mat.pod.road = road


def get_wall(mat: bpy.types.Material) -> bool:
    return mat.pod.wall


def set_wall(mat: bpy.types.Material, wall: bool) -> None:
    mat.pod.wall = wall


def get_slip(mat: bpy.types.Material) -> int:
    return mat.pod.slip


def set_slip(mat: bpy.types.Material, slip: int) -> None:
    mat.pod.slip = slip


def get_trans(mat: bpy.types.Material) -> bool:
    return mat.pod.trans


def set_trans(mat: bpy.types.Material, trans: bool) -> None:
    mat.pod.trans = trans


def get_dural(mat: bpy.types.Material) -> bool:
    return mat.pod.dural


def set_dural(mat: bpy.types.Material, dural: bool) -> None:
    mat.pod.dural = dural


def get_lod(mat: bpy.types.Material) -> int:
    return mat.pod.lod


def set_lod(mat: bpy.types.Material, lod: int) -> None:
    mat.pod.lod = lod


def get_mirror(mat: bpy.types.Material) -> int:
    return mat.pod.mirror


def set_mirror(mat: bpy.types.Material, mirror: int) -> None:
    mat.pod.mirror = mirror


def acquire(effect: Effect, color: tuple[float, float, float], image: bpy.types.Image, property: FaceProperty, mirror: int):
    key = _get_key(effect, color, image, property, mirror)
    # Try to find existing material matching the parameters.
    for mat in bpy.data.materials:
        if is_compatible(mat) and get_material_key(mat) == key:
            return mat
    # Create new material.
    mat = bpy.data.materials.new(key)
    mat.use_nodes = True
    set_effect(mat, effect)
    if effect.is_color():
        set_color(mat, color)
    else:
        set_image(mat, image)
    set_road(mat, property.road)
    set_wall(mat, property.wall)
    set_slip(mat, property.slip)
    set_backface(mat, property.backface)
    set_trans(mat, property.trans)
    set_dural(mat, property.dural)
    set_lod(mat, property.lod)
    set_mirror(mat, mirror)
    return mat
