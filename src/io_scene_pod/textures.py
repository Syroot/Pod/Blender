import bpy
from ubipod.texture import TextureProject, TextureRegion

from . import imgconv


def load_pal8(name: str, data: bytes, size: int, palette: bytes) -> bpy.types.Image:
    img = bpy.data.images.new(name, size, size)
    img.filepath = img.name + ".tga"
    img.pixels = imgconv.load_pixels_pal(data, (size, size), imgconv.load_palette_888(palette))
    img.pack()
    return img


def load_rgb565(name: str, data: bytes, size: int) -> bpy.types.Image:
    img = bpy.data.images.new(name, size, size)
    img.filepath = img.name + ".tga"
    img.pixels = imgconv.load_pixels_565(data, (size, size))
    img.pack()
    return img


def load_project(project_name: str, project: TextureProject, size: int, bpp: int) -> list[bpy.types.Image]:
    images = []
    for i, page in enumerate(project.texture_pages):
        name = f"{project_name}{i}"
        if bpp == 1:
            img = load_pal8(name, page, size, project.palette)
        elif bpp == 2:
            img = load_rgb565(name, page, size)
        images.append(img)
    return images


def save_rgb565(img: bpy.types.Image) -> bytes:
    return imgconv.save_pixels_565(img.pixels, img.size)


def save_project(project_name: str, project: TextureProject, size: int, bpp: int, images: list[bpy.types.Image]) -> None:
    # Validate image sizes.
    for img in images:
        if img.size[0] != size or img.size[1] != size:
            raise ValueError(f"Texture '{img.name}' must be {size}x{size} pixels large.")
    # Create palette by quantizing all images.
    if bpp == 1:
        img_pixels, palette = imgconv.save_pixels_pal_auto([image.pixels for image in images], (size, size))
        project.palette = imgconv.save_palette_888(palette)
    # Create texture data pages (with dummy region covering each).
    for i, img in enumerate(images):
        project.textures.append([TextureRegion(f"{project_name}{i}.tga", 0, 0, size, size)])
        if bpp == 1:
            data = img_pixels[i]
        elif bpp == 2:
            data = save_rgb565(img)
        project.texture_pages.append(data)
