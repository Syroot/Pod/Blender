import pathlib
from typing import Union

import bpy
import bpy_extras
from ubipod.binary import BinaryReader
from ubipod.images import ImageFile

from ... import imgconv


class POD_OT_ImagesImport(bpy.types.Operator, bpy_extras.io_utils.ImportHelper):
    """Load a Pod IMG images file"""
    bl_idname = "import_scene.img"
    bl_label = "Import Pod Images"
    bl_options = {'UNDO'}
    filename_ext = ".img"
    filter_glob: bpy.props.StringProperty(default="*.img", options={'HIDDEN'})

    @staticmethod
    def menu(menu: bpy.types.Menu, context: bpy.types.Context) -> None:
        menu.layout.operator(POD_OT_ImagesImport.bl_idname, text="Pod Images (.img)")

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        # Read from file.
        with BinaryReader(open(self.properties.filepath, "rb")) as read:
            images = read.any(ImageFile)
        # Convert palettes.
        palettes = [imgconv.load_palette_555(x) for x in images.palettes]
        # Convert images.
        base_name = pathlib.Path(self.properties.filepath).stem
        for i, image in enumerate(images.images):
            name = f"{base_name}{i}"
            data = images.data[image.ptr:]
            img = bpy.data.images.new(name, image.width, image.height)
            if image.palette == -1:
                img.pixels = imgconv.load_pixels_555(data, img.size)
            else:
                img.pixels = imgconv.load_pixels_pal(data, img.size, palettes[image.palette])
            img.pack()
        return {'FINISHED'}


def register() -> None:
    bpy.types.TOPBAR_MT_file_import.append(POD_OT_ImagesImport.menu)


def unregister() -> None:
    bpy.types.TOPBAR_MT_file_import.remove(POD_OT_ImagesImport.menu)
