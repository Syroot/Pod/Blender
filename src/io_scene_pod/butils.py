import re
from typing import Callable

import bmesh
import bpy


def bmesh_from_object(ob: bpy.types.Object, depsgraph: bpy.types.Depsgraph, ignore_transform: bool = False) -> bmesh.types.BMesh:
    bm = bmesh.new()
    bm.from_object(ob, depsgraph)
    if not ignore_transform:
        bm.transform(ob.matrix_world)
        bm.normal_update()
    return bm


def create_collection(name: str, parent: bpy.types.Collection = None) -> bpy.types.Collection:
    if not parent:
        parent = bpy.context.scene.collection
    # Get existing collection.
    for col in parent.children:
        if name_compare(col.name, name):
            return col
    # Create missing collection.
    col = bpy.data.collections.new(name)
    parent.children.link(col)
    return col


def filter_list(list: bpy.types.UIList, data: bpy.types.AnyType, propname: str, name_propname: str,
                filter_predicate: Callable[[object], bool] | None = None) -> tuple[list[int], list[int]]:
    inputs = getattr(data, propname)
    # Apply user filter.
    if list.filter_name:
        flags = bpy.types.UI_UL_list.filter_items_by_name(list.filter_name, list.bitflag_filter_item, inputs, name_propname)
    else:
        flags = [list.bitflag_filter_item] * len(inputs)
    # Apply lambda filter.
    if filter_predicate:
        for i, input in enumerate(inputs):
            if filter_predicate(input):
                if list.use_filter_invert:
                    flags[i] |= list.bitflag_filter_item
                else:
                    flags[i] &= ~list.bitflag_filter_item
    # Apply user order.
    if list.use_filter_sort_alpha:
        order = bpy.types.UI_UL_list.sort_items_by_name(inputs, name_propname)
    else:
        order = []
    return flags, order


def get_enum_name(data: bpy.types.AnyType, propname: str) -> str:
    prop = _get_enum_property(data, propname)
    id = getattr(data, propname)
    return prop.enum_items[id].name


def get_enum_value(data: bpy.types.AnyType, propname: str) -> int:
    prop = _get_enum_property(data, propname)
    ids = getattr(data, propname)
    if prop.is_enum_flag:
        value = 0
        for id in ids:
            value |= prop.enum_items[id].value
        return value
    else:
        return prop.enum_items[ids].value


def name_compare(a: str, b: str) -> bool:
    a = name_trim(a)
    b = name_trim(b)
    return a == b


def name_trim(name: str) -> str:
    res = re.search(r"\.\d\d\d$", name)
    if res:
        return name[0:res.span()[0]]
    return name


def set_enum_value(data: bpy.types.AnyType, propname: str, value: int) -> None:
    prop = _get_enum_property(data, propname)
    if prop.is_enum_flag:
        ids = set()
        remain = value
        for item in prop.enum_items:
            if value & item.value:
                remain &= ~item.value
                ids.add(item.identifier)
        if remain:
            raise KeyError(f"Unknown {propname} enum flags {bin(remain)}.")
        setattr(data, propname, ids)
    else:
        for item in prop.enum_items:
            if item.value == value:
                setattr(data, propname, item.identifier)
                return
        raise KeyError(f"Unknown {propname} enum value {value}.")


def _get_enum_property(data: bpy.types.AnyType, propname: str) -> bpy.types.EnumProperty:
    prop = data.bl_rna.properties[propname]
    assert isinstance(prop, bpy.types.EnumProperty)
    return prop
