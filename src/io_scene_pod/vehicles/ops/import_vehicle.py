from typing import Union

import bpy
import bpy_extras
from ubipod.pbdf import PbdfFormat, PbdfReader
from ubipod.vehicle import AutoPart, Vehicle

from ... import butils, objects, textures
from ..props.scene import POD_Vehicle, POD_VehiclePart, POD_VehiclePhysics, POD_VehicleSounds, POD_VehicleStats


class POD_OT_VehicleImport(bpy.types.Operator, bpy_extras.io_utils.ImportHelper):
    """Load a Pod BVx vehicle file"""
    bl_idname = "import_scene.bvx"
    bl_label = "Import Pod Vehicle"
    bl_options = {'UNDO'}
    filename_ext = ".bv?"
    filter_glob: bpy.props.StringProperty(default="*.bv?", options={'HIDDEN'})

    @staticmethod
    def menu(menu: bpy.types.Menu, context: bpy.types.Context) -> None:
        menu.layout.operator(POD_OT_VehicleImport.bl_idname, text="Pod Vehicle (.bv*)")

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        # Read from file.
        self.format = PbdfFormat(self.properties.filepath)
        with PbdfReader(open(self.properties.filepath, "rb"), self.format.pbdf_key, self.format.pbdf_buf_size) as read:
            read.format = self.format
            self.vehicle = read.any(Vehicle)
        # Convert name.
        bvehicle: POD_Vehicle = context.scene.pod_vehicle
        bvehicle.name = self.vehicle.name
        # Convert auto and physics.
        auto = self.vehicle.auto
        self.import_auto_part(bvehicle.wheel_fr, auto.wheel_fr)
        self.import_auto_part(bvehicle.wheel_rr, auto.wheel_rr)
        self.import_auto_part(bvehicle.wheel_fl, auto.wheel_fl)
        self.import_auto_part(bvehicle.wheel_rl, auto.wheel_rl)
        self.import_auto_part(bvehicle.chassis, auto.chassis)
        bvehicle.motor_curve.items.clear()
        for value in self.vehicle.auto.motor_curves:
            if value:
                bvehicle.motor_curve.items.add().value = value
        motor = self.vehicle.auto.motor
        bphysics: POD_VehiclePhysics = bvehicle.physics
        bphysics.speed_factors_used = motor.speed_factors_used
        bphysics.speed_factors = motor.speed_factors
        bphysics.steer_max = motor.steer_max
        bphysics.steer_speed = motor.steer_speed
        bphysics.steer_recall = motor.steer_recall
        bphysics.unknown494 = motor.unknown494
        bphysics.inertia_moments_jx = motor.inertia_moments[0]
        bphysics.inertia_moments_jy = motor.inertia_moments[1]
        bphysics.inertia_moments_jz = motor.inertia_moments[2]
        bphysics.gear_up_speed = motor.gear_up_speed
        bphysics.gear_down_speed = motor.gear_down_speed
        bphysics.chassis_mass = motor.chassis_mass
        bphysics.gravity_factor = motor.gravity_factor
        bphysics.fin_factor_x = motor.fin_factor[0]
        bphysics.fin_factor_y = motor.fin_factor[1]
        bphysics.brake_distribution = motor.brake_distribution
        bphysics.zoom_factor = motor.zoom_factor
        bphysics.viscous_friction = motor.viscous_friction
        bphysics.brake_slope_curve = motor.brake_slope_curve
        bphysics.brake_max = motor.brake_max
        bphysics.transmission_type = motor.transmission_type.name
        # Convert graphics.
        self.import_graphics()
        # Convert sounds.
        sounds = self.vehicle.sounds
        bsounds: POD_VehicleSounds = bvehicle.sounds
        bsounds.engine.set(sounds.engine)
        bsounds.gear_next.set(sounds.gear_next)
        bsounds.gear_prev.set(sounds.gear_prev)
        bsounds.brake.set(sounds.brake)
        bsounds.brake_stop.set(sounds.brake_stop)
        bsounds.crash_soft.set(sounds.crash_soft)
        bsounds.unknown.set(sounds.unknown)
        bsounds.shock.set(sounds.shock)
        bsounds.accel.set(sounds.accel)
        bsounds.accel_stop.set(sounds.accel_stop)
        bsounds.decel.set(sounds.decel)
        bsounds.decel_stop.set(sounds.decel_stop)
        bsounds.crash.set(sounds.crash)
        bsounds.skid_roof.set(sounds.skid_roof)
        bsounds.skid_roof_stop.set(sounds.skid_roof_stop)
        # Convert stats.
        bstats: POD_VehicleStats = bvehicle.stats
        bstats.accel = self.vehicle.default_accel
        bstats.brakes = self.vehicle.default_brakes
        bstats.grip = self.vehicle.default_grip
        bstats.handling = self.vehicle.default_handling
        bstats.speed = self.vehicle.default_speed
        return {'FINISHED'}

    def import_auto_part(self, bpart: POD_VehiclePart, ppart: AutoPart) -> None:
        bpart.shock_damper = ppart.shock_damper
        bpart.mass = ppart.mass
        bpart.radius = ppart.radius
        bpart.friction = ppart.friction
        bpart.grip_x = ppart.grip[0]
        bpart.grip_y = ppart.grip[1]
        bpart.grip_z = ppart.grip[2]
        bpart.stiffness = ppart.stiffness
        bpart.viscosity = ppart.viscosity
        bpart.z_empty = ppart.z_empty
        bpart.z_max = ppart.z_max
        bpart.z_min = ppart.z_min

    def import_graphics(self) -> None:
        auto = self.vehicle.auto
        graphics = self.vehicle.graphics
        chassis_pos = self.vehicle.auto.chassis.position
        images = textures.load_project(graphics.texture_project_name, graphics.texture_project, self.format.tex_size_small, self.format.tex_bpp)
        # Convert chassis meshes.
        bcol_chassis0 = butils.create_collection("Chassis 0 (Normal)")
        objects.load(graphics.chassis_rr0.object, "ChassisRR0", bcol_chassis0, images).location = chassis_pos
        objects.load(graphics.chassis_rl0.object, "ChassisRL0", bcol_chassis0, images).location = chassis_pos
        objects.load(graphics.chassis_sr0.object, "ChassisSR0", bcol_chassis0, images).location = chassis_pos
        objects.load(graphics.chassis_sl0.object, "ChassisSL0", bcol_chassis0, images).location = chassis_pos
        objects.load(graphics.chassis_fr0.object, "ChassisFR0", bcol_chassis0, images).location = chassis_pos
        objects.load(graphics.chassis_fl0.object, "ChassisFL0", bcol_chassis0, images).location = chassis_pos
        bcol_chassis1 = butils.create_collection("Chassis 1 (Damaged)")
        objects.load(graphics.chassis_rr1.object, "ChassisRR1", bcol_chassis1, images).location = chassis_pos
        objects.load(graphics.chassis_rl1.object, "ChassisRL1", bcol_chassis1, images).location = chassis_pos
        objects.load(graphics.chassis_sr1.object, "ChassisSR1", bcol_chassis1, images).location = chassis_pos
        objects.load(graphics.chassis_sl1.object, "ChassisSL1", bcol_chassis1, images).location = chassis_pos
        objects.load(graphics.chassis_fr1.object, "ChassisFR1", bcol_chassis1, images).location = chassis_pos
        objects.load(graphics.chassis_fl1.object, "ChassisFL1", bcol_chassis1, images).location = chassis_pos
        bcol_chassis2 = butils.create_collection("Chassis 2 (Ruined)")
        objects.load(graphics.chassis_rr2.object, "ChassisRR2", bcol_chassis2, images).location = chassis_pos
        objects.load(graphics.chassis_rl2.object, "ChassisRL2", bcol_chassis2, images).location = chassis_pos
        objects.load(graphics.chassis_sr2.object, "ChassisSR2", bcol_chassis2, images).location = chassis_pos
        objects.load(graphics.chassis_sl2.object, "ChassisSL2", bcol_chassis2, images).location = chassis_pos
        objects.load(graphics.chassis_fr2.object, "ChassisFR2", bcol_chassis2, images).location = chassis_pos
        objects.load(graphics.chassis_fl2.object, "ChassisFL2", bcol_chassis2, images).location = chassis_pos
        # Convert wheel meshes.
        bcol_wheels = butils.create_collection("Wheels")
        objects.load(graphics.wheel_fr.object, "WheelFR", bcol_wheels, images).location = auto.wheel_fr.position
        objects.load(graphics.wheel_rr.object, "WheelRR", bcol_wheels, images).location = auto.wheel_rr.position
        objects.load(graphics.wheel_fl.object, "WheelFL", bcol_wheels, images).location = auto.wheel_fl.position
        objects.load(graphics.wheel_rl.object, "WheelRL", bcol_wheels, images).location = auto.wheel_rl.position
        # Convert shadow meshes.
        bcol_shadow0 = butils.create_collection("Shadow 0 (Normal)")
        objects.load(graphics.shadow_r0, "ShadowR0", bcol_shadow0, images)
        objects.load(graphics.shadow_f0, "ShadowF0", bcol_shadow0, images)
        bcol_shadow2 = butils.create_collection("Shadow 2 (Ruined)")
        objects.load(graphics.shadow_r2, "ShadowR2", bcol_shadow2, images)
        objects.load(graphics.shadow_f2, "ShadowF2", bcol_shadow2, images)
        # Convert level of detail meshes.
        bcol_lod = butils.create_collection("Lod")
        objects.load(self.vehicle.lod1.object, "Lod1", bcol_lod, images)
        objects.load(self.vehicle.lod2.object, "Lod2", bcol_lod, images)


def register() -> None:
    bpy.types.TOPBAR_MT_file_import.append(POD_OT_VehicleImport.menu)


def unregister() -> None:
    bpy.types.TOPBAR_MT_file_import.remove(POD_OT_VehicleImport.menu)
