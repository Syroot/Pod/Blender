import pathlib
from typing import Sequence, Union

import bpy
import bpy_extras
import numpy
from ubipod.binary import BinaryReader, BinaryWritable, BinaryWriter
from ubipod.images import Image, ImageFile
from ubipod.pbdf import PbdfFormat, PbdfWriter
from ubipod.scripts import VehicleInfo, VehicleScript
from ubipod.sounds import MegaFile
from ubipod.vehicle import AutoPart, TransmissionType, Vehicle

from ... import imgconv, objects, textures
from ..props.scene import (POD_Vehicle, POD_VehiclePart, POD_VehiclePatcher, POD_VehiclePhysics, POD_VehicleSounds,
                           POD_VehicleStats)


class POD_OT_VehicleExport(bpy.types.Operator, bpy_extras.io_utils.ExportHelper):
    """Save a Pod BVx vehicle file"""
    bl_idname = "export_scene.bvx"
    bl_label = "Export Pod Vehicle"
    bl_options = {'UNDO'}
    filename_ext = ".bv4"
    filter_glob: bpy.props.StringProperty(default="*.bv?", options={'HIDDEN'})

    @staticmethod
    def menu(menu: bpy.types.Menu, context: bpy.types.Context) -> None:
        menu.layout.operator(POD_OT_VehicleExport.bl_idname, text="Pod Vehicle (.bv*)")

    def export_format_update(self, context: bpy.types.Context) -> None:
        POD_OT_VehicleExport.filename_ext = "." + self.export_format.lower()

    export_format: bpy.props.EnumProperty(name="Format", items=(
        ('BV3', "BV3", "", 3),
        ('BV4', "BV4", "", 4),
        ('BV6', "BV6", "", 6),
        ('BV7', "BV7", "", 7),
        ('BV8', "BV8", "", 8),
        ('BV9', "BV9", "", 9),
    ), default='BV4', update=export_format_update)
    install_game: bpy.props.BoolProperty(name="Install to Game")
    install_patch: bpy.props.BoolProperty(name="Create Patch Files")

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        layout.use_property_decorate = False
        layout.use_property_split = True
        layout.prop(self, "export_format")
        # Draw files panel.
        header, body = layout.panel("POD_OT_CircuitExportInstall", default_closed=False)
        header.label(text="Additional Files")
        if body:
            body.prop(self, "install_game")
            body.prop(self, "install_patch")

    def execute(self, context: bpy.types.Context) -> Union[set[int], set[str]]:
        if not self.validate(context):
            return {'CANCELLED'}
        self.context = context
        if context.mode != 'OBJECT':
            bpy.ops.object.mode_set(mode='OBJECT')
        # Collect information.
        self.vehicle = Vehicle()
        self.vehicle.name = pathlib.Path(self.properties.filepath).stem
        self.format = PbdfFormat(self.properties.filepath)
        # Convert contents.
        self.export_physics(context.scene.pod_vehicle)
        self.export_graphics()
        self.export_sounds(context.scene.pod_vehicle.sounds)
        self.export_stats(context.scene.pod_vehicle.stats)
        # Save vehicle.
        with PbdfWriter(open(self.properties.filepath, "w+b"),
                        self.format.pbdf_key, self.format.pbdf_buf_size, self.format.pbdf_ofs_count) as write:
            write.format = self.format
            write.any(self.vehicle)
        # Save / modify additional files.
        if self.install_game or self.install_patch:
            self.install()
        return {'FINISHED'}

    def export_physics(self, props: POD_Vehicle) -> None:
        def export_part(props: POD_VehiclePart, part: AutoPart) -> None:
            part.shock_damper = props.shock_damper
            part.mass = props.mass
            part.radius = props.radius
            part.friction = props.friction
            part.grip = (props.grip_x, props.grip_y, props.grip_z)
            part.stiffness = props.stiffness
            part.viscosity = props.viscosity
            part.z_empty = props.z_empty
            part.z_max = props.z_max
            part.z_min = props.z_min

        auto = self.vehicle.auto
        export_part(props.wheel_fr, auto.wheel_fr)
        export_part(props.wheel_rr, auto.wheel_rr)
        export_part(props.wheel_fl, auto.wheel_fl)
        export_part(props.wheel_rl, auto.wheel_rl)
        export_part(props.chassis, auto.chassis)
        auto.wheel_fr.position = tuple(self.context.scene.objects["WheelFR"].location)
        auto.wheel_rr.position = tuple(self.context.scene.objects["WheelRR"].location)
        auto.wheel_fl.position = tuple(self.context.scene.objects["WheelFL"].location)
        auto.wheel_rl.position = tuple(self.context.scene.objects["WheelRL"].location)
        auto.chassis.position = tuple(self.context.scene.objects["ChassisRR0"].location)
        auto.motor_curves = [x.value for x in props.motor_curve.items] + [0] * (64 - len(props.motor_curve.items))
        motor = auto.motor
        bphysics: POD_VehiclePhysics = props.physics
        motor.speed_factors_used = bphysics.speed_factors_used
        motor.speed_factors = bphysics.speed_factors
        motor.steer_max = bphysics.steer_max
        motor.steer_speed = bphysics.steer_speed
        motor.steer_recall = bphysics.steer_recall
        motor.unknown494 = bphysics.unknown494
        motor.inertia_moments = (bphysics.inertia_moments_jx, bphysics.inertia_moments_jy, bphysics.inertia_moments_jz)
        motor.gear_up_speed = bphysics.gear_up_speed
        motor.gear_down_speed = bphysics.gear_down_speed
        motor.chassis_mass = bphysics.chassis_mass
        motor.gravity_factor = bphysics.gravity_factor
        motor.fin_factor = (bphysics.fin_factor_x, bphysics.fin_factor_y)
        motor.brake_distribution = bphysics.brake_distribution
        motor.zoom_factor = bphysics.zoom_factor
        motor.viscous_friction = bphysics.viscous_friction
        motor.brake_slope_curve = bphysics.brake_slope_curve
        motor.brake_max = bphysics.brake_max
        motor.transmission_type = TransmissionType[bphysics.transmission_type]

    def export_graphics(self) -> None:
        graphics = self.vehicle.graphics
        obs = self.context.scene.objects
        images = []
        # Convert chassis meshes.
        graphics.has_face_material = True
        graphics.chassis_rr0.object = objects.save(self.context, obs["ChassisRR0"], images, True)
        graphics.chassis_rl0.object = objects.save(self.context, obs["ChassisRL0"], images, True)
        graphics.chassis_sr0.object = objects.save(self.context, obs["ChassisSR0"], images, True)
        graphics.chassis_sl0.object = objects.save(self.context, obs["ChassisSL0"], images, True)
        graphics.chassis_fr0.object = objects.save(self.context, obs["ChassisFR0"], images, True)
        graphics.chassis_fl0.object = objects.save(self.context, obs["ChassisFL0"], images, True)
        graphics.chassis_rr1.object = objects.save(self.context, obs["ChassisRR1"] or obs["ChassisRR0"], images, True)
        graphics.chassis_rl1.object = objects.save(self.context, obs["ChassisRL1"] or obs["ChassisRL0"], images, True)
        graphics.chassis_sr1.object = objects.save(self.context, obs["ChassisSR1"] or obs["ChassisSR0"], images, True)
        graphics.chassis_sl1.object = objects.save(self.context, obs["ChassisSL1"] or obs["ChassisSL0"], images, True)
        graphics.chassis_fr1.object = objects.save(self.context, obs["ChassisFR1"] or obs["ChassisFR0"], images, True)
        graphics.chassis_fl1.object = objects.save(self.context, obs["ChassisFL1"] or obs["ChassisFL0"], images, True)
        graphics.chassis_rr2.object = objects.save(self.context, obs["ChassisRR2"] or obs["ChassisRR1"] or obs["ChassisRR2"], images, True)
        graphics.chassis_rl2.object = objects.save(self.context, obs["ChassisRL2"] or obs["ChassisRL1"] or obs["ChassisRL2"], images, True)
        graphics.chassis_sr2.object = objects.save(self.context, obs["ChassisSR2"] or obs["ChassisSR1"] or obs["ChassisSR2"], images, True)
        graphics.chassis_sl2.object = objects.save(self.context, obs["ChassisSL2"] or obs["ChassisSL1"] or obs["ChassisSL2"], images, True)
        graphics.chassis_fr2.object = objects.save(self.context, obs["ChassisFR2"] or obs["ChassisFR1"] or obs["ChassisFR2"], images, True)
        graphics.chassis_fl2.object = objects.save(self.context, obs["ChassisFL2"] or obs["ChassisFL1"] or obs["ChassisFL2"], images, True)
        graphics.chassis_rr0.prism = graphics.chassis_rr0.object.calc_prism()
        graphics.chassis_rl0.prism = graphics.chassis_rl0.object.calc_prism()
        graphics.chassis_sr0.prism = graphics.chassis_sr0.object.calc_prism()
        graphics.chassis_sl0.prism = graphics.chassis_sl0.object.calc_prism()
        graphics.chassis_fr0.prism = graphics.chassis_fr0.object.calc_prism()
        graphics.chassis_fl0.prism = graphics.chassis_fl0.object.calc_prism()
        graphics.chassis_rr1.prism = graphics.chassis_rr1.object.calc_prism()
        graphics.chassis_rl1.prism = graphics.chassis_rl1.object.calc_prism()
        graphics.chassis_sr1.prism = graphics.chassis_sr1.object.calc_prism()
        graphics.chassis_sl1.prism = graphics.chassis_sl1.object.calc_prism()
        graphics.chassis_fr1.prism = graphics.chassis_fr1.object.calc_prism()
        graphics.chassis_fl1.prism = graphics.chassis_fl1.object.calc_prism()
        graphics.chassis_rr2.prism = graphics.chassis_rr2.object.calc_prism()
        graphics.chassis_rl2.prism = graphics.chassis_rl2.object.calc_prism()
        graphics.chassis_sr2.prism = graphics.chassis_sr2.object.calc_prism()
        graphics.chassis_sl2.prism = graphics.chassis_sl2.object.calc_prism()
        graphics.chassis_fr2.prism = graphics.chassis_fr2.object.calc_prism()
        graphics.chassis_fl2.prism = graphics.chassis_fl2.object.calc_prism()
        # Convert wheel meshes.
        graphics.wheel_fr.object = objects.save(self.context, obs["WheelFR"], images, True)
        graphics.wheel_rr.object = objects.save(self.context, obs["WheelRR"], images, True)
        graphics.wheel_fl.object = objects.save(self.context, obs["WheelFL"], images, True)
        graphics.wheel_rl.object = objects.save(self.context, obs["WheelRL"], images, True)
        graphics.wheel_fr.prism = graphics.wheel_fr.object.calc_prism()
        graphics.wheel_rr.prism = graphics.wheel_rr.object.calc_prism()
        graphics.wheel_fl.prism = graphics.wheel_fl.object.calc_prism()
        graphics.wheel_rl.prism = graphics.wheel_rl.object.calc_prism()
        # Convert shadow meshes.
        graphics.shadow_r0 = objects.save(self.context, obs["ShadowR0"], images)
        graphics.shadow_f0 = objects.save(self.context, obs["ShadowF0"], images)
        graphics.shadow_r2 = objects.save(self.context, obs["ShadowR2"] or obs["ShadowR0"], images)
        graphics.shadow_f2 = objects.save(self.context, obs["ShadowF2"] or obs["ShadowF0"], images)
        # Convert level of detail meshes.
        self.vehicle.lod1.file_name = self.vehicle.name.upper()
        self.vehicle.lod1.has_face_material = True
        self.vehicle.lod1.object = objects.save(self.context, obs["Lod1"], images)
        self.vehicle.lod1.prism = self.vehicle.lod1.object.calc_prism()
        self.vehicle.lod2.file_name = self.vehicle.name.upper()
        self.vehicle.lod2.has_face_material = True
        self.vehicle.lod2.object = objects.save(self.context, obs["Lod2"], images)
        self.vehicle.lod2.prism = self.vehicle.lod2.object.calc_prism()
        # Convert textures.
        graphics.texture_project_name = self.vehicle.name.upper()
        textures.save_project(graphics.texture_project_name, graphics.texture_project,
                              self.format.tex_size_small, self.format.tex_bpp, images)

    def export_sounds(self, props: POD_VehicleSounds) -> None:
        sounds = self.vehicle.sounds
        sounds.engine = props.engine.get()
        sounds.gear_next = props.gear_next.get()
        sounds.gear_prev = props.gear_prev.get()
        sounds.brake = props.brake.get()
        sounds.brake_stop = props.brake_stop.get()
        sounds.crash_soft = props.crash_soft.get()
        sounds.unknown = props.unknown.get()
        sounds.shock = props.shock.get()
        sounds.accel = props.accel.get()
        sounds.accel_stop = props.accel_stop.get()
        sounds.decel = props.decel.get()
        sounds.decel_stop = props.decel_stop.get()
        sounds.crash = props.crash.get()
        sounds.skid_roof = props.skid_roof.get()
        sounds.skid_roof_stop = props.skid_roof_stop.get()

    def export_stats(self, props: POD_VehicleStats) -> None:
        vehicle = self.vehicle
        vehicle.default_accel = props.accel
        vehicle.default_brakes = props.brakes
        vehicle.default_grip = props.grip
        vehicle.default_handling = props.handling
        vehicle.default_speed = props.speed

    def install(self) -> None:
        # Generate vehicle info.
        props: POD_VehiclePatcher = self.context.scene.pod_vehicle.patcher
        info = VehicleInfo()
        info.name = self.vehicle.name
        info.vehicle_name = self.vehicle.name
        info.tga_name = f"24{self.vehicle.name[:6]}.tga"
        info.small_e_name = f"P{self.vehicle.name[:5]}_E.tga"
        info.small_a_name = f"P{self.vehicle.name[:5]}_A.tga"

        # Generate image file.
        image_file = ImageFile()

        def add_image(pixels: Sequence[float], size: tuple[int, int]) -> None:
            image = Image(*size, len(image_file.images), len(image_file.data))
            image_file.images.append(image)
            image_file.data += imgconv.save_pixels_555(pixels, size)

        add_image(props.img.pixels, (333, 257))
        add_image(numpy.array(props.img_small.pixels) * 0.5, (125, 90))
        add_image(props.img_small.pixels, (125, 90))

        # Generate sound file.
        meg_file = MegaFile()

        # Create and update requested files.
        def write_file(path: str, data: BinaryWritable) -> None:
            with BinaryWriter(open(path, "w+b")) as write:
                write.any(data)

        vehicle_folder = pathlib.Path(self.properties.filepath).parent

        if self.install_patch:
            # Create circuits.bin slot.
            script = VehicleScript()
            script.infos.append(info)
            write_file(vehicle_folder / "voitures.bin", script)
            # Create image and sound file.
            write_file(vehicle_folder / f"{self.vehicle.name}.img", image_file)
            write_file(vehicle_folder / f"{self.vehicle.name}.meg", meg_file)

        if self.install_game:
            # Update voitures.bin and voiture2.bin.
            path = vehicle_folder.parent / "scripts" / "voitures.bin"
            with BinaryReader(open(path, "rb")) as read:
                script = read.any(VehicleScript)
            path_all = vehicle_folder.parent / "scripts" / "voiture2.bin"
            with BinaryReader(open(path_all, "rb")) as read:
                script_all = read.any(VehicleScript)
            # Check if it exists in voitures.bin and update there.
            found = False
            for i, it in enumerate(script.infos):
                if it.name.casefold() == info.name.casefold():
                    script.infos[i] = info
                    found = True
                    break
            if not found:
                # Exchange with first vehicle in voitures.bin and check if first exists in voiture2.bin.
                first = script.infos[0]
                script.infos[0] = info
                for i, it in enumerate(script_all.infos):
                    if it.name.casefold() == info.name.casefold():
                        script_all.infos[i] = first
                        found = True
                        break
                if not found:
                    # Append first to voiture2.bin.
                    script_all.infos.append(first)
            write_file(path, script)
            write_file(path_all, script_all)
            # Create image and sound file.
            write_file(vehicle_folder.parent / "mimg" / f"{self.vehicle.name}.img", image_file)
            write_file(vehicle_folder.parent / "resson" / f"{self.vehicle.name}.meg", meg_file)

    def validate(self, context: bpy.types.Context) -> bool:
        valid = True

        def fail(msg: str) -> None:
            nonlocal valid
            self.report({'ERROR'}, "ERROR: " + msg)
            valid = False

        def warn(msg: str) -> None:
            self.report({'WARNING'}, msg)

        # Validate existence of chassis (require undamaged and warn about missing damaged or ruined).
        for part in ["RR", "RL", "SR", "SL", "FR", "FL"]:
            if not context.scene.objects.get(f"Chassis{part}0"):
                fail(f"Missing 'Chassis{part}0' undamaged mesh.")
                continue
            if not context.scene.objects.get(f"Chassis{part}1"):
                if context.scene.objects.get(f"Chassis{part}2"):
                    warn(f"Missing 'Chassis{part}1' damaged mesh will be replaced with ruined mesh.")
                else:
                    warn(f"Missing 'Chassis{part}1' damaged mesh will be replaced with undamaged mesh.")
            if not context.scene.objects.get(f"Chassis{part}2"):
                if context.scene.objects.get(f"Chassis{part}1"):
                    warn(f"Missing 'Chassis{part}2' ruined mesh will be replaced with damaged mesh.")
                else:
                    warn(f"Missing 'Chassis{part}2' ruined mesh will be replaced with undamaged mesh.")
        # Validate existence of wheels.
        for part in ["FR", "RR", "FL", "RL"]:
            if not context.scene.objects.get(f"Wheel{part}"):
                fail(f"Missing 'Wheel{part}' mesh.")
        # Validate existence of shadows (require undamaged and warn about missing ruined).
        for part in ["R", "F"]:
            if not context.scene.objects.get(f"Shadow{part}0"):
                fail(f"Missing 'Shadow{part}0' undamaged mesh.")
                continue
            if not context.scene.objects.get(f"Shadow{part}2"):
                warn(f"Missing 'Shadow{part}2' ruined mesh will be replaced with normal mesh.")
        # Validate existence of level of detail models.
        for i in range(1, 3):
            if not context.scene.objects.get(f"Lod{i}"):
                fail(f"Missing 'Lod{i}' mesh.")
        # Validate vehicle stats.
        total = context.scene.pod_vehicle.stats.get_total()
        if total > 300:
            fail("Vehicle stats exceed 300 points, game will exit on load. Validate stats in Scene property panel.")
        elif total < 300:
            warn("Vehicle stats are less than 300 points, this is untypical.")
        return valid


def register() -> None:
    bpy.types.TOPBAR_MT_file_export.append(POD_OT_VehicleExport.menu)


def unregister() -> None:
    bpy.types.TOPBAR_MT_file_export.remove(POD_OT_VehicleExport.menu)
