from typing import Union

import bpy


class POD_OT_VehicleMotorCurveActions(bpy.types.Operator):
    bl_idname = "scene.pod_vehicle_motor_curve_actions"
    bl_label = "Motor Curve Actions"
    bl_description = "Add, remove, or sort motor curve items"
    bl_options = {'INTERNAL', 'UNDO'}
    action: bpy.props.EnumProperty(items=(
        ('ADD', "Add", ""),
        ('REMOVE', "Remove", ""),
        ('UP', "Up", ""),
        ('DOWN', "Down", ""),
    ))

    def invoke(self, context: bpy.types.Context, event: bpy.types.Event) -> Union[set[int], set[str]]:
        motor_curve = context.scene.pod_vehicle.motor_curve
        index = motor_curve.index
        items = motor_curve.items

        if self.action == 'ADD':
            if len(items) < 64:
                motor_curve.items.add()
                motor_curve.index = len(items) - 1
        elif index < len(motor_curve.items):
            if self.action == 'REMOVE':
                motor_curve.items.remove(index)
                motor_curve.index = min(motor_curve.index, len(items) - 1)
            elif self.action == 'UP' and index >= 1:
                motor_curve.items.move(index, index - 1)
                motor_curve.index -= 1
            elif self.action == 'DOWN' and index < len(items) - 1:
                motor_curve.items.move(index, index + 1)
                motor_curve.index += 1
        return {'FINISHED'}
