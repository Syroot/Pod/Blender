import bpy
from ubipod.vehicle import TransmissionType

from ...common.props.sound import POD_Sound


class POD_VehiclePart(bpy.types.PropertyGroup):
    shock_damper: bpy.props.FloatProperty(name="Shock Damper", subtype='FACTOR')
    mass: bpy.props.FloatProperty(name="Mass", subtype='FACTOR', soft_min=0, soft_max=2500)
    radius: bpy.props.FloatProperty(name="Radius", subtype='FACTOR', soft_min=0, soft_max=1)
    friction: bpy.props.FloatProperty(name="Friction", subtype='FACTOR', soft_min=0, soft_max=10)
    grip_x: bpy.props.FloatProperty(name="Grip X", subtype='FACTOR', soft_min=0, soft_max=4)
    grip_y: bpy.props.FloatProperty(name="Grip Y", subtype='FACTOR', soft_min=0, soft_max=0.610352)
    grip_z: bpy.props.FloatProperty(name="Grip Z", subtype='FACTOR')
    stiffness: bpy.props.FloatProperty(name="Stiffness", subtype='FACTOR', soft_min=0, soft_max=1)
    viscosity: bpy.props.FloatProperty(name="Viscosity", subtype='FACTOR', soft_min=0, soft_max=0.076599)
    z_empty: bpy.props.FloatProperty(name="Z Empty", subtype='FACTOR', soft_min=-0.592697, soft_max=0.5)
    z_max: bpy.props.FloatProperty(name="Z Max", subtype='FACTOR', soft_min=0, soft_max=1)
    z_min: bpy.props.FloatProperty(name="Z Min", subtype='FACTOR', soft_min=-1, soft_max=1)


class POD_VehiclePatcher(bpy.types.PropertyGroup):
    def img_poll(self, img: bpy.types.Image) -> bool:
        return img.size[0] == 333 and img.size[1] == 257

    def img_small_poll(self, img: bpy.types.Image) -> bool:
        return img.size[0] == 125 and img.size[1] == 90

    img: bpy.props.PointerProperty(name="Image", type=bpy.types.Image, poll=img_poll)
    img_small: bpy.props.PointerProperty(name="Small Image", type=bpy.types.Image, poll=img_small_poll)


class POD_VehiclePhysics(bpy.types.PropertyGroup):
    speed_factors_used: bpy.props.IntProperty(name="Used Speed Factors", subtype='FACTOR', min=0, max=8)
    speed_factors: bpy.props.FloatVectorProperty(name="Speed Factors", size=9)
    steer_max: bpy.props.FloatProperty(name="Steer Max", subtype='FACTOR', soft_min=0, soft_max=1.569092)
    steer_speed: bpy.props.FloatProperty(name="Steer Speed", subtype='FACTOR', soft_min=0, soft_max=0.399292)
    steer_recall: bpy.props.FloatProperty(name="Steer Recall", subtype='FACTOR', soft_min=0, soft_max=2)
    unknown494: bpy.props.FloatProperty(name="Unknown 494", subtype='FACTOR')
    inertia_moments_jx: bpy.props.FloatProperty(name="Inertia X", subtype='FACTOR', soft_min=0, soft_max=6103.515625)
    inertia_moments_jy: bpy.props.FloatProperty(name="Inertia Y", subtype='FACTOR', soft_min=0, soft_max=6103.515625)
    inertia_moments_jz: bpy.props.FloatProperty(name="Inertia Z", subtype='FACTOR', soft_min=0, soft_max=6103.515625)
    gear_up_speed: bpy.props.FloatProperty(name="Gear Up Speed", subtype='FACTOR')
    gear_down_speed: bpy.props.FloatProperty(name="Gear Down Speed", subtype='FACTOR')
    chassis_mass: bpy.props.FloatProperty(name="Chassis Mass", subtype='FACTOR', soft_min=0, soft_max=3000)
    gravity_factor: bpy.props.FloatProperty(name="Gravity Factor", subtype='FACTOR', soft_min=0, soft_max=5)
    fin_factor_x: bpy.props.FloatProperty(name="Fin Factor X", subtype='FACTOR')
    fin_factor_y: bpy.props.FloatProperty(name="Fin Factor Y", subtype='FACTOR')
    brake_distribution: bpy.props.FloatProperty(name="Brake Distribution", subtype='FACTOR', soft_min=0, soft_max=100)
    zoom_factor: bpy.props.FloatProperty(name="Zoom Factor", subtype='FACTOR', soft_min=0, soft_max=1.601898)
    viscous_friction: bpy.props.FloatProperty(name="Viscous Friction", subtype='FACTOR')
    brake_slope_curve: bpy.props.FloatProperty(name="Brake Slope Curve", subtype='FACTOR', soft_min=0, soft_max=3.051758)
    brake_max: bpy.props.FloatProperty(name="Brake Max", subtype='FACTOR', soft_min=0, soft_max=3.051758)
    transmission_type: bpy.props.EnumProperty(name="Transmission Type", items=(
        (TransmissionType.FOUR_WHEEL.name, "4WD", "", TransmissionType.FOUR_WHEEL.value),
        (TransmissionType.TRACTION.name, "Traction", "", TransmissionType.TRACTION.value),
        (TransmissionType.PROPULSION.name, "Propulsion", "", TransmissionType.PROPULSION.value),
    ))


class POD_VehicleMotorCurveItem(bpy.types.PropertyGroup):
    value: bpy.props.IntProperty()


class POD_VehicleMotorCurve(bpy.types.PropertyGroup):
    items: bpy.props.CollectionProperty(type=POD_VehicleMotorCurveItem)
    index: bpy.props.IntProperty(name="Index")


class POD_VehicleSounds(bpy.types.PropertyGroup):
    engine: bpy.props.PointerProperty(type=POD_Sound)
    gear_next: bpy.props.PointerProperty(type=POD_Sound)
    gear_prev: bpy.props.PointerProperty(type=POD_Sound)
    brake: bpy.props.PointerProperty(type=POD_Sound)
    brake_stop: bpy.props.PointerProperty(type=POD_Sound)
    crash_soft: bpy.props.PointerProperty(type=POD_Sound)
    unknown: bpy.props.PointerProperty(type=POD_Sound)
    shock: bpy.props.PointerProperty(type=POD_Sound)
    accel: bpy.props.PointerProperty(type=POD_Sound)
    accel_stop: bpy.props.PointerProperty(type=POD_Sound)
    decel: bpy.props.PointerProperty(type=POD_Sound)
    decel_stop: bpy.props.PointerProperty(type=POD_Sound)
    crash: bpy.props.PointerProperty(type=POD_Sound)
    skid_roof: bpy.props.PointerProperty(type=POD_Sound)
    skid_roof_stop: bpy.props.PointerProperty(type=POD_Sound)


class POD_VehicleStats(bpy.types.PropertyGroup):
    accel: bpy.props.IntProperty(name="Accel", subtype='FACTOR', soft_min=0, soft_max=100)
    brakes: bpy.props.IntProperty(name="Brakes", subtype='FACTOR', soft_min=0, soft_max=100)
    grip: bpy.props.IntProperty(name="Grip", subtype='FACTOR', soft_min=0, soft_max=100)
    handling: bpy.props.IntProperty(name="Handling", subtype='FACTOR', soft_min=0, soft_max=100)
    speed: bpy.props.IntProperty(name="Speed", subtype='FACTOR', soft_min=0, soft_max=100)

    def get_total(self) -> int:
        return self.accel + self.brakes + self.grip + self.handling + self.speed


class POD_Vehicle(bpy.types.PropertyGroup):
    patcher: bpy.props.PointerProperty(type=POD_VehiclePatcher)
    wheel_fr: bpy.props.PointerProperty(type=POD_VehiclePart)
    wheel_rr: bpy.props.PointerProperty(type=POD_VehiclePart)
    wheel_fl: bpy.props.PointerProperty(type=POD_VehiclePart)
    wheel_rl: bpy.props.PointerProperty(type=POD_VehiclePart)
    chassis: bpy.props.PointerProperty(type=POD_VehiclePart)
    physics: bpy.props.PointerProperty(type=POD_VehiclePhysics)
    motor_curve: bpy.props.PointerProperty(type=POD_VehicleMotorCurve)
    sounds: bpy.props.PointerProperty(type=POD_VehicleSounds)
    stats: bpy.props.PointerProperty(type=POD_VehicleStats)


def register() -> None:
    bpy.types.Scene.pod_vehicle = bpy.props.PointerProperty(type=POD_Vehicle)


def unregister() -> None:
    del bpy.types.Scene.pod_vehicle
