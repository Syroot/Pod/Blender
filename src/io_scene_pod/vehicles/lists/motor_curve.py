import bpy

from ..props.scene import POD_VehicleMotorCurve, POD_VehicleMotorCurveItem


class POD_UL_VehicleMotorCurveList(bpy.types.UIList):
    def draw_item(self, context: bpy.types.Context, layout: bpy.types.UILayout,
                  data: POD_VehicleMotorCurve, item: POD_VehicleMotorCurveItem, icon: int,
                  active_data: POD_VehicleMotorCurve, active_propname: str,
                  index: int, flt_flags: int) -> None:
        row = layout.row()
        row.split(align=True)
        row.label(text=f"Value {index + 1}")
        row.prop(item, "value", emboss=False, text=" ")
