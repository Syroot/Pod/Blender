import bpy

from ..lists.motor_curve import POD_UL_VehicleMotorCurveList
from ..ops.motor_curve_actions import POD_OT_VehicleMotorCurveActions
from ..props.scene import POD_VehiclePart, POD_VehiclePatcher, POD_VehicleSounds, POD_VehicleStats


class POD_PT_Vehicle(bpy.types.Panel):
    bl_label = "Pod Vehicle"
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'
    bl_context = "scene"

    def draw(self, context: bpy.types.Context) -> None:
        pass


class VehiclePartPanel:
    bl_options = {'DEFAULT_CLOSED'}
    bl_parent_id = POD_PT_Vehicle.__name__
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'

    def draw_part(self, part: POD_VehiclePart) -> None:
        layout = self.layout
        row = layout.row()
        row.prop(part, "shock_damper")
        row.prop(part, "mass")
        row = layout.row()
        row.prop(part, "radius")
        row.prop(part, "friction")
        row = layout.row()
        row.prop(part, "stiffness")
        row.prop(part, "viscosity")
        row = layout.row()
        row.prop(part, "grip_x")
        row.prop(part, "z_empty")
        row = layout.row()
        row.prop(part, "grip_y")
        row.prop(part, "z_min")
        row = layout.row()
        row.prop(part, "grip_z")
        row.prop(part, "z_max")


class POD_PT_VehiclePatcher(bpy.types.Panel, VehiclePartPanel):
    bl_label = "Patcher"
    bl_order = 1

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        layout.use_property_decorate = False
        layout.use_property_split = True
        props: POD_VehiclePatcher = context.scene.pod_vehicle.patcher
        layout.template_ID(props, "img", text="Image", new="image.new", open="image.open")
        layout.template_ID(props, "img_small", text="Small Image", new="image.new", open="image.open")


class POD_PT_VehicleMotorCurve(bpy.types.Panel):
    bl_context = "scene"
    bl_label = "Motor Curve"
    bl_options = {'DEFAULT_CLOSED'}
    bl_order = 2
    bl_parent_id = POD_PT_Vehicle.__name__
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        scene = context.scene
        row = layout.row()
        row.template_list(POD_UL_VehicleMotorCurveList.__name__, "",
                          scene.pod_vehicle.motor_curve, "items",
                          scene.pod_vehicle.motor_curve, "index",
                          rows=8)
        col = row.column(align=True)
        col.operator(POD_OT_VehicleMotorCurveActions.bl_idname, icon='ADD', text="").action = 'ADD'
        col.operator(POD_OT_VehicleMotorCurveActions.bl_idname, icon='REMOVE', text="").action = 'REMOVE'
        col.separator()
        col.operator(POD_OT_VehicleMotorCurveActions.bl_idname, icon='TRIA_UP', text="").action = 'UP'
        col.operator(POD_OT_VehicleMotorCurveActions.bl_idname, icon='TRIA_DOWN', text="").action = 'DOWN'


class POD_PT_VehiclePartWheelFL(bpy.types.Panel, VehiclePartPanel):
    bl_label = "Part Wheel Front Left"
    bl_order = 3

    def draw(self, context: bpy.types.Context) -> None:
        self.draw_part(context.scene.pod_vehicle.wheel_fl)


class POD_PT_VehiclePartWheelFR(bpy.types.Panel, VehiclePartPanel):
    bl_label = "Part Wheel Front Right"
    bl_order = 4

    def draw(self, context: bpy.types.Context) -> None:
        self.draw_part(context.scene.pod_vehicle.wheel_fr)


class POD_PT_VehiclePartWheelRL(bpy.types.Panel, VehiclePartPanel):
    bl_label = "Part Wheel Rear Left"
    bl_order = 5

    def draw(self, context: bpy.types.Context) -> None:
        self.draw_part(context.scene.pod_vehicle.wheel_rl)


class POD_PT_VehiclePartWheelRR(bpy.types.Panel, VehiclePartPanel):
    bl_label = "Part Wheel Rear Right"
    bl_order = 6

    def draw(self, context: bpy.types.Context) -> None:
        self.draw_part(context.scene.pod_vehicle.wheel_rr)


class POD_PT_VehiclePartChassis(bpy.types.Panel, VehiclePartPanel):
    bl_label = "Part Chassis"
    bl_order = 7

    def draw(self, context: bpy.types.Context) -> None:
        self.draw_part(context.scene.pod_vehicle.chassis)


class POD_PT_VehiclePhysics(bpy.types.Panel):
    bl_context = "scene"
    bl_label = "Physics"
    bl_options = {'DEFAULT_CLOSED'}
    bl_order = 8
    bl_parent_id = POD_PT_Vehicle.__name__
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        scene = context.scene
        layout.prop(scene.pod_vehicle.physics, "steer_max")
        layout.prop(scene.pod_vehicle.physics, "steer_speed")
        layout.prop(scene.pod_vehicle.physics, "steer_recall")
        layout.prop(scene.pod_vehicle.physics, "unknown494")
        layout.prop(scene.pod_vehicle.physics, "inertia_moments_jx")
        layout.prop(scene.pod_vehicle.physics, "inertia_moments_jy")
        layout.prop(scene.pod_vehicle.physics, "inertia_moments_jz")
        layout.prop(scene.pod_vehicle.physics, "gear_up_speed")
        layout.prop(scene.pod_vehicle.physics, "gear_down_speed")
        layout.prop(scene.pod_vehicle.physics, "chassis_mass")
        layout.prop(scene.pod_vehicle.physics, "gravity_factor")
        layout.prop(scene.pod_vehicle.physics, "fin_factor_x")
        layout.prop(scene.pod_vehicle.physics, "fin_factor_y")
        layout.prop(scene.pod_vehicle.physics, "brake_distribution")
        layout.prop(scene.pod_vehicle.physics, "zoom_factor")
        layout.prop(scene.pod_vehicle.physics, "viscous_friction")
        layout.prop(scene.pod_vehicle.physics, "brake_slope_curve")
        layout.prop(scene.pod_vehicle.physics, "brake_max")
        layout.prop(scene.pod_vehicle.physics, "transmission_type", expand=True)


class POD_PT_VehicleSpeedFactors(bpy.types.Panel):
    bl_context = "scene"
    bl_label = "Speed Factors"
    bl_options = {'DEFAULT_CLOSED'}
    bl_order = 9
    bl_parent_id = POD_PT_Vehicle.__name__
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        physics = context.scene.pod_vehicle.physics
        layout.prop(physics, "speed_factors_used", text="Forward Gears")
        row = layout.row()
        col = row.column()
        col.prop(physics, "speed_factors", text="")


class POD_PT_VehicleSounds(bpy.types.Panel):
    bl_context = "scene"
    bl_label = "Sounds"
    bl_options = {'DEFAULT_CLOSED'}
    bl_order = 10
    bl_parent_id = POD_PT_Vehicle.__name__
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        sounds: POD_VehicleSounds = context.scene.pod_vehicle.sounds
        split = layout.split()
        sounds.engine.draw(split, "Engine")
        split.label()
        split = layout.split()
        sounds.gear_prev.draw(split, "Gear Prev")
        sounds.gear_next.draw(split, "Gear Next")
        split = layout.split()
        sounds.accel.draw(split, "Accel")
        sounds.accel_stop.draw(split, "Accel Stop")
        split = layout.split()
        sounds.decel.draw(split, "Decel")
        sounds.decel_stop.draw(split, "Decel Stop")
        split = layout.split()
        sounds.brake.draw(split, "Brake")
        sounds.brake_stop.draw(split, "Brake Stop")
        split = layout.split()
        sounds.crash.draw(split, "Crash")
        sounds.crash_soft.draw(split, "Crash Soft")
        split = layout.split()
        sounds.skid_roof.draw(split, "Skid Roof")
        sounds.skid_roof_stop.draw(split, "Skid Roof Stop")
        split = layout.split()
        sounds.shock.draw(split, "Shock")
        sounds.unknown.draw(split, "Unknown")


class POD_PT_VehicleStats(bpy.types.Panel):
    bl_context = "scene"
    bl_label = "Stats"
    bl_options = {'DEFAULT_CLOSED'}
    bl_order = 11
    bl_parent_id = POD_PT_Vehicle.__name__
    bl_region_type = 'WINDOW'
    bl_space_type = 'PROPERTIES'

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        bstats: POD_VehicleStats = context.scene.pod_vehicle.stats
        # Display each setting.
        row = layout.row()
        row.prop(bstats, "accel")
        row = layout.row()
        row.prop(bstats, "brakes")
        row = layout.row()
        row.prop(bstats, "grip")
        row = layout.row()
        row.prop(bstats, "handling")
        row = layout.row()
        row.prop(bstats, "speed")
        # Display total.
        row = layout.row()
        total = bstats.get_total()
        if total > 300:
            row.label(text=f"Total is {total}.", icon='CANCEL')
        elif total < 300:
            row.label(text=f"Total is {total}.", icon='ERROR')
        else:
            row.label(text=f"Total is {total}.", icon='CHECKMARK')
