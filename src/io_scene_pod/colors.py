def from_rgb565(value: int) -> tuple[float, float, float]:
    r = (value >> 11 & 31) / 31.0
    g = (value >> 5 & 63) / 63.0
    b = (value & 31) / 31.0
    return r, g, b


def from_rgb888(value: int) -> tuple[float, float, float]:
    r = (value >> 16 & 255) / 255.0
    g = (value >> 8 & 255) / 255.0
    b = (value & 255) / 255.0
    return r, g, b


def to_rgb565(value: tuple[float, float, float]) -> int:
    r = round(value[0] * 31) << 11
    g = round(value[1] * 63) << 5
    b = round(value[2] * 31)
    return r | g | b


def to_rgb888(value: tuple[float, float, float]) -> int:
    r = round(value[0] * 255) << 16
    g = round(value[1] * 255) << 8
    b = round(value[2] * 255)
    return r | g | b
